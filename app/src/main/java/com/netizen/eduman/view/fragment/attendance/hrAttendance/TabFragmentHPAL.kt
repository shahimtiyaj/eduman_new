package com.netizen.eduman.view.fragment.attendance.hrAttendance

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentTabBinding

class TabFragmentHPAL : Fragment() {

    private lateinit var binding: FragmentTabBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tab, container, false)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        binding.viewpager.adapter = MyAdapter(childFragmentManager)
        binding.viewpager.offscreenPageLimit = 3

        binding.tabs.post { binding.tabs.setupWithViewPager(binding.viewpager) }

        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        return binding.root
    }

    internal inner class MyAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return FragmentHRPresentReport()
                1 -> return FragmentHRAbsentReport()
                2 -> return FragmentHRLeaveReport()
            }
             return FragmentHRPresentReport()
        }

        override fun getCount(): Int {

            return int_items
        }

        override fun getPageTitle(position: Int): CharSequence? {

            when (position) {
                0 -> return "Present"
                1 -> return "Absent"
                2 -> return "Leave"
            }
            return null
        }
    }

    companion object {
        var int_items = 3
    }
}

