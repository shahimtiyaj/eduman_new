package com.netizen.eduman.view.fragment.semesterExam.remarks

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.RemarksAssignSelectionFragmentBinding
import com.netizen.eduman.model.SectionResponse
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.utils.MyUtilsClass.Companion.showErrorToasty
import com.netizen.eduman.view.fragment.SectionDialogFragment
import com.netizen.eduman.viewModel.SemesterExamViewModel
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty

class RemarksAssignSelectionFragment : Fragment() {
    private lateinit var binding: RemarksAssignSelectionFragmentBinding
    private lateinit var studentViewModel: StudentViewModel
    private lateinit var semesterViewModel: SemesterExamViewModel
    private var errorTextView: TextView?=null
    private lateinit var preferences: AppPreferences
    private  var sectionList= ArrayList<SectionResponse.Section>()
    private var sectionName: String? = null
    private var sectionId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        studentViewModel = ViewModelProviders.of(
            activity!!,
            StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application)
        )
            .get(StudentViewModel::class.java)

        semesterViewModel = ViewModelProviders.of(
            activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application)
        )
            .get(SemesterExamViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.remarks_assign_selection_fragment_,
            container,
            false
        )

        binding.lifecycleOwner = this
        initViews()
        initObservables()
        studentViewModel.getSectionList()
        preferences = AppPreferences(context)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        semesterViewModel.classConfigId = null
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.sectionSelect.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("isStudent", true)

            val sectionDialogFragment = SectionDialogFragment()
            sectionDialogFragment.arguments = bundle
            sectionDialogFragment.show(activity!!.supportFragmentManager, null)
        }

        binding.spinnerExam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    semesterViewModel.getRemarkTitleExamId(parent!!.getItemAtPosition(position).toString())
                } else {
                    semesterViewModel.remarkTitleExamId = null
                }
            }
        }

        binding.spinnerRemarksTitle.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        semesterViewModel.geRemarkTitleId(parent!!.getItemAtPosition(position).toString())
                    } else {
                        semesterViewModel.remarkTitleId = null
                    }
                }
            }

        binding.buttonFind.btnFind.setOnClickListener {
            semesterViewModel.checkRemarkAssignValues()
        }
    }

    private fun initObservables() {
        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerSection.visibility = View.VISIBLE
            } else {
                binding.shimmerSection.visibility = View.GONE
            }
        })

        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        semesterViewModel.isSectionEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.textViewSection.error = "Empty"
                Toasty.error(context!!, getString(R.string.select_section), Toasty.LENGTH_LONG).show()
                semesterViewModel.isSectionEmpty.value = false
            }
        })

        Loaders.sectionName.observe(viewLifecycleOwner, Observer { sectionName ->
            if (!sectionName.isNullOrEmpty()) {
                if (!sectionName.equals("Select Section", true)) {
                    semesterViewModel.isSectionEmpty.value = false
                    binding.textViewSection.error = null
                } else {
                    semesterViewModel.classConfigId = null
                }

                semesterViewModel.sectionName.postValue(sectionName)
                binding.textViewSection.text = sectionName
                semesterViewModel.getSectionIdInRemarks(sectionName)
            }
        })

        studentViewModel.sectionList.observe(viewLifecycleOwner, Observer { sectionList ->
            if (!sectionList.isNullOrEmpty()) {
                semesterViewModel.sectionList.value = sectionList
            }
        })

        Loaders.isLoading3.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerExam.visibility = View.VISIBLE
            } else {
                binding.shimmerExam.visibility = View.GONE
            }
        })

        semesterViewModel.isExamEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it && binding.spinnerExam.selectedView !=null) {
                errorTextView = binding.spinnerExam.selectedView as? TextView
                errorTextView?.error = "Empty"
                showErrorToasty(context!!, getString(R.string.select_exam))
                semesterViewModel.isExamEmpty.value = false
            }
        })

        semesterViewModel.isRemarkTitleEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it && binding.spinnerRemarksTitle.selectedView !=null) {
                errorTextView = binding.spinnerRemarksTitle.selectedView as? TextView
                errorTextView?.error = "Empty"
                showErrorToasty(context!!, getString(R.string.select_remarks))
                semesterViewModel.isRemarkTitleEmpty.value = false
            }
        })

        semesterViewModel.tempExamList.observe(viewLifecycleOwner, Observer { examList ->
                setExamSpinner(examList)
        })

        semesterViewModel.remarkAssignDescription.observe(viewLifecycleOwner, Observer {

            binding.etStudentRemarksDescription.setText(it)

        })

        Loaders.isLoadingRemarkTitle.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerRemarksTitle.visibility = View.VISIBLE
            } else {
                binding.shimmerRemarksTitle.visibility = View.GONE
            }
        })

        semesterViewModel.tempRemarkAssignTitleList.observe(
            viewLifecycleOwner,
            Observer { remarkTitleList ->
                if (!remarkTitleList.isNullOrEmpty()) {
                    setRemarkTitleSpinner(remarkTitleList)
                }
            })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()){
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                findNavController().popBackStack()
                Loaders.apiSuccess.value = null
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

      semesterViewModel.isRemarkListFound.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                semesterViewModel.remarkAssignDescription.value = binding.etStudentRemarksDescription.text.toString()
                findNavController().navigate(R.id.action_remarksAssignSelectionFragment_to_remarkAssignStudentListFragment)
                semesterViewModel.isRemarkListFound.value = false
            }
        })

    }

    private fun setExamSpinner(examTempList: List<String>?) {
        val sectionAdapter = examTempList?.let {
            ArrayAdapter(
                requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                it
            )
        }
        sectionAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerExam.adapter = sectionAdapter
    }

    private fun setRemarkTitleSpinner(remarkTilteTempList: List<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            remarkTilteTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerRemarksTitle.adapter = sectionAdapter
    }
}
