package com.netizen.eduman.view.fragment.appOpeningInroPage

import android.annotation.SuppressLint
import android.content.res.Resources
import android.graphics.Rect
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.TouchDelegate
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import cdflynn.android.library.turn.TurnLayoutManager
import com.airbnb.lottie.LottieAnimationView
import com.netizen.eduman.R
import com.netizen.eduman.adapter.CycleAdapter
import com.netizen.eduman.adapter.SplashPagerAdapter
import com.netizen.eduman.databinding.FragmentSplashMainBinding
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.viewModel.SignInViewModel


class FragmentSplashMain : Fragment() {

    private lateinit var binding: FragmentSplashMainBinding
    private lateinit var signInViewModel: SignInViewModel

    private lateinit var viewpager: ViewPager
    private lateinit var adapter: SplashPagerAdapter
    private lateinit var turnLayoutManager: TurnLayoutManager

    private lateinit var lottieView: LottieAnimationView
    private lateinit var appPreferences: AppPreferences

    private var width: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash_main, container, false)

        appPreferences = AppPreferences(context)

//        av_from_code.setAnimation("loader.json")
//        av_from_code.visibility = View.VISIBLE
//        av_from_code.playAnimation()
//        av_from_code.loop(true)

        lottieView = binding.lottieSlider
        viewpager = binding.viewPager
        binding.tabLayoutDots.setupWithViewPager(viewpager, true)

        if (!appPreferences.isSplashShown()) {
            setupViewPager()
        } else {
            findNavController().navigate(R.id.action_splashFragment_to_signInFragment)
        }

        initViews()
        initObservables()
//        setUpCycleAdapter()

        return  binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        MainActivity.hideActionBar()
        signInViewModel = ViewModelProviders.of(this,
            SignInViewModel.SignInViewModelFactory())
            .get(SignInViewModel::class.java)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initViews() {
        var currentPage = 0
        width = binding.buttonLogIn.layoutParams.width

//        viewpager.setOnTouchListener(object : OnSwipeTouchListener() {
//
//            var position: Int = 0
//
//            override fun onSwipeLeft() {
//                super.onSwipeLeft()
//                Log.e(TAG, position.toString())
//
////                binding.recyclerViewCycle.smoothScrollToPosition(3)
////                turnLayoutManager.scrollToPosition(3)
//                if (position < 3) {
//                    turnLayoutManager.scrollToPositionWithOffset(++position, 0)
//                }
//            }
//
//            override fun onSwipeRight() {
//                super.onSwipeRight()
//                Log.e(TAG, position.toString())
//                if (position > 0) {
//                    turnLayoutManager.scrollToPositionWithOffset(--position, 0)
//                }
//            }
//        })

        viewpager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
//                Log.e("onPageScrollStateChanged", "$state")
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
//                Log.e("onPageScrolled", "$position $positionOffset$positionOffsetPixels")
                lottieView.progress = (position * (1f / position)) + (positionOffset * (1f / position))
            }

            override fun onPageSelected(position: Int) {
//                turnLayoutManager.scrollToPositionWithOffset(position, 0)

                if (position >= 3) {
//                    animateButton()

                    val animMove = AnimationUtils.loadAnimation(
                        context,
                        R.anim.move_right
                    )
                    binding.buttonNext.startAnimation(animMove)
                    binding.buttonNext.visibility = View.GONE

                    val layoutParams = binding.buttonLogIn.layoutParams
                    layoutParams.width = ConstraintLayout.LayoutParams.MATCH_PARENT
                    binding.buttonLogIn.layoutParams = layoutParams

//                    binding.layoutButton.visibility = View.GONE
//                    binding.buttonLogInLarge.visibility = View.VISIBLE
//
//                    binding.layoutButton.animate().alpha(0.0f)
//                    binding.buttonLogInLarge.animate().alpha(1.0f)
                } else if (position < currentPage && position == 2) {
                    val animMove = AnimationUtils.loadAnimation(
                        context,
                        R.anim.move_left
                    )
                    binding.buttonNext.startAnimation(animMove)

                    val layoutParams = binding.buttonLogIn.layoutParams
                    layoutParams.width = width!!
                    binding.buttonLogIn.layoutParams = layoutParams
                }
//                else {
//                    binding.layoutButton.visibility = View.VISIBLE
//                    binding.buttonLogInLarge.visibility = View.GONE
//
//                    binding.buttonLogInLarge.animate().alpha(0.0f)
//                    binding.layoutButton.animate().alpha(1.0f)
//                }

                currentPage = position
            }
        })

        binding.recyclerViewCycle.setOnTouchListener({ v, event -> true })

        binding.buttonLogIn.setOnClickListener {
            appPreferences.setIsSplashShown()
            findNavController().navigate(R.id.action_splashFragment_to_signInFragment)
        }

        binding.buttonNext.setOnClickListener {
            viewpager.setCurrentItem(++currentPage, true)
//            adapter.notifyDataSetChanged()
        }
    }

    private fun initObservables() {
        signInViewModel.fragmentPosition.observe(viewLifecycleOwner, Observer {
//            Log.e(FragmentSplashMain::class.java.simpleName, it.toString())

            when(it) {
                2 -> {
                    viewpager.setCurrentItem(0, true)
                }
                3 -> {
                    viewpager.setCurrentItem(1, true)
                }
                4 -> {
                    viewpager.setCurrentItem(2, true)
                }
                5 -> {
                    viewpager.setCurrentItem(3, true)
                }
            }
        })
    }

    private fun setupViewPager() {
        adapter = SplashPagerAdapter(activity!!.supportFragmentManager)

        adapter.addFragment(SplashFragmentOne())
        adapter.addFragment(SplashFragmentFour())
        adapter.addFragment(SplashFragmentTwo())
        adapter.addFragment(SplashFragmentThree())

        viewpager.adapter = adapter
    }

    private fun setUpCycleAdapter() {
        val cycleAdapter = CycleAdapter(context!!, signInViewModel)

        turnLayoutManager = TurnLayoutManager(context,
            TurnLayoutManager.Gravity.END,
            TurnLayoutManager.Orientation.HORIZONTAL,
            700,
            1500,
            true)

        binding.recyclerViewCycle.layoutManager = turnLayoutManager
        binding.recyclerViewCycle.setHasFixedSize(true)
        binding.recyclerViewCycle.adapter = cycleAdapter
    }

    private fun animateButton() {
        val animMove = AnimationUtils.loadAnimation(
            context,
            R.anim.move_right
        )
        binding.buttonNext.startAnimation(animMove)

//        width = binding.buttonLogIn.layoutParams.width

        val layoutParams = binding.buttonLogIn.layoutParams
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        binding.buttonLogIn.layoutParams = layoutParams

//        val anim: Animation = ScaleAnimation(
//            1f, 2f,  // Start and end values for the X axis scaling
//            1f, 1f,  // Start and end values for the Y axis scaling
//            Animation.RELATIVE_TO_SELF, 0f,  // Pivot point of X scaling
//            Animation.RELATIVE_TO_SELF, 0f
//        ) // Pivot point of Y scaling
//
//        anim.fillAfter = true // Needed to keep the result of the animation
//        binding.buttonLogIn.startAnimation(anim)

//        val scaleDownX = ObjectAnimator.ofFloat(
//            binding.buttonLogIn,
//            "scaleX", 1.5f
//        )
//        scaleDownX.duration = 1000
//
//        val scaleDown = AnimatorSet()
//        scaleDown.play(scaleDownX)
//        scaleDown.start()
    }

    /**
     * Increase the click area of this view
     */
    fun View.increaseHitArea(dp: Float) {
        // increase the hit area
        val increasedArea = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp,
            Resources.getSystem().displayMetrics
        ).toInt()

        val parent = parent as View
        parent.post {
            val rect = Rect()
            getHitRect(rect)
            rect.top -= increasedArea
            rect.left -= increasedArea
            rect.bottom += increasedArea
            rect.right += increasedArea
            parent.touchDelegate = TouchDelegate(rect, this)
        }
    }

//    fun getHitRect(outRect: Rect) {
//        outRect.set(getLeft(), getTop(), getRight(), getBottom() + 30)
//    }
}
