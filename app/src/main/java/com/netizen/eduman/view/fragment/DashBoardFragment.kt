package com.netizen.eduman.view.fragment

import android.app.Application
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentDashBoardBinding
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.view.activity.MainActivity
import com.netizen.eduman.viewModel.DashboardViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class DashBoardFragment : Fragment() {

    private lateinit var binding: FragmentDashBoardBinding
    private var dashboardViewModel: DashboardViewModel? = null
    private lateinit var preferences: AppPreferences

    //private lateinit var binding: ActivityMainEndBinding
    //private lateinit var binding: ActivityMainStartBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = AppPreferences(context)

        dashboardViewModel = ViewModelProviders.of(
            this, DashboardViewModel.DashboardFactory(context!!.applicationContext as Application)
        ).get(DashboardViewModel::class.java)

        dashboardViewModel?.clearPreviousInstituteData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dash_board, container, false)

        MainActivity.showBottomNavigation()

        binding.shimmerSchoolLogo.visibility = View.VISIBLE
        binding.shimmerSchoolName.visibility = View.VISIBLE
        binding.shimmerSchoolAddress.visibility = View.VISIBLE
        binding.shimmerInstituteId.visibility = View.VISIBLE
        binding.shimmerAcademicYear.visibility = View.VISIBLE
        binding.shimmerSmsBalance.visibility = View.VISIBLE
        binding.shimmerBillPayment.visibility = View.VISIBLE

        binding.studentcardId.setOnClickListener { findNavController().navigate(R.id.action_dashBoardFragment_to_studentDashBoardFragment) }
        binding.hrcardId.setOnClickListener { findNavController().navigate(R.id.action_dashBoardFragment_to_hrDashBoardFragment) }
        binding.attendancecardId.setOnClickListener { findNavController().navigate(R.id.action_dashBoardFragment_to_attendanceDashboardFragment) }
        binding.semesterExamcardId.setOnClickListener { findNavController().navigate(R.id.action_dashBoardFragment_to_semesterExamDashBoardFragment) }

        binding.lifecycleOwner = this

        preferences.getToken()?.let { dashboardViewModel?.refreshInstituteInfoList(it) }
        preferences.getToken()?.let { dashboardViewModel?.refreshSmsBalanceInfoList(it) }
        // dashboardViewModel?.refreshBillPaymentInfoList("10503")//10503

        try {
            initObservables()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return binding.root
    }

    private fun initObservables() {
        dashboardViewModel?.instituteInfoMutableLiveData?.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                binding.textViewSchoolName.text = it[0].instituteName
                binding.textViewSchoolAddress.text = it[0].instituteAddress
                binding.textViewInstituteId.text = it[0].instituteId
                binding.textViewAcademicYear.text = it[0].academic_year

                if (!it[0].instituteLogo.isNullOrEmpty()) {
                    Glide.with(context)
                        .load(Base64.decode(it[0].instituteLogo, Base64.DEFAULT))
                        .asBitmap()
                        .placeholder(R.drawable.ic_boy_icon)
                        .into(binding.schoolLogo)
                }

                binding.shimmerSchoolLogo.visibility = View.GONE
                binding.shimmerSchoolName.visibility = View.GONE
                binding.shimmerSchoolAddress.visibility = View.GONE
                binding.shimmerInstituteId.visibility = View.GONE
                binding.shimmerAcademicYear.visibility = View.GONE

                it[0].instituteId?.let { it1 -> dashboardViewModel?.refreshBillPaymentInfoList(it1) }//10503

            }
        })

        dashboardViewModel?.lottieProgressDialog?.observe(this, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        dashboardViewModel?.errorStatus?.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
            }
        })

        //SMS Balance Observer
        dashboardViewModel?.smsBalanceInfoMutableLiveData?.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) {
                binding.textSmsBalance.text = "0"
            } else {
                binding.textSmsBalance.text = it.toString()
            }

            binding.shimmerSmsBalance.visibility = View.GONE
        })

        //Bill Payment Observer
        dashboardViewModel?.billPaymentInfoMutableLiveData?.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) {
                binding.textSmsBalance.text = "N/A"
            } else {
                binding.textBillPayment.text = it.toString()
            }

            binding.shimmerBillPayment.visibility = View.GONE
        })
    }
}
