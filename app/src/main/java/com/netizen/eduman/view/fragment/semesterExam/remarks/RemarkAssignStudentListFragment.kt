package com.netizen.eduman.view.fragment.semesterExam.remarks

import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.RemarksAssignAdapter
import com.netizen.eduman.databinding.RemarksAssignRecylerListviewBinding
import com.netizen.eduman.model.RemarkAssignPostData
import com.netizen.eduman.model.RemarkAssignStudentListResponse
import com.netizen.eduman.model.SectionResponse
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.SemesterExamViewModel
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty

class RemarkAssignStudentListFragment : Fragment(), RemarksAssignAdapter.OnSaveButtonClick{

    private lateinit var binding: RemarksAssignRecylerListviewBinding
    private lateinit var studentViewModel: StudentViewModel
    private lateinit var semesterViewModel: SemesterExamViewModel
    private lateinit var errorTextView: TextView
    private lateinit var preferences: AppPreferences
    private lateinit var sectionList: List<SectionResponse.Section>
    private var sectionId: String? = null
    private var remarksAssignDescription: String? = null
    private var remarkAssignStudentList = ArrayList<RemarkAssignStudentListResponse.StudentRemarkAssign>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        studentViewModel = ViewModelProviders.of(activity!!,
            StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application))
            .get(StudentViewModel::class.java)

        semesterViewModel = ViewModelProviders.of(activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application))
            .get(SemesterExamViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=DataBindingUtil.inflate(inflater, R.layout.remarks_assign_recyler_listview, container, false)
        binding.lifecycleOwner = this

        initViews()
        initObservables()
//        val args =
//            RemarkAssignStudentListFragmentArgs.fromBundle(
//                arguments!!
//            )
        // binding.textViewSectionName.text = semesterViewModel.sectionName.value?.replace("-", " > ")
        //semesterViewModel.getRemarkAssignStudentList(args.sectionId, semesterViewModel.remarkTitleExamId.toString())

        return binding.root
    }

    private fun initObservables() {

        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        semesterViewModel.remarkAssignStudentList.observe(
            viewLifecycleOwner,
            Observer { remarkAssignList ->
                if (!remarkAssignList.isNullOrEmpty()) {
                    this.remarkAssignStudentList = remarkAssignList as ArrayList<RemarkAssignStudentListResponse.StudentRemarkAssign>
                    setAdapter(remarkAssignList)
                }
                binding.totalFound.text= remarkAssignList.size.toString()

            })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                findNavController().popBackStack()
                Loaders.apiSuccess.value=""
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value=""
            }
        })
        semesterViewModel.sectionName.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.textViewSectionName.text = it.toString().replace("-", " > ")
            }
        })
        /*Loaders.sectionName.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.textViewSectionName.text = it
            }
        })*/

        semesterViewModel.remarkAssignDescription.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                remarksAssignDescription = it
            }
        })
    }

    override fun onSave() {
        val ids = ArrayList<String>()
        for (remarkAssign in remarkAssignStudentList) {
            if (remarkAssign.getSelected()) { ids.add(remarkAssign.getIdentificationId().toString()) }
        }
        if (!ids.isNullOrEmpty()){
            val remarkAssignList = RemarkAssignPostData(ids, semesterViewModel.remarkTitleExamId, remarksAssignDescription)
            semesterViewModel.postRemarkAssignData(remarkAssignList)
        }

        else{
            Toasty.error(context!!, "No remarks is checked to assign!", Toasty.LENGTH_LONG).show()
        }
    }

    private fun setAdapter(remarkAssignList: List<RemarkAssignStudentListResponse.StudentRemarkAssign>?) {
        binding.allRemarksRecylerViewId.setHasFixedSize(true)
        binding.allRemarksRecylerViewId.layoutManager = LinearLayoutManager(context)
        binding.allRemarksRecylerViewId.adapter = remarkAssignList?.let { RemarksAssignAdapter(context!!, it, this) }
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.imageViewSearch.setOnClickListener {
            binding.layoutSearch.visibility = View.VISIBLE
            binding.toolbarTitle.visibility = View.GONE
            binding.imageViewSearch.visibility = View.GONE
        }

        binding.imageViewSearchClose.setOnClickListener {
            binding.layoutSearch.visibility = View.GONE
            binding.toolbarTitle.visibility = View.VISIBLE
            binding.imageViewSearch.visibility = View.VISIBLE
        }

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempTakeAtdReportList = ArrayList<RemarkAssignStudentListResponse.StudentRemarkAssign>()

                remarkAssignStudentList.forEach { remarkAssignReport ->
                    if (remarkAssignReport.getStudentRoll().toString().toLowerCase().contains(s.toString(), true)) {
                        tempTakeAtdReportList.add(remarkAssignReport)
                    }
                }

                setAdapter(tempTakeAtdReportList)
            }
        })
    }
}
