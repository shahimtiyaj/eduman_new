package com.netizen.eduman.view.fragment.semesterExam.reports


import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.UnAssignMarksDetailsAdapter
import com.netizen.eduman.databinding.FragmentUnAssignedMarksDetailsBinding
import com.netizen.eduman.viewModel.SemesterExamViewModel

/**
 * A simple [Fragment] subclass.
 */
class UnassignedMarksDetailsFragment : Fragment() {

    private lateinit var binding: FragmentUnAssignedMarksDetailsBinding
    private lateinit var semesterViewModel: SemesterExamViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_un_assigned_marks_details,
            container,
            false
        )

        initObservables()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        semesterViewModel = ViewModelProviders.of(activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application))
            .get(SemesterExamViewModel::class.java)
    }



    private fun initObservables() {

        semesterViewModel.unassignedMarksList.observe(viewLifecycleOwner, Observer { unAssignMarkList ->
            if (!unAssignMarkList.isNullOrEmpty()) {
                binding.recyclerViewUnassignedMarks.layoutManager = LinearLayoutManager(context)
                binding.recyclerViewUnassignedMarks.setHasFixedSize(true)
                binding.recyclerViewUnassignedMarks.adapter = UnAssignMarksDetailsAdapter(context!!, unAssignMarkList, semesterViewModel)
            }
        })

        semesterViewModel.isGoToNext.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                RemainingSubjectsFragment().show(activity!!.supportFragmentManager, null)
                semesterViewModel.isGoToNext.value = false
            }
        })
    }
}
