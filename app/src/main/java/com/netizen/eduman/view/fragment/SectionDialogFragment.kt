package com.netizen.eduman.view.fragment


import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.SectionAdapter
import com.netizen.eduman.databinding.FragmentSectionDialogBinding
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.SemesterExamViewModel
import com.netizen.eduman.viewModel.StudentViewModel

/**
 * A simple [Fragment] subclass.
 */
class SectionDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentSectionDialogBinding
    private lateinit var semesterViewModel: SemesterExamViewModel
    private lateinit var studentViewModel: StudentViewModel

    private  var studentSectionList=ArrayList<String>()
    private var semesterExamSectionList=ArrayList<String>()

    private var isStudent: Boolean? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_section_dialog,
            container,
            false)

        isStudent = arguments!!.getBoolean("isStudent")

        initViews()
        initObservables()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        semesterViewModel = ViewModelProviders.of(activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application))
            .get(SemesterExamViewModel::class.java)

        studentViewModel = ViewModelProviders.of(activity!!,
            StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application))
            .get(StudentViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val params = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params
    }

    private fun initViews() {
        binding.textViewClose.setOnClickListener {
            dismiss()
        }

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempSectionList = ArrayList<String>()

                studentSectionList.forEach { section ->
                    if (section.toLowerCase().contains(s.toString(), true)) {
                        tempSectionList.add(section)
                    }

                    setSectionList(tempSectionList)
                }
            }
        })
    }

    private fun initObservables() {
        semesterViewModel.tempSectionList.observe(viewLifecycleOwner, Observer {tempSectionList ->
            if (!tempSectionList.isNullOrEmpty() && !isStudent!!) {
                this.semesterExamSectionList = tempSectionList
                setSectionList(tempSectionList)
            }
        })

        studentViewModel.tempSectionList.observe(viewLifecycleOwner, Observer { tempSectionList ->
            if (!tempSectionList.isNullOrEmpty() && isStudent!!) {
                this.studentSectionList = tempSectionList
                setSectionList(tempSectionList)
            }
        })

        Loaders.sectionName.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                dismiss()
                Loaders.sectionName.postValue(null)
            }
        })
    }

    private fun setSectionList(tempSectionList: List<String>) {
        binding.recyclerViewSearchSection.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewSearchSection.setHasFixedSize(true)
        binding.recyclerViewSearchSection.adapter = SectionAdapter(context!!, tempSectionList)
    }
}
