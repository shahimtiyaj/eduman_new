package com.netizen.eduman.view.fragment.semesterExam.remarks


import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentRemarksUpdateSelectionBinding
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.view.fragment.SectionDialogFragment
import com.netizen.eduman.viewModel.SemesterExamViewModel
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class RemarksUpdateSelectionFragment : Fragment() {

    private lateinit var binding: FragmentRemarksUpdateSelectionBinding
    private lateinit var semesterViewModel: SemesterExamViewModel
    private lateinit var studentViewModel: StudentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_remarks_update_selection,
            container,
            false
        )

        initViews()
        initObservables()
        studentViewModel.getSectionList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        semesterViewModel = ViewModelProviders.of(activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application))
            .get(SemesterExamViewModel::class.java)

        studentViewModel = ViewModelProviders.of(activity!!,
            StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application))
            .get(StudentViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        semesterViewModel.classConfigId = null
    }

    private fun initViews() {

        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.sectionSelect.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("isStudent", true)

            val sectionDialogFragment = SectionDialogFragment()
            sectionDialogFragment.arguments = bundle
            sectionDialogFragment.show(activity!!.supportFragmentManager, null)
        }

        binding.spinnerExam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    semesterViewModel.getExamIdInRemarks(parent!!.getItemAtPosition(position).toString())
                } else {
                    semesterViewModel.examId = null
                }
            }
        }

        binding.buttonFind.btnFind.setOnClickListener {
            semesterViewModel.checkRemarksUpdate()
        }
    }

    private fun initObservables() {

        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerSection.visibility = View.VISIBLE
            } else {
                binding.shimmerSection.visibility = View.GONE
            }
        })

        Loaders.isLoading3.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerExam.visibility = View.VISIBLE
            } else {
                binding.shimmerExam.visibility = View.GONE
            }
        })

        semesterViewModel.isSectionEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.textViewSection.error = "Empty"
                Toasty.error(context!!, getString(R.string.select_section), Toasty.LENGTH_LONG).show()
            }
        })

        semesterViewModel.isExamEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it && binding.spinnerExam.selectedView != null) {
                val errorTextView = binding.spinnerExam.selectedView as TextView
                errorTextView.error = "Empty"
                Toasty.error(context!!, getString(R.string.select_exam), Toasty.LENGTH_LONG).show()
            }
        })

        Loaders.sectionName.observe(viewLifecycleOwner, Observer { sectionName ->
            if (!sectionName.isNullOrEmpty()) {
                if (!sectionName.equals("Select Section", true)) {
                    semesterViewModel.isSectionEmpty.value = false
                    binding.textViewSection.error = null
                } else {
                    semesterViewModel.classConfigId = null
                }

                semesterViewModel.sectionName.postValue(sectionName)
                binding.textViewSection.text = sectionName
                semesterViewModel.getSectionIdInRemarks(sectionName)
            }
        })

        studentViewModel.sectionList.observe(viewLifecycleOwner, Observer { sectionList ->
            if (!sectionList.isNullOrEmpty()) {
                semesterViewModel.sectionList.value = sectionList
            }
        })

        semesterViewModel.tempExamList.observe(viewLifecycleOwner, Observer { tempExamList ->
            if (!tempExamList.isNullOrEmpty()) {
                setExamSpinner(tempExamList)
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()){
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                findNavController().popBackStack()
                Loaders.apiSuccess.value = null
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        semesterViewModel.isListFound.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                findNavController().navigate(R.id.action_remarksUpdateSelectionFragment_to_remarkUpdateFragment)
                semesterViewModel.isListFound.value = false
            }
        })
    }

    private fun setExamSpinner(examTempList: List<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            examTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerExam.adapter = sectionAdapter
    }
}
