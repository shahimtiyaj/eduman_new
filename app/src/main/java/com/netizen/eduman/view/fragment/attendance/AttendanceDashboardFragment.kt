package com.netizen.eduman.view.fragment.attendance

import android.app.Application
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentAttendanceDashboardBinding
import com.netizen.eduman.viewModel.DashboardViewModel

class AttendanceDashboardFragment : Fragment() {
    private var dashboardViewModel: DashboardViewModel? = null
    private lateinit var binding: FragmentAttendanceDashboardBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_attendance_dashboard, container, false)
        dashboardViewModel = ViewModelProviders.of(this, DashboardViewModel.DashboardFactory(context!!.applicationContext as Application)).get(DashboardViewModel::class.java)

        initViews()

        onClick()

        return binding.root
    }

    private fun initViews(){
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        dashboardViewModel?.instituteInfoMutableLiveData!!.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.textViewSchoolName.text = it[0].instituteName
                binding.textViewSchoolAddress.text = it[0].instituteAddress
                binding.textViewInstituteId.text = it[0].instituteId
                binding.textViewAcademicYear.text = it[0].academic_year

                if (!it[0].instituteLogo.isNullOrEmpty()) {
                    Glide.with(context)
                        .load(Base64.decode(it[0].instituteLogo, Base64.DEFAULT))
                        .asBitmap()
                        .placeholder(R.drawable.ic_boy_icon)
                        .into(binding.schoolLogo)
                }
            }
        })

    }

    private fun onClick(){
        binding.layoutReport.setOnClickListener {
            findNavController().navigate(R.id.action_attendanceDashboardFragment_to_attendanceReportFragment)
        }

        binding.layoutAttendance.setOnClickListener {
            findNavController().navigate(R.id.action_attendanceDashboardFragment_to_searchTakeAttendanceStdFragment)
        }
    }
}
