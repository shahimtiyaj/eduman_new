package com.netizen.eduman.view.fragment.hrManagement

import android.app.Application
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.adapter.HRListAdapter
import com.netizen.eduman.databinding.HrRecylerListviewBinding
import com.netizen.eduman.model.HrReport
import com.netizen.eduman.viewModel.HRViewModel
import es.dmoral.toasty.Toasty

class HrReportFragment : Fragment() {
    private lateinit var binding: HrRecylerListviewBinding
    private lateinit var hrViewModel: HRViewModel
    private lateinit var layoutManager: RecyclerView.LayoutManager

    //private lateinit var hrReportList: List<HrReport.HRreportData?>
     var hrReportList = ArrayList<HrReport.HRreportData?>()

    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hrViewModel = ViewModelProviders.of(this, HRViewModel.HRViewModelFactory(context?.applicationContext as Application)).get(HRViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.hr_recyler_listview, container, false)
        binding.lifecycleOwner = this

        initViews()
        initObservables()
        hrViewModel. postHrReportGetData()

        return binding.root
    }

    private fun initObservables() {
        mHandler = Handler()
        layoutManager = LinearLayoutManager(context)

        hrViewModel.hrReportList.observe(viewLifecycleOwner, Observer { hrReportList ->
            if (hrReportList.isNotEmpty()) {
                this.hrReportList = hrReportList as ArrayList<HrReport.HRreportData?>
                setAdapter(hrReportList)
            }
            binding.totalFound.text= hrReportList.size.toString()
        })

        hrViewModel.isLoading.observe(viewLifecycleOwner, Observer {

            if (it!= null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        hrViewModel.apiError.observe(viewLifecycleOwner, Observer {
            Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
        })
    }

    private fun setAdapter(hrReportList: List<HrReport.HRreportData?>?) {
        binding.allHrRecylerListId.setHasFixedSize(true)
        binding.allHrRecylerListId.layoutManager = LinearLayoutManager(context)
        binding.allHrRecylerListId.adapter = hrReportList?.let { HRListAdapter(context!!, it) }

        binding.swipeRefreshLayout.setOnRefreshListener {
            mRunnable = Runnable {
                binding.allHrRecylerListId.adapter?.notifyDataSetChanged()
                hrReportList
                binding.swipeRefreshLayout.isRefreshing = false
            }
            mHandler.postDelayed(
                mRunnable, 50
            )
        }
    }

    private fun initViews() {

        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.imageViewSearch.setOnClickListener {
            binding.layoutSearch.visibility = View.VISIBLE
            binding.toolbarTitle.visibility = View.GONE
            binding.imageViewSearch.visibility = View.GONE
        }

        binding.imageViewSearchClose.setOnClickListener {
            binding.layoutSearch.visibility = View.GONE
            binding.toolbarTitle.visibility = View.VISIBLE
            binding.imageViewSearch.visibility = View.VISIBLE
        }

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempHrReportList = ArrayList<HrReport.HRreportData>()

                hrReportList.forEach { hrReport ->
                    if (hrReport!!.getHr_id().toString().toLowerCase().contains(s.toString(), true)) {
                        tempHrReportList.add(hrReport)
                    }
                }

                setAdapter(tempHrReportList)
            }
        })
    }
}
