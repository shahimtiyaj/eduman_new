package com.netizen.eduman.view.fragment.student

import android.app.Application
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentStudentDashBoardBinding
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.viewModel.DashboardViewModel

class StudentDashBoardFragment : Fragment() {

    private var dashboardViewModel: DashboardViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentStudentDashBoardBinding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_student_dash_board, container, false)
        binding.lifecycleOwner = this

        val preferences = AppPreferences(context)
        preferences.setAcademicYearPosition(0)
        preferences.setSectionPosition(0)
        preferences.setGroupPosition(0)
        preferences.setCategoryPosition(0)
        preferences.setIdTypePosition(0)

        dashboardViewModel = ViewModelProviders.of(
            this, DashboardViewModel.DashboardFactory(context!!.applicationContext as Application)
        ).get(DashboardViewModel::class.java)

        dashboardViewModel?.instituteInfoMutableLiveData!!.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.textViewSchoolName.text = it[0].instituteName
                binding.textViewSchoolAddress.text = it[0].instituteAddress
                binding.textViewInstituteId.text = it[0].instituteId
                binding.textViewAcademicYear.text = it[0].academic_year

                if (!it[0].instituteLogo.isNullOrEmpty()) {
                    Glide.with(context)
                        .load(Base64.decode(it[0].instituteLogo, Base64.DEFAULT))
                        .asBitmap()
                        .placeholder(R.drawable.ic_boy_icon)
                        .into(binding.schoolLogo)
                }
            }
        })

        binding.layoutEnrollment.setOnClickListener {
            findNavController().navigate(R.id.action_studentDashBoardFragment_to_firstStudentEnrollmentFragment)
        }

        binding.layoutReport.setOnClickListener {
            findNavController().navigate(R.id.action_studentDashBoardFragment_to_searchReportFragment)
        }

        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        return binding.root
    }
}
