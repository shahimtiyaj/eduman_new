package com.netizen.eduman.view.fragment.student


import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentStudentEnrollmentSecondBinding
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class StudentEnrollmentSecondFragment : Fragment() {

    private lateinit var binding: FragmentStudentEnrollmentSecondBinding
    private lateinit var studentViewModel: StudentViewModel

    private var bundleArguments: StudentEnrollmentSecondFragmentArgs? = null

    private var gender: String? = null
    private var religion: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_student_enrollment_second, container, false)

        bundleArguments =
            StudentEnrollmentSecondFragmentArgs.fromBundle(
                arguments!!
            )

        initViews()
        initObservables()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        studentViewModel = ViewModelProviders.of(this,
            StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application))
            .get(StudentViewModel::class.java)
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.spinnerGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    gender = parent!!.getItemAtPosition(position).toString()
                }
            }
        }

        binding.spinnerReligion.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    religion = parent!!.getItemAtPosition(position).toString()
                }
            }
        }

        binding.editTextContactNumber.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.toString().startsWith("01", true)) {
                    binding.editTextContactNumber.error = getString(R.string.err_msg_phone_invalid)
                }
            }
        })

        binding.buttonSave.setOnClickListener {
            checkValues()
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.progressBar.visibility = View.VISIBLE
            } else{
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showErrorToasty(it.toString())
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                findNavController().popBackStack()
                clearData()
            }
        })
    }

    private fun checkValues() {
        val errorTextView: TextView

        when {
            binding.editTextStudentName.text.isNullOrEmpty() -> {
                binding.editTextStudentName.error = getString(R.string.err_msg_name2)
            }
            gender.isNullOrEmpty() -> {
                errorTextView = binding.spinnerGender.selectedView as TextView
                errorTextView.error = "Empty"
                showErrorToasty(getString(R.string.select_gender))
            }
            religion.isNullOrEmpty() -> {
                errorTextView = binding.spinnerReligion.selectedView as TextView
                errorTextView.error = "Empty"
                showErrorToasty(getString(R.string.select_religion))
            }
            binding.editTextFatherName.text.isNullOrEmpty() -> {
                binding.editTextFatherName.error = getString(R.string.err_mssg_fname)
            }
            binding.editTextMotherName.text.isNullOrEmpty() -> {
                binding.editTextMotherName.error = getString(R.string.err_mssg_mname)
            }
            binding.editTextContactNumber.text.isNullOrEmpty() || binding.editTextContactNumber.text.length != 11 -> {
                binding.editTextContactNumber.error = getString(R.string.hint_mobile)
            }
            else -> {
                val jsonObject = JsonObject()
                val arrayItem = JsonObject()
                val jsonArray = JsonArray()

                try {
                    arrayItem.addProperty("academicSession", "2017-2018")
                    arrayItem.addProperty("studentDOB", "1800-01-01")
                    arrayItem.addProperty("bloodGroup", "---")
                    arrayItem.addProperty("customStudentId", bundleArguments?.studentId)
                    arrayItem.addProperty("fatherName", binding.editTextFatherName.text.toString())
                    arrayItem.addProperty("motherName", binding.editTextMotherName.text.toString())
                    arrayItem.addProperty("studentGender", gender)
                    arrayItem.addProperty("studentName", binding.editTextStudentName.text.toString())
                    arrayItem.addProperty("studentReligion", religion)
                    arrayItem.addProperty("studentRoll", bundleArguments?.rollNumber)
                    arrayItem.addProperty("guardianMobile", binding.editTextContactNumber.text.toString())

                    jsonArray.add(arrayItem)

                    jsonObject.addProperty("academicYear", bundleArguments?.academicYear)
                    jsonObject.addProperty("classConfigId", bundleArguments?.section)
                    jsonObject.addProperty("groupId", bundleArguments?.group)
                    jsonObject.addProperty("studentCategoryId", bundleArguments?.category)
                    jsonObject.add("studentRequestHelpers", jsonArray)

                    studentViewModel.saveEnrollmentData(bundleArguments!!.idType, jsonObject)
                } catch (e: Exception) {
                    showErrorToasty("Couldn't save enrollment data!")
                }
            }
        }
    }

    private fun clearData() {
        binding.editTextStudentName.text = null
        gender = null
        religion = null
        binding.editTextFatherName.text = null
        binding.editTextMotherName.text = null
        binding.editTextContactNumber.text = null
    }

    private fun showErrorToasty(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_SHORT).show()
    }

    companion object {
        private const val TAG = "SECOND_ENROLLMENT"
    }
}
