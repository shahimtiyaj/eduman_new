package com.netizen.eduman.view.fragment.student


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentStudentReportDetailBinding
import com.netizen.eduman.model.StudentReportResponse

/**
 * A simple [Fragment] subclass.
 */
class StudentReportDetailFragment : DialogFragment() {

    private lateinit var binding: FragmentStudentReportDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_student_report_detail, container, false)

//        val bundleArgs = arguments
        if (arguments != null) {
            setValues(arguments!!.get("report_details") as StudentReportResponse.StudentReport)
        }

        binding.imageViewClose.setOnClickListener {
            dismiss()
        }

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog!!.window!!.attributes.windowAnimations = R.style.VerticalDialogAnimation
    }

    private fun setValues(studentReport: StudentReportResponse.StudentReport) {
        binding.textViewName.text = studentReport.getStudentName()
        binding.textViewId.text = studentReport.getStudentId()
        binding.textViewRoll.text = studentReport.getStudentRoll().toString()
        binding.textViewGroup.text = studentReport.getGroupName()
        binding.textViewCategory.text = studentReport.getStudentCategoryName()
        binding.textViewGender.text = studentReport.getStudentGender()
        binding.textViewReligion.text = studentReport.getStudentReligion()
        if (studentReport.getStringDateOfBirth().toString()=="null"){ binding.textViewDateOfBirth.text = "---" }
        else{ binding.textViewDateOfBirth.text = studentReport.getStringDateOfBirth().toString() }
        if (studentReport.getBloodGroup().toString()=="null"){ binding.textViewBloodGroup.text = "---" }
        else{ binding.textViewBloodGroup.text = studentReport.getBloodGroup().toString() }
        binding.textViewFatherName.text = studentReport.getFatherName()
        binding.textViewMotherName.text = studentReport.getMotherName()
        binding.textViewMobile.text = studentReport.getMobileNo().toString()
    }
}
