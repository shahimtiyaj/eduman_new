package com.netizen.eduman.view.fragment.semesterExam.examMarks


import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.MarkInputAdapter
import com.netizen.eduman.adapter.MarkScaleAdapter
import com.netizen.eduman.adapter.MarkUpdateAdapter
import com.netizen.eduman.databinding.FragmentMarkInputBinding
import com.netizen.eduman.model.ExamMarkInput
import com.netizen.eduman.model.MarkInputResponse
import com.netizen.eduman.model.MarkScaleResponse
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.SemesterExamViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class MarkInputFragment : Fragment(), MarkInputAdapter.OnSaveButtonClick, MarkUpdateAdapter.OnSaveButtonClick {

    private lateinit var binding: FragmentMarkInputBinding
    private lateinit var semesterViewModel: SemesterExamViewModel

    private var markInputList=ArrayList<MarkInputResponse.MarkInput>()
    private var isMarkInputFragment: Boolean? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_mark_input, container, false)

        val args =
            MarkInputFragmentArgs.fromBundle(
                arguments!!
            )
        isMarkInputFragment = args.isMarkInputFragment
        if (isMarkInputFragment!!) {
            binding.toolbarTitle.text = "Mark Input Form"
        } else {
            binding.toolbarTitle.text = "Mark Update Form"
        }

        initViews()
        initObservables()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        semesterViewModel = ViewModelProviders.of(
            activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application)
        ).get(SemesterExamViewModel::class.java)
        MarkInputAdapter.markInputRqHelperList.clear()
    }

    override fun onSave(markInputRqHelperList: List<ExamMarkInput.ExamMarkInputRqHelper>) {
        semesterViewModel.saveInputMark(markInputRqHelperList)
    }

    override fun onUpdate(markInputRqHelperList: List<ExamMarkInput.ExamMarkInputRqHelper>) {
        semesterViewModel.updateInputMark(markInputRqHelperList)
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.textViewShowMarkScale.setOnClickListener {
            if (binding.layoutMarkScaleTable.isVisible) {
                binding.layoutMarkScaleTable.visibility = View.GONE
                binding.textViewShowMarkScale.text = "Show Mark Scale"
                binding.textViewShowMarkScale.setCompoundDrawablesWithIntrinsicBounds(null, null, context!!.resources.getDrawable(R.drawable.ic_keyboard_arrow_down_blue_24dp), null)
            } else {
                binding.layoutMarkScaleTable.visibility = View.VISIBLE
                binding.textViewShowMarkScale.text = "Hide Mark Scale"
                binding.textViewShowMarkScale.setCompoundDrawablesWithIntrinsicBounds(null, null, context!!.resources.getDrawable(R.drawable.ic_keyboard_arrow_up_blue_24dp), null)
            }
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                MarkInputAdapter.markInputRqHelperList.clear()
                findNavController().popBackStack()
            }
        })

        semesterViewModel.markInputList.observe(viewLifecycleOwner, Observer { markInputList ->
            this.markInputList = markInputList as ArrayList<MarkInputResponse.MarkInput>
        })

        semesterViewModel.markScaleList.observe(viewLifecycleOwner, Observer { markScaleList ->
            setMarkScale(markScaleList)

            if (isMarkInputFragment!!) {
                setMarkInputList(markScaleList)
            } else {
                setMarkUpdateList(markScaleList)
            }
        })
    }

    private fun setMarkInputList(markScaleList: List<MarkScaleResponse.MarkScale>) {
        binding.recyclerViewMarkInput.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewMarkInput.setHasFixedSize(true)

        val markInputAdapter = MarkInputAdapter(context!!, markInputList, markScaleList, this)
        binding.recyclerViewMarkInput.adapter = markInputAdapter
        markInputAdapter.notifyDataSetChanged()
    }

    private fun setMarkUpdateList(markScaleList: List<MarkScaleResponse.MarkScale>) {
        binding.recyclerViewMarkInput.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewMarkInput.setHasFixedSize(true)

        val markInputAdapter = MarkUpdateAdapter(context!!, markInputList, markScaleList, this)
        binding.recyclerViewMarkInput.adapter = markInputAdapter
        markInputAdapter.notifyDataSetChanged()
    }

    private fun setMarkScale(markScaleList: List<MarkScaleResponse.MarkScale>) {
        binding.recyclerViewMarkScale.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewMarkScale.setHasFixedSize(true)

        val markScaleAdapter = MarkScaleAdapter(context!!, markScaleList)
        binding.recyclerViewMarkScale.adapter = markScaleAdapter
        markScaleAdapter.notifyDataSetChanged()
    }
}
