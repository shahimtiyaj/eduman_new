package com.netizen.eduman.view.fragment.attendance.studentAttendance


import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.StudentAttendanceSummaryReportAdapter
import com.netizen.eduman.databinding.FragmentStudentAttendanceReportSummaryBinding
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.AttendanceViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class StudentAttendanceReportSummaryFragment : Fragment(), StudentAttendanceSummaryReportAdapter.OnNextClick {

    private lateinit var binding: FragmentStudentAttendanceReportSummaryBinding
    private lateinit var attendanceViewModel: AttendanceViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_student_attendance_report_summary,
            container,
            false)

//        val args =
//            StudentAttendanceReportSummaryFragmentArgs.fromBundle(
//                arguments!!
//            )

        initViews()
        initObservables()

//        attendanceViewModel.getStudentAttendanceReportSummaryList(args.date, args.periodId)

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attendanceViewModel = ViewModelProviders.of(activity!!,
            AttendanceViewModel.AttendanceViewModelFactory(context!!.applicationContext as Application))
            .get(AttendanceViewModel::class.java)
    }

    override fun showDetails(
        sectionName: String,
        classConfigId: String,
        attendanceDate: String,
        status: String,
        header: String
    ) {
        findNavController().navigate(
            StudentAttendanceReportSummaryFragmentDirections.actionStudentAttendanceReportSummaryFragmentToStudentAttendanceReportFragment(
                sectionName,
                classConfigId,
                attendanceDate,
                header,
                status
            )
        )
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it!= null && it)  {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()){
                Toasty.error(context!!, it, Toasty.LENGTH_SHORT).show()
                Loaders.apiError.value = null
            }
        })

        attendanceViewModel.studentAttendanceReportSummaryList.observe(viewLifecycleOwner, Observer { attendanceReportSummaryList ->
            if (!attendanceReportSummaryList.isNullOrEmpty()) {
                binding.recyclerViewSummaryReport.layoutManager = LinearLayoutManager(context!!)
                binding.recyclerViewSummaryReport.setHasFixedSize(true)
                binding.recyclerViewSummaryReport.adapter = StudentAttendanceSummaryReportAdapter(context!!,
                    attendanceReportSummaryList,
                    this)
            }
        })
    }

    companion object {
        private val TAG = StudentAttendanceReportSummaryFragment::class.java.simpleName
    }
}
