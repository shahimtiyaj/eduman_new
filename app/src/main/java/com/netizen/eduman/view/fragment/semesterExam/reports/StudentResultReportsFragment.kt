package com.netizen.eduman.view.fragment.semesterExam.reports

import android.app.Application
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.StudentResultReportsAdapter
import com.netizen.eduman.databinding.FragmentStudentResultReportsListviewBinding
import com.netizen.eduman.model.StudentResultReportResponse
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.SemesterExamViewModel
import es.dmoral.toasty.Toasty

class StudentResultReportsFragment : Fragment() {
    private lateinit var binding: FragmentStudentResultReportsListviewBinding
    private lateinit var semesterExamViewModel: SemesterExamViewModel
    private  var reportList=ArrayList<StudentResultReportResponse.StudentReport>()
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        semesterExamViewModel = ViewModelProviders.of(activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application))
            .get(SemesterExamViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=DataBindingUtil.inflate(inflater, R.layout.fragment_student_result_reports_listview, container, false)

        initViews()
        initObservables()

       // val bundleArgs = StudentReportListFragmentArgs.fromBundle(arguments!!)
      //  binding.textViewSectionName.text = bundleArgs.sectionName.replace("-", " > ")

       // val examId=semesterExamViewModel.resultReportexamId.toString()
        //semesterExamViewModel.getstudentResultReportsList(bundleArgs.sectionId, examId)

        return binding.root
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.imageViewSearch.setOnClickListener {
            binding.layoutSearch.visibility = View.VISIBLE
            binding.toolbarTitle.visibility = View.GONE
            binding.imageViewSearch.visibility = View.GONE
        }

        binding.imageViewSearchClose.setOnClickListener {
            binding.layoutSearch.visibility = View.GONE
            binding.toolbarTitle.visibility = View.VISIBLE
            binding.imageViewSearch.visibility = View.VISIBLE
        }

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempReportList = ArrayList<StudentResultReportResponse.StudentReport>()
                reportList.forEach { report ->
                    if (report.getCustomStudentId()!!.toLowerCase().contains(s.toString(), true)) {
                        tempReportList.add(report) }
                }
                setAdapter(tempReportList)
            }
        })
    }


    private fun initObservables() {
        mHandler = Handler()

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        semesterExamViewModel.sectionName.observe(viewLifecycleOwner, Observer { sectionName ->
            if (!sectionName.isNullOrEmpty()) {
                binding.textViewSectionName.text = sectionName.replace("-", " > ")
            }
        })


        semesterExamViewModel.studentResultReportList.observe(viewLifecycleOwner, Observer { reportList ->
            if (reportList.isNotEmpty()) {
                this.reportList = reportList as ArrayList<StudentResultReportResponse.StudentReport>
                setAdapter(reportList)
                binding.totalFound.text= reportList.size.toString()
            }
        })
    }

    private fun setAdapter(reportList: List<StudentResultReportResponse.StudentReport>?) {
        binding.resultRecylerListId.setHasFixedSize(true)
        binding.resultRecylerListId.layoutManager = LinearLayoutManager(context)
        binding.resultRecylerListId.adapter = reportList?.let { StudentResultReportsAdapter(context!!, it) }

        binding.swipeRefreshLayout.setOnRefreshListener {
            mRunnable = Runnable {
                binding.resultRecylerListId.adapter?.notifyDataSetChanged()
                reportList
                binding.swipeRefreshLayout.isRefreshing = false
            }
            mHandler.postDelayed(
                mRunnable, 50
            )
        }
    }
}
