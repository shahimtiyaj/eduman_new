package com.netizen.eduman.view.fragment.student

import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.StudentReportAdapter
import com.netizen.eduman.databinding.FragmentStudentReportListBinding
import com.netizen.eduman.model.StudentReportResponse
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class StudentReportListFragment : Fragment(), StudentReportAdapter.OnItemClick {

    private lateinit var binding: FragmentStudentReportListBinding
    private lateinit var studentViewModel: StudentViewModel

    private  var reportList=ArrayList<StudentReportResponse.StudentReport>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_student_report_list,
            container,
            false
        )
        binding.lifecycleOwner = this

        val bundleArgs =
            StudentReportListFragmentArgs.fromBundle(
                arguments!!
            )
        binding.textViewSectionName.text = bundleArgs.sectionName.replace("-", " > ")

        initViews()
        initObservables()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        studentViewModel = ViewModelProviders.of(
            activity!!,
            StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application)
        )
            .get(StudentViewModel::class.java)
    }

    override fun goToDetailsFragment(studentReport: StudentReportResponse.StudentReport) {
        val bundle = Bundle()
        bundle.putParcelable("report_details", studentReport)

        val reportDetailFragment =
            StudentReportDetailFragment()
        reportDetailFragment.arguments = bundle
        reportDetailFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.imageViewSearch.setOnClickListener {
            binding.layoutSearch.visibility = View.VISIBLE
            binding.toolbarTitle.visibility = View.GONE
            binding.imageViewSearch.visibility = View.GONE
        }

        binding.imageViewSearchClose.setOnClickListener {
            binding.layoutSearch.visibility = View.GONE
            binding.toolbarTitle.visibility = View.VISIBLE
            binding.imageViewSearch.visibility = View.VISIBLE
        }

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempReportList = ArrayList<StudentReportResponse.StudentReport>()

                reportList.forEach { report ->
                    if (!report.getCustomStudentId().isNullOrEmpty() &&
                        report.getCustomStudentId()!!.contains(s.toString(), true)) {
                        tempReportList.add(report)
                    }
                }

                setAdapter(tempReportList)
            }
        })
    }

    private fun initObservables() {

        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()){
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value=null
            }
        })

        studentViewModel.studentReportList.observe(viewLifecycleOwner, Observer { reportList ->
            if (reportList.isNotEmpty()) {
                this.reportList = reportList as ArrayList<StudentReportResponse.StudentReport>
                setAdapter(reportList)
            }
        })
    }

    private fun setAdapter(reportList: List<StudentReportResponse.StudentReport>?) {
        binding.recyclerViewReport.setHasFixedSize(true)
        binding.recyclerViewReport.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewReport.adapter =
            reportList?.let { StudentReportAdapter(context!!, it, this) }
    }
}
