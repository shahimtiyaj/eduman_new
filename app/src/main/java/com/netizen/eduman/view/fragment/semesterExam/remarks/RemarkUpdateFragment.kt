package com.netizen.eduman.view.fragment.semesterExam.remarks


import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.RemarksUpdateAdapter
import com.netizen.eduman.databinding.FragmentRemarkUpdateBinding
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.SemesterExamViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class RemarkUpdateFragment : Fragment() {

    private lateinit var binding: FragmentRemarkUpdateBinding
    private lateinit var semesterViewModel: SemesterExamViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_remark_update,
            container,
            false
        )

        initViews()
        initObservables()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        semesterViewModel = ViewModelProviders.of(activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application))
            .get(SemesterExamViewModel::class.java)
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
            }
        })

        semesterViewModel.sectionName.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.textViewSectionName.text = it.toString().replace("-", " > ")
            }
        })

        semesterViewModel.isRemarkListFound.observe(viewLifecycleOwner, Observer {
            if (it != null && !it) {
                Toasty.error(context!!, "No remarks is checked to update!", Toasty.LENGTH_LONG).show()
                semesterViewModel.isRemarkListFound.value = null
            }
        })

        semesterViewModel.remarksUpdateList.observe(viewLifecycleOwner, Observer { remarksUpdateList ->
            if (!remarksUpdateList.isNullOrEmpty()) {
                binding.textViewTotalFound.text = "Total Found: " + remarksUpdateList.size.toString()

                binding.recyclerViewRemarks.layoutManager = LinearLayoutManager(context)
                binding.recyclerViewRemarks.setHasFixedSize(true)
                binding.recyclerViewRemarks.adapter = RemarksUpdateAdapter(context!!, remarksUpdateList, semesterViewModel)
            }
        })
    }
}
