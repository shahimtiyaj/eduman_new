package com.netizen.eduman.view.fragment.attendance.studentAttendance

import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.AttendanceAdapter
import com.netizen.eduman.databinding.TakeAttendanceLayoutListBinding
import com.netizen.eduman.model.TakeAtdStudentPost
import com.netizen.eduman.model.TakeAtdStudentResponse
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.utils.Loaders.Companion.isLoading
import com.netizen.eduman.utils.MyUtilsClass
import com.netizen.eduman.viewModel.AttendanceViewModel
import es.dmoral.toasty.Toasty


class TakeAttendanceStudentFragment : Fragment(), AttendanceAdapter.OnSaveButtonClick {

    private lateinit var binding: TakeAttendanceLayoutListBinding
    private lateinit var attendanceViewModel: AttendanceViewModel
    private var takAdtStudentList = ArrayList<TakeAtdStudentResponse.TakAtdStudent>()

    private var smsAbsent: Int?=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        attendanceViewModel =
            ViewModelProviders.of(
                activity!!,
                AttendanceViewModel.AttendanceViewModelFactory(context!!.applicationContext as Application)
            ).get(AttendanceViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.take_attendance_layout_list,
            container,
            false
        )

        initViews()
        initObservables()

        return binding.root
    }

    override fun onSave(isChecked: Boolean) {
        val takeAttendanceList = TakeAtdStudentPost()
        if (isChecked) {
            takeAttendanceList.setSmsSendingStatus(1)
        } else {
            takeAttendanceList.setSmsSendingStatus(0)
        }

        val ids = ArrayList<String>()

        for (attend in takAdtStudentList) {
            if (attend.getSelected()) {
                ids.add(attend.getIdentificationId().toString())
            }
        }

        takeAttendanceList.setIdentificationIds(ids)
        takeAttendanceList.setAttendanceDate(MyUtilsClass.postDateFormatForTakeAtdStdData(attendanceViewModel.date!!))
        takeAttendanceList.setClassConfigId(attendanceViewModel.sectionId)
        takeAttendanceList.setPeriodId(attendanceViewModel.periodId)

        attendanceViewModel.postTakeAttendanceData(takeAttendanceList)
    }

    private fun initObservables() {

        isLoading.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        attendanceViewModel.sectionName.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                binding.textViewSectionName.text = it.replace("-", " > ")
            }
        })

        attendanceViewModel.takAdtStudentList.observe(
            viewLifecycleOwner,
            Observer { studentTakeAtdList ->
                if (!studentTakeAtdList.isNullOrEmpty()) {
                    this.takAdtStudentList =
                        studentTakeAtdList as ArrayList<TakeAtdStudentResponse.TakAtdStudent>
                    setAdapter(takAdtStudentList)
                    binding.totalFound.text= studentTakeAtdList.size.toString()
                }
            })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                findNavController().popBackStack()
                Loaders.apiSuccess.value = null
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })
    }

    private fun setAdapter(takeAtdReportList: List<TakeAtdStudentResponse.TakAtdStudent>?) {
        binding.allAttendanceRecylerListId.setHasFixedSize(true)
        binding.allAttendanceRecylerListId.layoutManager = LinearLayoutManager(context)
        binding.allAttendanceRecylerListId.adapter = takeAtdReportList?.let { AttendanceAdapter(context!!, it, this) }
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.imageViewSearch.setOnClickListener {
            binding.layoutSearch.visibility = View.VISIBLE
            binding.toolbarTitle.visibility = View.GONE
            binding.imageViewSearch.visibility = View.GONE
        }

        binding.imageViewSearchClose.setOnClickListener {
            binding.layoutSearch.visibility = View.GONE
            binding.toolbarTitle.visibility = View.VISIBLE
            binding.imageViewSearch.visibility = View.VISIBLE
        }

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempTakeAtdReportList = ArrayList<TakeAtdStudentResponse.TakAtdStudent>()

                takAdtStudentList.forEach { takeAtdReport ->
                    if (takeAtdReport.getStudentRoll().toString().toLowerCase().contains(
                            s.toString(),
                            true
                        )
                    ) {
                        tempTakeAtdReportList.add(takeAtdReport)
                    }
                }

                setAdapter(tempTakeAtdReportList)
            }
        })
    }

}
