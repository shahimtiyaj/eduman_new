package com.netizen.eduman.view.fragment.student


import android.app.Application
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentStudentEnrollmentFirstBinding
import com.netizen.eduman.model.AcademicYearResponse
import com.netizen.eduman.model.CategoryResponse
import com.netizen.eduman.model.GroupResponse
import com.netizen.eduman.model.SectionResponse
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.view.fragment.SectionDialogFragment
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class StudentEnrollmentFirstFragment : Fragment() {

    private lateinit var binding: FragmentStudentEnrollmentFirstBinding
    private lateinit var studentViewModel: StudentViewModel

    private  var sectionList= ArrayList<SectionResponse.Section>()
    private  var categoryList= ArrayList<CategoryResponse.Category>()
    private  var groupList= ArrayList<GroupResponse.Group>()
    private lateinit var tempSectionList: ArrayList<String>

    private var academicYear: String? = null
    private var sectionId: String? = null
    private var groupId: String? = null
    private var categoryId: String? = null
    private var studentIdType: String? = null
    private var studentRollNumber: String? = null

    private lateinit var preferences: AppPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_student_enrollment_first,
            container,
            false
        )

        binding.lifecycleOwner = this
        preferences = AppPreferences(context)
        binding.spinnerIdType.setSelection(preferences.getIdTypePosition())

        initViews()
        initObservables()
        studentViewModel.getAllDataList()
        setGroupSpinner(emptyList())

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        studentViewModel = ViewModelProviders.of(
            activity!!,
            StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application)
        )
            .get(StudentViewModel::class.java)
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.textViewSection.setOnClickListener {
            if (!tempSectionList.isNullOrEmpty()) {
                val bundle = Bundle()
                bundle.putBoolean("isStudent", true)

                val sectionDialogFragment = SectionDialogFragment()
                sectionDialogFragment.arguments = bundle
                sectionDialogFragment.show(activity!!.supportFragmentManager, null)
            }
        }

        binding.imageViewSpinnerIcon.setOnClickListener {
            if (!tempSectionList.isNullOrEmpty()) {
                val bundle = Bundle()
                bundle.putBoolean("isStudent", true)

                val sectionDialogFragment = SectionDialogFragment()
                sectionDialogFragment.arguments = bundle
                sectionDialogFragment.show(activity!!.supportFragmentManager, null)
            }
        }

        binding.spinnerAcademicYear.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    academicYear = if (position == 0) {
                        null
                    } else {
                        preferences.setAcademicYearPosition(position)
                        parent!!.getItemAtPosition(position).toString()
                    }
                }
            }

        binding.spinnerGroup.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position == 0) {
                    groupId = null
                } else {
                    preferences.setGroupPosition(position)
                    getGroupId(parent!!.getItemAtPosition(position).toString())
                }
            }
        }

        binding.spinnerCategory.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position == 0) {
                        categoryId = null
                    } else {
                        preferences.setCategoryPosition(position)
                        getCategoryId(parent!!.getItemAtPosition(position).toString())
                    }
                }
            }

        binding.spinnerIdType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                preferences.setIdTypePosition(position)
                when (position) {
                    0 -> {
                        studentIdType = null
                        binding.layoutStudentId.visibility = View.GONE
                    }
                    1 -> {
                        studentIdType = "0"
                        binding.layoutStudentId.visibility = View.GONE
                    }
                    2 -> {
                        studentIdType = "1"
                        binding.layoutStudentId.visibility = View.VISIBLE
                    }
                }
            }
        }

        binding.buttonNext.setOnClickListener {
            studentRollNumber = binding.editTextRollNumber.text.toString()
            checkValues()
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
//                binding.progressBar.visibility = View.VISIBLE
                binding.shimmerAcademicYear.visibility = View.VISIBLE
                binding.shimmerSection.visibility = View.VISIBLE
                binding.shimmerCategory.visibility = View.VISIBLE
            } else {
//                binding.progressBar.visibility = View.GONE
                binding.shimmerAcademicYear.visibility = View.GONE
                binding.shimmerSection.visibility = View.GONE
                binding.shimmerCategory.visibility = View.GONE
            }
        })

        Loaders.isLoading2.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerGroup.visibility = View.VISIBLE
            } else {
                binding.shimmerGroup.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
            }
        })

        Loaders.sectionName.observe(viewLifecycleOwner, Observer { sectionName ->
            if (!sectionName.isNullOrEmpty()) {
                studentViewModel.sectionName.value = sectionName
            }
        })

        studentViewModel.sectionName.observe(viewLifecycleOwner, Observer { sectionName ->
            binding.textViewSection.text = sectionName

            if (sectionName.equals("Select Section", true)) {
                sectionId = null
            } else {
                binding.textViewSection.error = null
                getSectionId(sectionName)
            }
        })

        studentViewModel.academicYearList.observe(
            viewLifecycleOwner,
            Observer { academicYearList ->
                if (!academicYearList.isNullOrEmpty()) {
//                Log.e("ACADEMIC YEAR LIST SIZE", academicYearList.size.toString())
                    setAcademicYearSpinner(academicYearList)
                }
            })

        studentViewModel.tempSectionList.observe(viewLifecycleOwner, Observer { tempSectionList ->
            if (!tempSectionList.isNullOrEmpty()) {
                this.tempSectionList = tempSectionList
            }
        })

        studentViewModel.sectionList.observe(viewLifecycleOwner, Observer { sectionList ->
            if (!sectionList.isNullOrEmpty()) {
                this.sectionList = sectionList as ArrayList<SectionResponse.Section>
//                setSectionSpinner(sectionList)
            }
        })

        studentViewModel.groupList.observe(viewLifecycleOwner, Observer { groupList ->
            if (!groupList.isNullOrEmpty()) {
                this.groupList = groupList as ArrayList<GroupResponse.Group>
                setGroupSpinner(groupList)
            }
        })

        studentViewModel.categoryList.observe(
            viewLifecycleOwner,
            Observer { categoryList ->
                if (!categoryList.isNullOrEmpty()) {
                    this.categoryList = categoryList as ArrayList<CategoryResponse.Category>
                    setCategorySpinner(categoryList)
                }
            })
    }

    private fun setAcademicYearSpinner(academicYearList: List<AcademicYearResponse.AcademicYear>) {
        val academicYearTempList = ArrayList<String>()
        academicYearTempList.add("Select Academic Year")

        academicYearList.forEach { academicYear ->
            academicYearTempList.add(academicYear.getName().toString())
        }

        val academicAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
//            R.layout.layout_spinner_item,
            academicYearTempList)
        academicAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        academicAdapter.setDropDownViewResource(R.layout.layout_spinner_item)
        binding.spinnerAcademicYear.adapter = academicAdapter
        if (academicYearTempList.size > 1) {
            binding.spinnerAcademicYear.setSelection(preferences.getAcademicYearPosition())
        }
    }

    private fun getSectionId(sectionName: String) {
        sectionList.forEach { section ->
            if (section.getClassShiftSection().equals(sectionName, true)) {
                sectionId = section.getClassConfigId().toString()
                studentViewModel.getGroupList(sectionId!!)
                return
            }
        }
    }

    private fun setCategorySpinner(categoryList: List<CategoryResponse.Category>) {
        val categoryTempList = ArrayList<String>()
        categoryTempList.add("Select Category")

        categoryList.forEach { category ->
            categoryTempList.add(category.getName().toString())
        }

        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            categoryTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerCategory.adapter = sectionAdapter

        if (categoryTempList.size > 1) {
            binding.spinnerCategory.setSelection(preferences.getCategoryPosition())
        }
    }

    private fun setGroupSpinner(groupList: List<GroupResponse.Group>) {
        val groupTempList = ArrayList<String>()
        groupTempList.add("Select Group")
        Log.e("GROUP_LIST", groupList.size.toString())

        if (!groupList.isNullOrEmpty()) {
            groupList.forEach { group ->
                groupTempList.add(group.getGroupObject()!!.getName().toString())
            }
        }

        val groupAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            groupTempList
        )
        groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerGroup.adapter = groupAdapter

        if (groupTempList.size > 1) {
            binding.spinnerGroup.setSelection(preferences.getGroupPosition())
        }
    }

    private fun getGroupId(groupItem: String) {
        groupList.forEach { group ->
            if (group.getGroupObject()!!.getName().equals(groupItem, true)) {
                groupId = group.getGroupObject()!!.getId().toString()
            }
        }
    }

    private fun getCategoryId(categoryItem: String) {
        categoryList.forEach { category ->
            if (category.getName().equals(categoryItem, true)) {
                categoryId = category.getId().toString()
            }
        }
    }

    private fun checkValues() {
        val errorTextView: TextView

        when {
            academicYear.isNullOrEmpty() -> {
                errorTextView = binding.spinnerAcademicYear.selectedView as TextView
                errorTextView.error = "Empty"
                showErrorToasty(getString(R.string.select_session))
            }
            sectionId.isNullOrEmpty() -> {
                binding.textViewSection.error = "Empty"
                showErrorToasty(getString(R.string.select_section))
            }
            groupId.isNullOrEmpty() -> {
                errorTextView = binding.spinnerGroup.selectedView as TextView
                errorTextView.error = "Empty"
                showErrorToasty(getString(R.string.select_group))
            }
            categoryId.isNullOrEmpty() -> {
                errorTextView = binding.spinnerCategory.selectedView as TextView
                errorTextView.error = "Empty"
                showErrorToasty(getString(R.string.select_category))
            }
            studentIdType.isNullOrEmpty() -> {
                errorTextView = binding.spinnerIdType.selectedView as TextView
                errorTextView.error = "Empty"
                showErrorToasty(getString(R.string.select_student_id_type))
            }
            studentIdType == "1" && binding.editTextStudentId.text.isNullOrEmpty() -> {
                binding.editTextStudentId.error = getString(R.string.err_mssg_st_id)
            }
            studentRollNumber.isNullOrEmpty() -> {
                binding.editTextRollNumber.error = getString(R.string.err_mssg_st_roll)
            }

            else -> {
                val action =
                    StudentEnrollmentFirstFragmentDirections.actionFirstStudentEnrollmentFragmentToSecondStudentEnrollmentFragment(
                        academicYear!!,
                        sectionId!!,
                        groupId!!,
                        categoryId!!,
                        studentIdType!!,
                        studentRollNumber!!,
                        binding.editTextStudentId.text.toString()
                    )
                findNavController().navigate(action)
            }
        }
    }

    private fun showErrorToasty(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_SHORT).show()
    }
}
