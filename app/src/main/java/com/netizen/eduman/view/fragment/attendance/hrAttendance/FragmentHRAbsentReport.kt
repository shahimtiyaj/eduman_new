package com.netizen.eduman.view.fragment.attendance.hrAttendance

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.HrAttendanceAdapter
import com.netizen.eduman.databinding.HrAttendanceReportListBinding
import com.netizen.eduman.model.HRAttendanceReportsResponse
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.AttendanceViewModel
import es.dmoral.toasty.Toasty

class FragmentHRAbsentReport : Fragment() {
    private lateinit var binding: HrAttendanceReportListBinding
    private lateinit var attendanceViewModel: AttendanceViewModel
    private var hrAttdendanceReportsList = ArrayList<HRAttendanceReportsResponse.HRAttendanceReports>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attendanceViewModel = ViewModelProviders.of(this, AttendanceViewModel.
            AttendanceViewModelFactory(context!!.applicationContext as Application)).get(
            AttendanceViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.hr_attendance_report_list, container, false)

        //val args = FragmentHRPresentReportArgs.fromBundle(arguments!!)

        initObservables()
        attendanceViewModel.getHRAttendanceListReports(FragmentHRDateSelection.hrDateAttendance, "2") //01/11/2019

        return binding.root
    }

    private fun initObservables() {

        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        attendanceViewModel.hrAttendanceList.observe(
            viewLifecycleOwner,
            Observer { studentTakeAtdList ->
                if (!studentTakeAtdList.isNullOrEmpty()) {
                    this.hrAttdendanceReportsList = studentTakeAtdList as ArrayList<HRAttendanceReportsResponse.HRAttendanceReports>
                    setAdapter(hrAttdendanceReportsList)
                    binding.totalFound.text= studentTakeAtdList.size.toString()
                }
            })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })
    }

    private fun setAdapter(hrAttendanceList: List<HRAttendanceReportsResponse.HRAttendanceReports>) {
        binding.hrAttendanceRecylerListId.layoutManager = LinearLayoutManager(context)
        binding.hrAttendanceRecylerListId.setHasFixedSize(true)
        binding.hrAttendanceRecylerListId.adapter = HrAttendanceAdapter(context!!, hrAttendanceList)
    }
}
