package com.netizen.eduman.view.fragment.hrManagement

import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentHrEnlistmentBinding
import com.netizen.eduman.model.HRDesignation
import com.netizen.eduman.model.HREnlist
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.HRViewModel
import es.dmoral.toasty.Toasty

class HrEnlistmentFragment : Fragment() {

    private lateinit var binding: FragmentHrEnlistmentBinding
    private lateinit var hrViewModel: HRViewModel

    private lateinit var hrDesignationList: List<HRDesignation.Designation>
    private var hrDesignationId: String? = null

    private var gender: String? = null
    private var religion: String? = null
    private var category: String? = null
    private var hrIdType: String? = null
    private var selectIdType_hr: String? = null
    private lateinit var preferences: AppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hrViewModel = ViewModelProviders.of(
            this,
            HRViewModel.HRViewModelFactory(context?.applicationContext as Application)
        )
            .get(HRViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_hr_enlistment, container, false)
        preferences = AppPreferences(context)

        initViews()
        initObservables()

        hrViewModel.getHRDesignationList()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initViews() {

        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.etHrMobile.addTextChangedListener(MyTextWatcher(binding.etHrMobile!!))

        binding.spinnerGenderHr.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        gender = parent!!.getItemAtPosition(position).toString()
                    }
                }
            }

        binding.spinnerReligionHr.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        religion = parent!!.getItemAtPosition(position).toString()
                    }
                }
            }

        binding.spinnerCategoryHr.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        category = parent!!.getItemAtPosition(position).toString()
                    }
                }
            }

        binding.spinnerHrIdType.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                  when (position) {
                        0 -> {
                            hrIdType = null
                            binding.linear4?.visibility = View.GONE
                            binding.linear5?.visibility = View.GONE
                            binding.etHrCustomId?.visibility = View.GONE
                            binding.etHrCustomId?.visibility = View.GONE
                            binding.hrHead?.visibility = View.GONE
                        }
                        1 -> {
                            hrIdType = 0.toString()
                            binding.linear4?.visibility = View.GONE
                            binding.linear5?.visibility = View.GONE
                            binding.etHrCustomId?.visibility = View.GONE
                            binding.etHrCustomId?.visibility = View.GONE
                            binding.hrHead?.visibility = View.GONE
                        }
                        2 -> {
                            hrIdType = 1.toString()
                            binding.linear5?.visibility = View.VISIBLE
                            binding.linear4?.visibility = View.VISIBLE
                            binding.etHrCustomId?.visibility = View.VISIBLE
                            binding.etHrCustomId?.visibility = View.VISIBLE
                            binding.hrHead?.visibility = View.VISIBLE
                            selectIdType_hr =parent!!.getItemAtPosition(position).toString()
                        }
                    }
                }
            }

        binding.spinnerDesignationHr.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position == 0) {
                        hrDesignationId = null
                    } else {
                        preferences.setHrDesignationPosition(position)
                        getHRDesignationId(parent!!.getItemAtPosition(position).toString())
                    }
                }
            }

        binding.btnHrRegSubmit.setOnClickListener {

            checkValues()

        }

    }

    private fun initObservables() {
       Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerLayoutId?.visibility = View.VISIBLE
                binding.shimmerLayoutId?.startShimmer()
            } else {
                binding.shimmerLayoutId?.visibility = View.GONE
            }
        })

        hrViewModel.hrDesignationList.observe(
            viewLifecycleOwner,
            Observer { sectionList ->
                if (!sectionList.isNullOrEmpty()) {
                    this.hrDesignationList = sectionList
                    setHrDesignationSpinner(sectionList)
                }
            })

        hrViewModel.isLoadingSave.observe(this, Observer {
            if (it!= null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        hrViewModel.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                hrViewModel.apiError.value=null
            }
        })

        hrViewModel.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                binding.spinnerHrIdType.setSelection(0)
                binding.etHrCustomId.text?.clear()
                binding.etHrName.text?.clear()
                binding.spinnerGenderHr.setSelection(0)
                binding.spinnerReligionHr.setSelection(0)
                binding.spinnerCategoryHr.setSelection(0)
                binding.spinnerDesignationHr.setSelection(0)
                binding.etHrMobile.text?.clear()
                binding.etHrMobile.clearFocus()
                hrViewModel.apiSuccess.value=null
            }
        })
    }

    private fun setHrDesignationSpinner(hrDesignationList: List<HRDesignation.Designation>) {
        val hrDesignationTempList = ArrayList<String>()
        hrDesignationTempList.add("Select HR Designation")

        hrDesignationList.forEach { section ->
            hrDesignationTempList.add(section.name.toString())
        }

        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            hrDesignationTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinnerDesignationHr.adapter = sectionAdapter
    }


    private fun getHRDesignationId(desigName: String) {
        hrDesignationList.forEach { hrDesignation ->
            if (hrDesignation.getDesigName().equals(desigName, true)) {
                hrDesignationId = hrDesignation.getDesigID().toString()
            }
        }
    }

    private fun checkValues() {

        when {
            hrIdType.isNullOrEmpty() -> {
                activity?.let {
                    Toasty.error(it, "Select HR ID Type", Toast.LENGTH_SHORT, true).show()
                }
            }

            hrIdType == "1" && binding.etHrCustomId.text.isNullOrEmpty() -> {
                binding.etHrCustomId.error = getString(R.string.err_mssg_st_id)
                requestFocus(binding.etHrCustomId!!)
            }

            binding.etHrName.text.isNullOrEmpty() -> {
                binding.etHrName.error = getString(R.string.err_mssg_name)
                requestFocus(binding.etHrName)
            }

            gender.isNullOrEmpty() -> {
                activity?.let {
                    Toasty.error(it, "Select Gender", Toast.LENGTH_SHORT, true).show()
                }
            }
            religion.isNullOrEmpty() -> {
                activity?.let {
                    Toasty.error(it, "Select Religion", Toast.LENGTH_SHORT, true).show()
                }
            }

            category.isNullOrEmpty() -> {
                activity?.let {
                    Toasty.error(it, "Select Category", Toast.LENGTH_SHORT, true).show()
                }
            }

            hrDesignationId.isNullOrEmpty() -> {
                activity?.let {
                    Toasty.error(it, "Select Designation", Toast.LENGTH_SHORT, true).show()
                }
            }

            binding.etHrMobile.text.isNullOrEmpty() || binding.etHrMobile.text.length != 11 -> {
                binding.etHrMobile.error = getString(R.string.err_msg_phone_invalid)
                requestFocus(binding.etHrMobile!!)

            }

            else -> {

                        val hrEnlist = HREnlist(
                            category, binding.etHrCustomId.text.toString(),
                            hrDesignationId, gender, binding.etHrMobile.text.toString(),
                            binding.etHrName.text.toString(), religion
                        )

                        hrViewModel.postHREnlistData(arrayOf(hrEnlist), hrIdType!!)
                    }


        }
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            if (!charSequence.toString().startsWith("01", true)) {
                binding.etHrMobile.error = getString(R.string.err_msg_phone_invalid)
            }
        }

        override fun afterTextChanged(editable: Editable) {
        }
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

}
