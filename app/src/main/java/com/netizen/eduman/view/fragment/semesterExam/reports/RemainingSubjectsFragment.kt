package com.netizen.eduman.view.fragment.semesterExam.reports


import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.RemainingSubjectsAdapter
import com.netizen.eduman.databinding.FragmentRemainingSubjectsBinding
import com.netizen.eduman.viewModel.SemesterExamViewModel

/**
 * A simple [Fragment] subclass.
 */
class RemainingSubjectsFragment : DialogFragment() {

    private lateinit var binding: FragmentRemainingSubjectsBinding
    private lateinit var semesterViewModel: SemesterExamViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_remaining_subjects,
            container,
            false
        )

        initViews()
        initObservables()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)

        semesterViewModel = ViewModelProviders.of(activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application))
            .get(SemesterExamViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog!!.window!!.attributes.windowAnimations = R.style.VerticalDialogAnimation
    }

    private fun initViews() {
        binding.imageViewClose.setOnClickListener {
            dismiss()
        }
    }

    private fun initObservables() {
        semesterViewModel.unAssignedResult.observe(viewLifecycleOwner, Observer { unAssignResult ->
            if (unAssignResult != null) {
                binding.textViewTotalSubject.text = unAssignResult.getNumOfTotalSubjects().toString()
                binding.textViewRemainingSubjects.text = unAssignResult.getNumOfMarkUnInputtedSubjects().toString()
                binding.textViewSection.text = unAssignResult.getSectionName()

                val subjectList = unAssignResult.getMarkUnInputtedSubjects()?.split(",")
                binding.recyclerViewSubjectList.layoutManager = LinearLayoutManager(context)
                binding.recyclerViewSubjectList.setHasFixedSize(true)
                binding.recyclerViewSubjectList.adapter = RemainingSubjectsAdapter(context!!, subjectList)
            }
        })
    }
}
