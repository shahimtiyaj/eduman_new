package com.netizen.eduman.view.fragment.attendance.hrAttendance

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.HrReportDateSelectionBinding
import com.netizen.eduman.utils.MyUtilsClass
import java.util.*

class FragmentHRDateSelection : Fragment() {
    private lateinit var binding: HrReportDateSelectionBinding
    private var calendar = Calendar.getInstance()
    private var errorTextView: TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=DataBindingUtil.inflate(inflater, R.layout.hr_report_date_selection, container, false)

        initViews()

        return binding.root
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.textViewDate.setOnClickListener {
            MyUtilsClass.showsDatePicker(context!!, dateSetListener)
        }

        binding.buttonFind.setOnClickListener {

            hrDateAttendance =binding.textViewDate.text.toString()

            when {
                binding.textViewDate.text.toString().equals("Select Date", true) -> {
                    binding.textViewDate.error = "Select Date"
                    MyUtilsClass.showErrorToasty(context!!, "Select Date")
                }
                else -> {
                    val action = FragmentHRDateSelectionDirections.actionFragmentHRDateSelection2ToTabFragmentHPAL()
                    findNavController().navigate(action)
                }
            }
        }
    }

    private val dateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            binding.textViewDate.text = MyUtilsClass.dateFormatHRAttendance.format(calendar.time)
            binding.textViewDate.error=null
        }

    companion object{
        var hrDateAttendance=""
    }
}
