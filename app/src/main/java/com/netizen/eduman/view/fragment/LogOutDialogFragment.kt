package com.netizen.eduman.view.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentLogOutDialogBinding
import com.netizen.eduman.view.activity.MainActivity


/**
 * A simple [Fragment] subclass.
 */
class LogOutDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentLogOutDialogBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_log_out_dialog,
            container,
            false
        )

        binding.imageViewClose.setOnClickListener {
            dismiss()
        }

        binding.cancel.setOnClickListener {
            dismiss()
        }

        binding.logOut.setOnClickListener {
            dismiss()
            findNavController().popBackStack(R.id.dashBoardFragment, true)
            MainActivity.hideBottomNavigation()
            findNavController().navigate(R.id.signInFragment)
        }

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)
    }
}
