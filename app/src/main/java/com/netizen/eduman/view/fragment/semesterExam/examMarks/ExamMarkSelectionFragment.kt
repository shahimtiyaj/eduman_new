package com.netizen.eduman.view.fragment.semesterExam.examMarks


import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentExamMarkSelectionBinding
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.view.fragment.SectionDialogFragment
import com.netizen.eduman.viewModel.SemesterExamViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class ExamMarkSelectionFragment : Fragment() {

    private lateinit var binding: FragmentExamMarkSelectionBinding
    private lateinit var semesterViewModel: SemesterExamViewModel

    private lateinit var errorTextView: TextView
    private var isMarkInputFragment: Boolean? = null

    private var tempSectionList: ArrayList<String>?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_exam_mark_selection,
            container,
            false
        )

        val args =
            ExamMarkSelectionFragmentArgs.fromBundle(
                arguments!!
            )
        isMarkInputFragment = args.isMarkInputFragment
        if (isMarkInputFragment!!) {
            binding.toolbarTitle.text = "Mark Input Form"
        } else {
            binding.toolbarTitle.text = "Mark Update Form"
        }

        semesterViewModel.getRoleWiseSectionList()

        initViews()
        initObservables()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        semesterViewModel = ViewModelProviders.of(
            activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application)
        )
            .get(SemesterExamViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        semesterViewModel.classConfigId = null
    }

    private fun initViews() {

        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.sectionSelect.setOnClickListener {
            if (!tempSectionList.isNullOrEmpty()) {
                val bundle = Bundle()
                bundle.putBoolean("isStudent", false)

                val sectionDialogFragment = SectionDialogFragment()
                sectionDialogFragment.arguments = bundle
                sectionDialogFragment.show(activity!!.supportFragmentManager, null)
            }
        }

        binding.imageViewSpinnerIcon.setOnClickListener {
            if (!tempSectionList.isNullOrEmpty()) {
                val bundle = Bundle()
                bundle.putBoolean("isStudent", false)

                val sectionDialogFragment = SectionDialogFragment()
                sectionDialogFragment.arguments = bundle
                sectionDialogFragment.show(activity!!.supportFragmentManager, null)
            }
        }

        binding.spinnerGroup.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    semesterViewModel.getGroupId(parent!!.getItemAtPosition(position).toString())
                } else {
                    semesterViewModel.groupId = null

                }
            }
        }

        binding.spinnerExam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    try {
                        semesterViewModel.getExamId(parent!!.getItemAtPosition(position).toString())
                    }
                    catch (e: KotlinNullPointerException){
                        e.printStackTrace()
                    }
                } else {
                    semesterViewModel.examId = null
                }
            }
        }

        binding.spinnerSubject.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        semesterViewModel.getSubjectId(parent!!.getItemAtPosition(position).toString())
                    } else {
                        semesterViewModel.subjectId = null
                    }
                }
            }

        binding.buttonFind.btnFind.setOnClickListener {
            semesterViewModel.checkExamMarkSelection(isMarkInputFragment!!)
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerSection.visibility = View.VISIBLE
            } else {
                binding.shimmerSection.visibility = View.GONE
            }
        })

        Loaders.isLoading2.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerGroup.visibility = View.VISIBLE
            } else {
                binding.shimmerGroup.visibility = View.GONE
            }
        })

        Loaders.isLoading3.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerExam.visibility = View.VISIBLE
            } else {
                binding.shimmerExam.visibility = View.GONE
            }
        })

        Loaders.isLoading4.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerSubject.visibility = View.VISIBLE
            } else {
                binding.shimmerSubject.visibility = View.GONE
            }
        })

        semesterViewModel.tempSectionList.observe(viewLifecycleOwner, Observer { tempSectionList ->
            if (!tempSectionList.isNullOrEmpty()) {
//                setSectionSpinner(tempSectionList)
                this.tempSectionList = tempSectionList
            }
        })

        Loaders.sectionName.observe(viewLifecycleOwner, Observer { sectionName ->
            if (!sectionName.isNullOrEmpty()) {
                if (!sectionName.equals("Select Section", true)) {
                    semesterViewModel.isSectionEmpty.value = false
                    binding.textViewSection.error = null
                } else {
                    semesterViewModel.classConfigId = null
                }

                binding.textViewSection.text = sectionName
                semesterViewModel.getSectionId(sectionName)
            }
        })

        semesterViewModel.tempGroupList.observe(viewLifecycleOwner, Observer { groupList ->
            if (!groupList.isNullOrEmpty()) {
                setGroupSpinner(groupList)
            }
        })

        semesterViewModel.tempExamList.observe(viewLifecycleOwner, Observer { examList ->
            if (!examList.isNullOrEmpty()) {
                setExamSpinner(examList)
            }
        })

        semesterViewModel.tempSubjectList.observe(viewLifecycleOwner, Observer { subjectList ->
            if (!subjectList.isNullOrEmpty()) {
                setSubjectSpinner(subjectList)
            }
        })

        semesterViewModel.isSectionEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.textViewSection.error = "Empty"
                showErrorToasty(getString(R.string.select_section))
                semesterViewModel.isSectionEmpty.value = false
            }
        })

        semesterViewModel.isGroupEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it && binding.spinnerGroup.selectedView != null) {
                errorTextView = binding.spinnerGroup.selectedView as TextView
                errorTextView.error = "Empty"
                showErrorToasty(getString(R.string.select_group))
            }
        })

        semesterViewModel.isExamEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it && binding.spinnerExam.selectedView != null) {
                errorTextView = binding.spinnerExam.selectedView as TextView
                errorTextView.error = "Empty"
                showErrorToasty(getString(R.string.select_exam))
            }
        })

        semesterViewModel.isSubjectEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it && binding.spinnerSubject.selectedView != null) {
                errorTextView = binding.spinnerSubject.selectedView as TextView
                errorTextView.error = "Empty"
                showErrorToasty(getString(R.string.select_subject))
            }
        })

        semesterViewModel.isListFound.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                findNavController().navigate(
                    ExamMarkSelectionFragmentDirections.actionExamMarkFragmentToMarkInputFragment(
                        isMarkInputFragment!!
                    )
                )
                semesterViewModel.isListFound.value = false
            }
        })
    }

    private fun setGroupSpinner(groupTempList: List<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            groupTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerGroup.adapter = sectionAdapter
    }

    private fun setExamSpinner(examTempList: List<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            examTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerExam.adapter = sectionAdapter
    }

    private fun setSubjectSpinner(subjectTempList: List<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            subjectTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerSubject.adapter = sectionAdapter
    }

    private fun showErrorToasty(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_SHORT).show()
    }
}
