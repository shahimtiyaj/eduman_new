package com.netizen.eduman.view.fragment.attendance.studentAttendance

import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentSearchTakeAttedanceStdBinding
import com.netizen.eduman.model.SectionResponse
import com.netizen.eduman.model.TakeAtdPeriodResponse
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.utils.MyUtilsClass
import com.netizen.eduman.utils.MyUtilsClass.Companion.dateFormatTakeAttendance
import com.netizen.eduman.view.fragment.SectionDialogFragment
import com.netizen.eduman.viewModel.AttendanceViewModel
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty
import java.util.*
import kotlin.collections.ArrayList

class TakeAttendanceStdSelectionFragment : Fragment() {

    private lateinit var binding: FragmentSearchTakeAttedanceStdBinding
    private lateinit var studentViewModel: StudentViewModel
    private lateinit var sectionList: List<SectionResponse.Section>
    private var sectionName: String? = null
    private var periodName: String? = null
    private var sectionId: String? = null

    private lateinit var attendanceViewModel: AttendanceViewModel
    private var takeAdtPeriodList = ArrayList<TakeAtdPeriodResponse.TakAtdPeriod>()
    private var takeAtdPeriodId: String? = null
    private var errorTextView: TextView? = null
    private var calendar = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_search_take_attedance_std,
            container,
            false
        )

        initViews()
        initObservables()

        // sectionId=null
        // takeAtdPeriodId=null

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attendanceViewModel = ViewModelProviders.of(
                activity!!,
                AttendanceViewModel.AttendanceViewModelFactory(context!!.applicationContext as Application)
            )
            .get(AttendanceViewModel::class.java)

        studentViewModel = ViewModelProviders.of(
                activity!!,
                StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application)
            )
            .get(StudentViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        sectionName = null
        sectionId = null
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.textViewDate.setOnClickListener {
            MyUtilsClass.showsDatePicker(context!!, dateSetListener)
        }

        binding.textViewSection.setOnClickListener {
            if (!binding.textViewDate.text.toString().equals("Select Date", true)) {
                val bundle = Bundle()
                bundle.putBoolean("isStudent", true)

                val sectionDialogFragment = SectionDialogFragment()
                sectionDialogFragment.arguments = bundle
                sectionDialogFragment.show(activity!!.supportFragmentManager, null)
            } else {
                Toasty.error(context!!, "Please select date first!", Toasty.LENGTH_LONG).show()
            }
        }

        binding.spinnerAttendancePeriod.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position == 0) {
                    takeAtdPeriodId = null
                } else {
                    periodName = parent!!.getItemAtPosition(position).toString()
                    getPeriodId(periodName!!)
                }
            }
        }

        binding.buttonAllSearch.btnFind.setOnClickListener {
            when {
                binding.textViewDate.text.toString().equals("Select Date", true) -> {
                    binding.textViewDate.error = "Select Date"
                    MyUtilsClass.showErrorToasty(context!!, "Select Date")
                }

                sectionId.isNullOrEmpty() -> {
                    binding.textViewSection.error = "Empty"
                    MyUtilsClass.showErrorToasty(context!!, getString(R.string.select_section))
                }

                takeAtdPeriodId.isNullOrEmpty() -> {
                    errorTextView = binding.spinnerAttendancePeriod.selectedView as? TextView
                    errorTextView?.error = "Empty"
                    MyUtilsClass.showErrorToasty(context!!, getString(R.string.select_period))
                }

                else -> {
                    attendanceViewModel.sectionName.value = sectionName
                    attendanceViewModel.sectionId = sectionId
                    attendanceViewModel.periodId = takeAtdPeriodId
                    attendanceViewModel.date = MyUtilsClass.getDateFormatForTakeAtdStdData(binding.textViewDate.text.toString())

                    attendanceViewModel.getStudentListForTakeAttendance(
                        MyUtilsClass.getDateFormatForTakeAtdStdData(binding.textViewDate.text.toString()),
                        sectionId!!,
                        takeAtdPeriodId!!
                    )
                }
            }
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerSection.visibility = View.VISIBLE
            } else {
                binding.shimmerSection.visibility = View.GONE
            }
        })

        Loaders.isLoading2.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerLayoutId.visibility = View.VISIBLE
            } else {
                binding.shimmerLayoutId.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        Loaders.sectionName.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()){
                if (it.equals("Select Section", true)) {
                    sectionName = it
                    binding.textViewSection.text = it
                    sectionId = null
                } else {
                    sectionName = it
                    binding.textViewSection.text = it
                    binding.textViewSection.error = null
                    getSectionId(it)
                }
            }
        })

        studentViewModel.sectionList.observe(viewLifecycleOwner, Observer { sectionList ->
            if (!sectionList.isNullOrEmpty()) {
                this.sectionList = sectionList
//                setSectionSpinner(sectionList)
            }
        })

        attendanceViewModel.takAdtPeriodList.observe(viewLifecycleOwner, Observer { periodList ->
            if (!periodList.isNullOrEmpty()) {
                this.takeAdtPeriodList = periodList as ArrayList<TakeAtdPeriodResponse.TakAtdPeriod>
                setTakeAdtPeriodSpinner(periodList)
            }
        })

        attendanceViewModel.isListFound.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_searchTakeAttendanceStdFragment_to_takeAttendanceStudentFragment)
                attendanceViewModel.isListFound.value = false
            }
        })
    }

    private fun setTakeAdtPeriodSpinner(takeAdtPeriodList: List<TakeAtdPeriodResponse.TakAtdPeriod>) {
        val takeAtdPeriodTempList = ArrayList<String>()
        takeAtdPeriodTempList.add("Select Period")

        takeAdtPeriodList.forEach { period ->
            takeAtdPeriodTempList.add(period.getTakAtdPeriodName().toString())
        }

        val periodAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            takeAtdPeriodTempList
        )
        periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerAttendancePeriod.adapter = periodAdapter
    }

    private fun getPeriodId(takeAtdPeriodName: String) {
        takeAdtPeriodList.forEach { period ->
            if (period.getTakAtdPeriodName().equals(takeAtdPeriodName, true)) {
                takeAtdPeriodId = period.getTakAtdPeriodId().toString()
            }
        }
    }

    private fun getSectionId(sectionName: String) {
        sectionList.forEach { section ->
            if (section.getClassShiftSection().equals(sectionName, true)) {
                sectionId = section.getClassConfigId().toString()

                attendanceViewModel.getPeriodListForTakeAtdStudentList(
                    binding.textViewDate.text.toString(),
                    sectionId!!
                )
                return
            }
        }
    }

    private val dateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            binding.textViewDate.text = dateFormatTakeAttendance.format(calendar.time)
            binding.textViewDate.error=null
            studentViewModel.getSectionList()
        }

}
