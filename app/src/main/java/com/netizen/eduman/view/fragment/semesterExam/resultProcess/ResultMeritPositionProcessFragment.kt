package com.netizen.eduman.view.fragment.semesterExam.resultProcess

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentResultMeritPositionProcessBinding
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.utils.MyUtilsClass.Companion.showErrorToasty
import com.netizen.eduman.viewModel.SemesterExamViewModel
import es.dmoral.toasty.Toasty

class ResultMeritPositionProcessFragment : Fragment() {
    private lateinit var binding: FragmentResultMeritPositionProcessBinding
    private lateinit var semesterViewModel: SemesterExamViewModel
    private var errorTextView: TextView? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        semesterViewModel = ViewModelProviders.of(activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application))
            .get(SemesterExamViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_result_merit_position_process,
            container,
            false
        )

        binding.lifecycleOwner = this
        initViews()
        initObservables()
        semesterViewModel.getGrandFinalClassList()

        return binding.root
    }

    private fun initViews() {

        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.spinnerClass.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        semesterViewModel.getMeritPositionClassId(parent!!.getItemAtPosition(position).toString())
                    }

                    else {
                        semesterViewModel.classConfigId = ""
                    }
                }
            }

        binding.spinnerExam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    semesterViewModel.getMeritPositionExamId(parent!!.getItemAtPosition(position).toString())
                }

                else {
                    semesterViewModel.grandFinalExamId = ""
                }
            }
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerSection.visibility = View.VISIBLE
            } else {
                binding.shimmerSection.visibility = View.GONE
            }
        })

        Loaders.isLoading3.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerExam.visibility = View.VISIBLE
            } else {
                binding.shimmerExam.visibility = View.GONE
            }
        })

        semesterViewModel.isExamEmpty.observe(viewLifecycleOwner, Observer {
            if (it!==null && it && binding.spinnerExam.selectedView !=null) {
                errorTextView = (binding.spinnerExam.selectedView as? TextView)
                errorTextView?.error = "Empty"
                showErrorToasty(context!!,getString(R.string.select_exam))
                semesterViewModel.isExamEmpty.value=null
            }
        })

        semesterViewModel.isSectionEmpty.observe(viewLifecycleOwner, Observer {
            if (it!==null && it && binding.spinnerClass.selectedView !=null) {
                errorTextView = (binding.spinnerClass.selectedView as? TextView)
                errorTextView?.error = "Empty"
                showErrorToasty(context!!,getString(R.string.select_class))
                semesterViewModel.isSectionEmpty.value=null

            }
        })

        semesterViewModel.tempMeritPositionExamList.observe(viewLifecycleOwner, Observer { examList ->
//            if (!examList.isNullOrEmpty()) {
//                setExamSpinner(examList)
//            }
            setExamSpinner(examList)
        })

        semesterViewModel.tempGrandFinalClassList.observe(viewLifecycleOwner, Observer {classList ->
            if (!classList.isNullOrEmpty()) {
                setSectionSpinner(classList)
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()){
                Toasty.success(activity!!, it, Toasty.LENGTH_LONG).show()
                findNavController().popBackStack()
                Loaders.apiSuccess.value=""
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()){
                Toasty.error(activity!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value=""
            }
        })

        binding.btnFind.setOnClickListener {
            semesterViewModel.checkMeritPositionValues()
        }
    }

    private fun setSectionSpinner(grandFinalClassTempList: ArrayList<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            grandFinalClassTempList)

        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerClass.adapter = sectionAdapter
    }

    private fun setExamSpinner(examTempList: List<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            examTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerExam.adapter = sectionAdapter
    }
}
