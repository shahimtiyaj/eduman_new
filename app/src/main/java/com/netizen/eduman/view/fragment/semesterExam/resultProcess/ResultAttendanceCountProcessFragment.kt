package com.netizen.eduman.view.fragment.semesterExam.resultProcess

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentResultAttendanceCountProcessBinding
import com.netizen.eduman.model.PeriodResponse
import com.netizen.eduman.model.SectionResponse
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.utils.MyUtilsClass.Companion.showErrorToasty
import com.netizen.eduman.view.fragment.SectionDialogFragment
import com.netizen.eduman.viewModel.AttendanceViewModel
import com.netizen.eduman.viewModel.SemesterExamViewModel
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty

class ResultAttendanceCountProcessFragment : Fragment() {

    private lateinit var binding: FragmentResultAttendanceCountProcessBinding
    private lateinit var studentViewModel: StudentViewModel
    private lateinit var semesterViewModel: SemesterExamViewModel
    private lateinit var attendanceViewModel: AttendanceViewModel
    private var errorTextView: TextView? = null
    private lateinit var preferences: AppPreferences

    private lateinit var sectionList: List<SectionResponse.Section>
    private var sectionName: String? = null
    private var sectionId: String? = null
    private var periodList = ArrayList<PeriodResponse.Period>()
    private var periodId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_result_attendance_count_process,
            container,
            false
        )

        binding.lifecycleOwner = this
        preferences = AppPreferences(context)
        initViews()
        initObservables()
        studentViewModel.getSectionList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        studentViewModel = ViewModelProviders.of(
                activity!!,
                StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application)
            )
            .get(StudentViewModel::class.java)

        semesterViewModel = ViewModelProviders.of(
                activity!!,
                SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application)
            )
            .get(SemesterExamViewModel::class.java)

        attendanceViewModel = ViewModelProviders.of(
                activity!!,
                AttendanceViewModel.AttendanceViewModelFactory(context!!.applicationContext as Application)
            )
            .get(AttendanceViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        sectionName = null
        sectionId = null
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.textViewSection.setOnClickListener {
            if (!sectionList.isNullOrEmpty()) {
                val bundle = Bundle()
                bundle.putBoolean("isStudent", true)

                val sectionDialogFragment = SectionDialogFragment()
                sectionDialogFragment.arguments = bundle
                sectionDialogFragment.show(activity!!.supportFragmentManager, null)
            }
        }

        binding.spinnerPeriod.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    getPeriodId(parent!!.getItemAtPosition(position).toString())
                } else {
                    periodId = ""
                }
            }
        }

        binding.spinnerExam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    semesterViewModel.getExamIdForAtdCount(parent!!.getItemAtPosition(position).toString())
                } else {
                    semesterViewModel.examIdAtdCount = ""
                }
            }
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerSection.visibility = View.VISIBLE
            } else {
                binding.shimmerSection.visibility = View.GONE
            }
        })

        Loaders.sectionName.observe(viewLifecycleOwner, Observer { sectionName ->
            if (!sectionName.isNullOrEmpty()) {
                if (sectionName.equals("Select Section", true)) {
                    this.sectionName = sectionName
                    binding.textViewSection.text = sectionName
                    sectionId = null
                } else {
                    this.sectionName = sectionName
                    binding.textViewSection.text = sectionName
                    binding.textViewSection.error = null
                    getSectionId(sectionName)
                }
            }
        })

        studentViewModel.sectionList.observe(viewLifecycleOwner, Observer { sectionList ->
            if (!sectionList.isNullOrEmpty()) {
                this.sectionList = sectionList
//                setSectionSpinner(sectionList)
            }
        })

        Loaders.isLoading2.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerPeriod.visibility = View.VISIBLE
            } else {
                binding.shimmerPeriod.visibility = View.GONE
            }
        })

        attendanceViewModel.periodList.observe(viewLifecycleOwner, Observer { periodList ->
            if (!periodList.isNullOrEmpty()) {
                this.periodList = periodList as ArrayList<PeriodResponse.Period>
                setPeriodSpinner(periodList)
            }
        })

        Loaders.isLoading3.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerExam.visibility = View.VISIBLE
            } else {
                binding.shimmerExam.visibility = View.GONE
            }
        })

        semesterViewModel.isExamEmpty.observe(viewLifecycleOwner, Observer {
            if (it != null && it && binding.spinnerExam.selectedView != null) {
                 errorTextView = (binding.spinnerExam.selectedView as? TextView)
                 errorTextView?.error = "Empty"
                showErrorToasty(context!!, getString(R.string.select_exam))
                semesterViewModel.isExamEmpty.value = null

            }
        })

        semesterViewModel.tempExamList.observe(viewLifecycleOwner, Observer { examList ->
            if (!examList.isNullOrEmpty()) {
                setExamSpinner(examList)
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                findNavController().popBackStack()
                Loaders.apiSuccess.value = ""
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = ""
            }
        })

        binding.btnFind.setOnClickListener {
            checkValues()
        }
    }

    private fun checkValues() {
        when {
            sectionId.isNullOrEmpty() -> {
                activity?.let {
                    binding.textViewSection.error = "Empty"
                    showErrorToasty(context!!, getString(R.string.select_section))
                }
            }

            periodId.isNullOrEmpty() -> {
                activity?.let {
                    errorTextView = (binding.spinnerPeriod.selectedView as? TextView)
                    errorTextView?.error = "Empty"
                    showErrorToasty(context!!, getString(R.string.select_period))
                }
            }

            else -> {
                semesterViewModel.checkAttendanceValues(sectionId!!, periodId!!)
            }
        }
    }

    private fun getSectionId(sectionName: String) {
        sectionList.forEach { section ->
            if (section.getClassShiftSection().equals(sectionName, true)) {
                sectionId = section.getClassConfigId().toString()
                semesterViewModel.getExamListForGeneralExam(sectionId!!)
                attendanceViewModel.getPeriodListForSearchingStudentReport()

                return
            }
        }
    }

    private fun getPeriodId(periodName: String) {
        periodList.forEach { period ->
            if (period.getName().equals(periodName, true)) {
                periodId = period.getId().toString()
            }
        }
    }

    private fun setPeriodSpinner(periodList: List<PeriodResponse.Period>) {
        val periodTempList = ArrayList<String>()
        periodTempList.add("Select Period")

        periodList.forEach { period ->
            periodTempList.add(period.getName().toString())
        }

        val periodAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            periodTempList
        )

        periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerPeriod.adapter = periodAdapter
    }

    private fun setExamSpinner(examTempList: List<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            examTempList
        )

        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerExam.adapter = sectionAdapter
    }
}
