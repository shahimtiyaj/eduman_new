package com.netizen.eduman.view.fragment.semesterExam.examMarks


import android.app.Application
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentExamMarkDashboardBinding
import com.netizen.eduman.viewModel.DashboardViewModel

/**
 * A simple [Fragment] subclass.
 */
class ExamMarkDashBoardFragment : Fragment() {

    private lateinit var binding: FragmentExamMarkDashboardBinding
    private var dashboardViewModel: DashboardViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_exam_mark_dashboard,
            container,
            false
        )

        dashboardViewModel = ViewModelProviders.of(
            this, DashboardViewModel.DashboardFactory(context!!.applicationContext as Application)
        ).get(DashboardViewModel::class.java)

        dashboardViewModel?.instituteInfoMutableLiveData!!.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.textViewSchoolName.text = it[0].instituteName
                binding.textViewSchoolAddress.text = it[0].instituteAddress
                binding.textViewInstituteId.text = it[0].instituteId
                binding.textViewAcademicYear.text = it[0].academic_year

                if (!it[0].instituteLogo.isNullOrEmpty()) {
                    Glide.with(context)
                        .load(Base64.decode(it[0].instituteLogo, Base64.DEFAULT))
                        .asBitmap()
                        .placeholder(R.drawable.ic_boy_icon)
                        .into(binding.schoolLogo)
                }
            }
        })

        binding.layoutMarkInput.setOnClickListener {
            val action =
                ExamMarkDashBoardFragmentDirections.actionExamMarksFragmentToMarkInputFragment(
                    true
                )
            findNavController().navigate(action)
        }

        binding.layoutMarkUpdate.setOnClickListener {
            val action =
                ExamMarkDashBoardFragmentDirections.actionExamMarksFragmentToMarkInputFragment(
                    false
                )
            findNavController().navigate(action)
        }
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        return binding.root
    }


}
