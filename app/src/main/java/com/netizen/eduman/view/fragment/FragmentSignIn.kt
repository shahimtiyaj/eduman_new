package com.netizen.eduman.view.fragment

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentSignInBinding
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.UserSession
import com.netizen.eduman.viewModel.SignInViewModel
import es.dmoral.toasty.Toasty

class FragmentSignIn : Fragment() {

    private var signInViewModel: SignInViewModel? = null
    private lateinit var binding: FragmentSignInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signInViewModel = ViewModelProviders.of(this, SignInViewModel.SignInViewModelFactory())
            .get(SignInViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)

        binding.lifecycleOwner = this

        try {
            initObservables()
        }
        catch (e: Exception){
            e.printStackTrace()
        }
        return binding.root
    }

    private fun initObservables() {

        binding.btnLogin?.setOnClickListener {
            it.hideKeyboard()
            signInViewModel?.onLogInClick(
                binding.inputUserName?.text.toString(),
                binding.inputPassword?.text.toString()
            )
        }

        signInViewModel?.isUserNameEmpty?.observe(this, Observer {
            if (it != null && it) {
                binding.inputUserName?.error = "Required Username"
                binding.inputUserName?.requestFocus()
            }
        })

        signInViewModel?.isPasswordEmpty?.observe(this, Observer {
            if (it != null && it) {
                binding.inputPassword?.error = "Required Password"
                binding.inputPassword?.requestFocus()
            }
        })

        signInViewModel?.accessToken?.observe(this, Observer { token ->
            if (token != null) {
                AppPreferences(context).setToken(token)
            }
        })

        signInViewModel?.lottieProgressDialog?.observe(this, Observer {
            if (it != null && it) {
//                binding.lottieProgressbar.setAnimation("loader.json")
//                binding.lottieProgressbar.playAnimation()
//                binding.lottieProgressbar.loop(true)
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                //binding.lottieProgressbar.setAnimation("loader.json")
//                binding.lottieProgressbar.cancelAnimation()
//                binding.lottieProgressbar.loop(false)
                binding.lottieProgressbar.visibility = View.GONE
            }
        })


        signInViewModel?.isLoggedIn?.observe(this, Observer {
            if (it != null && it) {
                val userSession = UserSession(context)
                userSession.createUserLogInSession()
                userSession.setUserCredentials(binding.inputUserName.text.toString())

                signInViewModel?.isUserNameEmpty?.value = false
                signInViewModel?.isPasswordEmpty?.value = false

                binding.inputUserName.text = null
                binding.inputPassword.text = null

                findNavController().navigate(R.id.action_signInFragment_to_dashBoardFragment)
            }
        })

        signInViewModel?.successStatus?.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
            }
        })

        signInViewModel?.errorStatus?.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
            }
        })

        val userName = UserSession(context).getUserName()
        if (!TextUtils.isEmpty(userName)) {
            binding.inputUserName.setText(userName)
        }
    }


    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}
