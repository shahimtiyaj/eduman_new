package com.netizen.eduman.view.fragment.attendance.studentAttendance


import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentSearchStudentAttendanceReportBinding
import com.netizen.eduman.model.PeriodResponse
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.AttendanceViewModel
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class SearchStudentAttendanceReportFragment : Fragment() {

    private lateinit var binding: FragmentSearchStudentAttendanceReportBinding
    private lateinit var attendanceViewModel: AttendanceViewModel

    private var calendar = Calendar.getInstance()
    private var periodList = ArrayList<PeriodResponse.Period>()

    companion object {
        var periodId: String? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_student_attendance_report,
            container,
            false)

        periodId = null

        initViews()
        initObservables()
        attendanceViewModel.getPeriodListForSearchingStudentReport()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attendanceViewModel = ViewModelProviders.of(activity!!,
            AttendanceViewModel.AttendanceViewModelFactory(context!!.applicationContext as Application))
            .get(AttendanceViewModel::class.java)
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.textViewDate.setOnClickListener {
            showDatePicker()
        }

        binding.spinnerPeriod.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    getPeriodId(parent!!.getItemAtPosition(position).toString())
                }
            }
        }

        binding.buttonFind.setOnClickListener {
            when {
                binding.textViewDate.text.toString().equals("Select Date", true) -> {
                    binding.textViewDate.error = "Select Date"
                    showToastyError("Select Date")
                }
                periodId.isNullOrEmpty() -> {
                    val errorTextView: TextView = binding.spinnerPeriod.selectedView as TextView
                    errorTextView.error = getString(R.string.select_period)
                    showToastyError(getString(R.string.select_period))
                }
                else -> {
                    attendanceViewModel.getStudentAttendanceReportSummaryList(
                        binding.textViewDate.text.toString(),
                        periodId!!
                    )
                }
            }
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it!= null && it)  {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerPeriod.visibility = View.VISIBLE
            } else {
                binding.shimmerPeriod.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_SHORT).show()
                Loaders.apiError.value = null
            }
        })

        attendanceViewModel.periodList.observe(viewLifecycleOwner, Observer { periodList ->
            if (!periodList.isNullOrEmpty()) {
                this.periodList = periodList as ArrayList<PeriodResponse.Period>
                setPeriodSpinner(periodList)
            }
        })

        attendanceViewModel.isListFound.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_searchStudentAttendanceReportFragment_to_studentAttendanceReportSummaryFragment)
                attendanceViewModel.isListFound.value = false
            }
        })
    }

    private fun setPeriodSpinner(periodList: List<PeriodResponse.Period>) {
        val periodTempList = ArrayList<String>()
        periodTempList.add("Select Period")

        periodList.forEach { period ->
            periodTempList.add(period.getName().toString())
        }

        val periodAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            periodTempList
        )
        periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerPeriod.adapter = periodAdapter
    }

    private fun getPeriodId(periodName: String) {
        periodList.forEach { period ->
            if (period.getName().equals(periodName, true)) {
                periodId = period.getId().toString()
            }
        }
    }

    private fun showDatePicker() {
        val datePickerDialog = DatePickerDialog(context!!, dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH))

        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val dateFormat = SimpleDateFormat("yyyy/MM/dd", Locale.US)
//      val dateFormat = SimpleDateFormat(myFormat)
        binding.textViewDate.text = dateFormat.format(calendar.time)
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_SHORT).show()
    }
}
