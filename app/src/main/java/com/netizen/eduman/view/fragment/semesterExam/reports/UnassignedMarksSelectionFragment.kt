package com.netizen.eduman.view.fragment.semesterExam.reports


import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentUnassignedMarksSelectionBinding
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.SemesterExamViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class UnassignedMarksSelectionFragment : Fragment() {

    private lateinit var binding: FragmentUnassignedMarksSelectionBinding
    private lateinit var semesterExamViewModel: SemesterExamViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_unassigned_marks_selection,
            container,
            false
        )

        initViews()
        initObservables()

        semesterExamViewModel.getAllExamList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        semesterExamViewModel = ViewModelProviders.of(
            activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application)
        )
            .get(SemesterExamViewModel::class.java)
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.spinnerExam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    semesterExamViewModel.getAllExamId(parent!!.getItemAtPosition(position).toString())
                } else {
                    semesterExamViewModel.examId = null
                }
            }
        }

        binding.buttonFind.btnFind.setOnClickListener {
            semesterExamViewModel.checkUnAssignedMarkSelection()
        }
    }

    private fun initObservables() {
        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it!=null && it) {
                binding.shimmerExam.visibility = View.VISIBLE
            } else {
                binding.shimmerExam.visibility = View.GONE
            }
        })

        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it!=null && it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        semesterExamViewModel.tempAllExamList.observe(viewLifecycleOwner, Observer { tempExamList ->
            setExamSpinner(tempExamList)
        })

        semesterExamViewModel.isExamEmpty.observe(viewLifecycleOwner, Observer {
            if (it!=null && it && binding.spinnerExam.selectedView != null) {
                val errorTextView = binding.spinnerExam.selectedView as TextView
                errorTextView.error = "Empty"
                Toasty.error(context!!, getString(R.string.select_exam), Toasty.LENGTH_LONG).show()
            }
        })

        semesterExamViewModel.isUnassignedResultFound.observe(viewLifecycleOwner, Observer {
            if (it!=null && it) {
                findNavController().navigate(R.id.action_unassignedMarksFragment_to_unassignedResultDetailsFragment)
                semesterExamViewModel.isUnassignedResultFound.value = false
            }
        })
    }

    private fun setExamSpinner(tempExamList: List<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            tempExamList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerExam.adapter = sectionAdapter
    }
}
