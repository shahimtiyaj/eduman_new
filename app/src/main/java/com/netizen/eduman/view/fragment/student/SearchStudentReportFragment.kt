package com.netizen.eduman.view.fragment.student


import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentSearchStudentReportBinding
import com.netizen.eduman.model.SectionResponse
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.view.fragment.SectionDialogFragment
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class SearchStudentReportFragment : Fragment() {

    private lateinit var binding: FragmentSearchStudentReportBinding
    private lateinit var studentViewModel: StudentViewModel

    private var sectionList= ArrayList<SectionResponse.Section>()
    private lateinit var tempSectionList: ArrayList<String>

    private var sectionName: String? = null
    private var sectionId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_student_report, container, false)

        initViews()
        initObservables()
        studentViewModel.getSectionList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        studentViewModel = ViewModelProviders.of(activity!!,
            StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application))
            .get(StudentViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        sectionName = null
        sectionId = null
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.textViewSection.setOnClickListener {
            if (!tempSectionList.isNullOrEmpty()) {
                val bundle = Bundle()
                bundle.putBoolean("isStudent", true)

                val sectionDialogFragment = SectionDialogFragment()
                sectionDialogFragment.arguments = bundle
                sectionDialogFragment.show(activity!!.supportFragmentManager, null)
            }
        }

        binding.imageViewSpinnerIcon.setOnClickListener {
            if (!tempSectionList.isNullOrEmpty()) {
                val bundle = Bundle()
                bundle.putBoolean("isStudent", true)

                val sectionDialogFragment = SectionDialogFragment()
                sectionDialogFragment.arguments = bundle
                sectionDialogFragment.show(activity!!.supportFragmentManager, null)
            }
        }

        binding.buttonFind.setOnClickListener {
            if (sectionId.isNullOrEmpty()) {
                binding.textViewSection.error = "Empty"
                Toasty.error(context!!, getString(R.string.select_section), Toasty.LENGTH_SHORT).show()
            } else {
                studentViewModel.getStudentReports(sectionId!!)
            }
        }
    }

    private fun initObservables() {
        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.shimmerSection.visibility = View.VISIBLE
            } else{
                binding.shimmerSection.visibility = View.GONE
            }
        })

        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.progressBar.visibility = View.VISIBLE
            } else{
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        Loaders.sectionName.observe(viewLifecycleOwner, Observer { sectionName ->
            if (!sectionName.isNullOrEmpty()) {
                this.sectionName = sectionName
                binding.textViewSection.error = null
                binding.textViewSection.text = sectionName
                getSectionId(sectionName)
            }

            if (sectionName.equals("Select Section", true)) {
                this.sectionName = null
                sectionId = null
            }
        })

        studentViewModel.tempSectionList.observe(viewLifecycleOwner, Observer { tempSectionList ->
            if (!tempSectionList.isNullOrEmpty()) {
                this.tempSectionList = tempSectionList
            }
        })

        studentViewModel.sectionList.observe(viewLifecycleOwner, Observer { sectionList ->
            if (!sectionList.isNullOrEmpty()) {
                this.sectionList = sectionList as ArrayList<SectionResponse.Section>
            }
        })

        studentViewModel.isStudentReportListFound.observe(viewLifecycleOwner, Observer {
            if (it && sectionName != null && sectionId != null) {
                val action =
                    SearchStudentReportFragmentDirections.actionSearchReportFragmentToReportDetailFragment(
                        sectionName!!,
                        sectionId!!
                    )
                findNavController().navigate(action)
                studentViewModel.isStudentReportListFound.value = false
            }
        })
    }

    private fun getSectionId(sectionName: String) {
        sectionList.forEach { section ->
            if (section.getClassShiftSection().equals(sectionName, true)) {
                sectionId = section.getClassConfigId().toString()
                return
            }
        }
    }
}
