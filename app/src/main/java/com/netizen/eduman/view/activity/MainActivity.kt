package com.netizen.eduman.view.activity

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import cdflynn.android.library.turn.TurnLayoutManager
import com.crashlytics.android.Crashlytics
import com.netizen.eduman.R
import com.netizen.eduman.databinding.ActivityMainBinding
import com.netizen.eduman.view.fragment.LogOutDialogFragment
import es.dmoral.toasty.Toasty
import io.fabric.sdk.android.Fabric

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var navController: NavController

    companion object {

        private lateinit var bottomNavigation: LinearLayout

        fun showBottomNavigation() {
            bottomNavigation.visibility = View.VISIBLE
        }

        fun hideBottomNavigation() {
            bottomNavigation.visibility = View.GONE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        //Crash Analytics---------------
        val fabric = Fabric.Builder(this)
            .kits(Crashlytics())
            .debuggable(true)
            .build()
        Fabric.with(fabric)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navController = Navigation.findNavController(this@MainActivity, R.id.nav_host_fragment)

        bottomNavigation = binding.customBottomNavigation
        hideBottomNavigation()

        binding.navigationHome.setOnClickListener {
//            if (navController.currentDestination?.id != R.id.dashBoardFragment) {
//                navController.navigate(R.id.dashBoardFragment)
//            }

            navController.navigate(R.id.dashBoardFragment)

            binding.navigationHome.setTextColor(resources.getColor(android.R.color.black))
//            binding.navigationDashboard.setTextColor(resources.getColor(android.R.color.darker_gray))
            binding.navigationLogOut.setTextColor(resources.getColor(android.R.color.darker_gray))

            binding.navigationHome.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_home_icon_blue, 0, 0)
//            binding.navigationDashboard.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_dashboard_icon_grey, 0, 0)
            binding.navigationLogOut.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_log_out_grey_icon, 0, 0)
        }

//       binding.navigationDashboard.setOnClickListener {
//            Toast.makeText(this, "Coming Soon.", Toast.LENGTH_SHORT).show()
//       }

        binding.navigationLogOut.setOnClickListener {
            binding.navigationHome.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_home_icon_grey, 0, 0)
//            binding.navigationDashboard.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_dashboard_icon_grey, 0, 0)
            binding.navigationLogOut.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_log_out_blue_icon, 0, 0)

            binding.navigationLogOut.setTextColor(resources.getColor(android.R.color.black))
//            binding.navigationDashboard.setTextColor(resources.getColor(android.R.color.darker_gray))
            binding.navigationHome.setTextColor(resources.getColor(android.R.color.darker_gray))

            LogOutDialogFragment().show(supportFragmentManager, null)
        }

        if (!isOnline(this)) {
            Toasty.error(this@MainActivity, "No connection!", Toasty.LENGTH_LONG).show()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            addConnectivityCallBack()
        }
    }

    override fun onBackPressed() {
        if (navController.currentDestination?.id == R.id.dashBoardFragment) {
            LogOutDialogFragment().show(supportFragmentManager, null)
        } else {
            super.onBackPressed()
        }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun addConnectivityCallBack() {
        val connectivityCallback: ConnectivityManager.NetworkCallback = @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
//                Toasty.success(this@MainActivity, "Connected.", Toasty.LENGTH_LONG).show()
            }

            override fun onLost(network: Network) {
                Toasty.error(this@MainActivity, "Connection lost!", Toasty.LENGTH_LONG).show()
            }
        }

        val connectivityManager = getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager

        connectivityManager.registerNetworkCallback(
            NetworkRequest.Builder()
                .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                .build(), connectivityCallback
        )
    }
}
