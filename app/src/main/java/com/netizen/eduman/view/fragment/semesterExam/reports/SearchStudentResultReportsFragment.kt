package com.netizen.eduman.view.fragment.semesterExam.reports

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentSearchStudentResultReportsBinding
import com.netizen.eduman.model.SectionResponse
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.utils.MyUtilsClass.Companion.showErrorToasty
import com.netizen.eduman.view.fragment.SectionDialogFragment
import com.netizen.eduman.viewModel.SemesterExamViewModel
import com.netizen.eduman.viewModel.StudentViewModel
import es.dmoral.toasty.Toasty

class SearchStudentResultReportsFragment : Fragment() {
    private lateinit var binding: FragmentSearchStudentResultReportsBinding
    private lateinit var studentViewModel: StudentViewModel
    private lateinit var semesterViewModel: SemesterExamViewModel
    private var sectionList= ArrayList<SectionResponse.Section>()
    private var sectionName: String? = null
    private var sectionId: String? = null
    private var errorTextView: TextView? = null
    private lateinit var preferences: AppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        studentViewModel = ViewModelProviders.of(
            activity!!,
            StudentViewModel.StudentViewModelFactory(context!!.applicationContext as Application)
        )
            .get(StudentViewModel::class.java)

        semesterViewModel = ViewModelProviders.of(
            activity!!,
            SemesterExamViewModel.SemesterExamViewModelFactory(context!!.applicationContext as Application)
        )
            .get(SemesterExamViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_search_student_result_reports,
            container,
            false
        )

        binding.lifecycleOwner = this
        preferences = AppPreferences(context)
        initViews()
        initObservables()
        studentViewModel.getSectionList()

        return binding.root
    }

    private fun initViews() {

        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.sectionSelect.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("isStudent", true)

            val sectionDialogFragment = SectionDialogFragment()
            sectionDialogFragment.arguments = bundle
            sectionDialogFragment.show(activity!!.supportFragmentManager, null)
        }


        /* binding.spinnerSection.onItemSelectedListener =
             object : AdapterView.OnItemSelectedListener {
                 override fun onNothingSelected(parent: AdapterView<*>?) {

                 }

                 override fun onItemSelected(
                     parent: AdapterView<*>?,
                     view: View?,
                     position: Int,
                     id: Long
                 ) {
                     if (position == 0) {
                         sectionId = null
                     } else {
                         sectionName = parent!!.getItemAtPosition(position).toString()
                         getSectionId(sectionName!!)
                     }
                 }
             }*/

        binding.spinnerExam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    semesterViewModel.getResultReportsExamId(parent!!.getItemAtPosition(position).toString())
                } else {
                    semesterViewModel.resultReportexamId = null
                }
            }
        }

        binding.buttonAllSearch.btnFind.setOnClickListener {

            semesterViewModel.checkResultReportsValues()

            /*if (sectionId.isNullOrEmpty()) {
                val errorTextView = binding.spinnerSection.selectedView as? TextView
                errorTextView?.error = "Empty"
                Toasty.error(context!!, getString(R.string.select_section), Toasty.LENGTH_SHORT)
                    .show()
            } else {
                semesterViewModel.checkResultReportsValues()
            }*/
        }
    }

    private fun initObservables() {
        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerSection.visibility = View.VISIBLE
            } else {
                binding.shimmerSection.visibility = View.GONE
            }
        })

        /* studentViewModel.sectionList.observe(viewLifecycleOwner, Observer { sectionList ->
             if (!sectionList.isNullOrEmpty()) {
                 this.sectionList = sectionList
                 setSectionSpinner(sectionList)
             }
         })*/


        semesterViewModel.isSectionEmpty.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.textViewSection.error = "Empty"
                Toasty.error(context!!, getString(R.string.select_section), Toasty.LENGTH_LONG).show()
                semesterViewModel.isSectionEmpty.value = false
            }
        })



        studentViewModel.sectionList.observe(viewLifecycleOwner, Observer { sectionList ->
            if (!sectionList.isNullOrEmpty()) {
                semesterViewModel.sectionList.value = sectionList
            }
        })

        Loaders.sectionName.observe(viewLifecycleOwner, Observer { sectionName ->
            if (!sectionName.isNullOrEmpty()) {
                if (!sectionName.equals("Select Section", true)) {
                    semesterViewModel.isSectionEmpty.value = false
                    binding.textViewSection.error = null
                    semesterViewModel.sectionName.value = sectionName
                } else {
                    semesterViewModel.classConfigId = null
                }

                binding.textViewSection.text = sectionName
               // studentViewModel.getSectionId(sectionName)
                semesterViewModel.getSectionIdInRemarks(sectionName)

            }
        })

        Loaders.isLoading3.observe(viewLifecycleOwner, Observer {
            if (it!= null && it) {
                binding.shimmerExam.visibility = View.VISIBLE
            } else {
                binding.shimmerExam.visibility = View.GONE
            }
        })

        semesterViewModel.isExamEmpty.observe(viewLifecycleOwner, Observer {
            if (it != null && it && binding.spinnerExam.selectedView != null) {
                errorTextView = binding.spinnerExam.selectedView as TextView
                errorTextView?.error = "Empty"
                showErrorToasty(context!!, getString(R.string.select_exam))
            }
        })

        semesterViewModel.tempExamList.observe(viewLifecycleOwner, Observer { examList ->
            setExamSpinner(examList)
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(activity!!, it, Toasty.LENGTH_LONG).show()
                findNavController().popBackStack()
                Loaders.apiSuccess.value = ""
            }

        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(activity!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = ""
            }
        })

        /*    semesterViewModel.isListFound.observe(viewLifecycleOwner, Observer {
                if (it) {
                    findNavController().navigate(
                        SearchStudentResultReportsFragmentDirections.actionSearchStudentResultReportsFragmentToStudentResultReportsFragment(
                            " ",
                            sectionName!!
                        )
                    )
                   // semesterViewModel.isGoToNext.value = false
                    semesterViewModel.isListFound.value = false

                }
            })*/

        semesterViewModel.isListFound.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                findNavController().navigate(R.id.action_searchStudentResultReportsFragment_to_studentResultReportsFragment)
                semesterViewModel.isListFound.value = false
            }
        })
    }

    /*  private fun setSectionSpinner(sectionList: List<SectionResponse.Section>) {
          val sectionTempList = ArrayList<String>()
          sectionTempList.add("Select Section")

          sectionList.forEach { section ->
              sectionTempList.add(section.getClassShiftSection().toString())
          }

          val sectionAdapter = ArrayAdapter(
              requireContext(),
              android.R.layout.simple_spinner_dropdown_item,
              sectionTempList
          )
          sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
          binding.spinnerSection.adapter = sectionAdapter
      }*/

    /* private fun getSectionId(sectionName: String) {
         sectionList.forEach { section ->
             if (section.getClassShiftSection().equals(sectionName, true)) {
                 sectionId = section.getClassConfigId().toString()
                 semesterViewModel.getExamListForGeneralExam(sectionId!!)
                 return
             }
         }
     }*/

    private fun setExamSpinner(examTempList: List<String>) {
        val sectionAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            examTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerExam.adapter = sectionAdapter
    }
}
