package com.netizen.eduman.view.fragment.appOpeningInroPage


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentSplashOneBinding

/**
 * A simple [Fragment] subclass.
 */
class SplashFragmentOne : Fragment() {

    private lateinit var binding: FragmentSplashOneBinding

    private var isVisibleToUser: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash_one, container, false)

//        if (isVisibleToUser) {
//            binding.lottie.playAnimation()
//        }

        return binding.root
    }

//    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
//        super.setUserVisibleHint(isVisibleToUser)
//        if (isVisibleToUser) {
//            binding.lottie.playAnimation()
//            this.isVisibleToUser = isVisibleToUser
//        }
//    }

//    override fun setMenuVisibility(menuVisible: Boolean) {
//        super.setMenuVisibility(menuVisible)
//        binding.lottie.playAnimation()
//    }
}
