package com.netizen.eduman.view.fragment.attendance.studentAttendance


import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.eduman.R
import com.netizen.eduman.adapter.StudentAttendanceReportDetailsAdapter
import com.netizen.eduman.databinding.FragmentStudentAttendanceReportDetailsBinding
import com.netizen.eduman.model.StudentAttendanceReportDetailsResponse
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.viewModel.AttendanceViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class StudentAttendanceReportDetailsFragment : Fragment() {

    private lateinit var binding: FragmentStudentAttendanceReportDetailsBinding
    private lateinit var attendanceViewModel: AttendanceViewModel

    private  var attendanceList =ArrayList<StudentAttendanceReportDetailsResponse.StudentAttendanceReportDetails>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_student_attendance_report_details,
            container,
            false)
        binding.lifecycleOwner = this

        val args =
            StudentAttendanceReportDetailsFragmentArgs.fromBundle(
                arguments!!
            )
        binding.textViewListType.text = args.sectionName.replace("-", " > ")

        initViews()
        initViewModel()
        attendanceViewModel.getStudentAttendanceReportList(args.attendanceDate,
            SearchStudentAttendanceReportFragment.periodId!!,
            args.classConfigId,
            args.status)

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attendanceViewModel = ViewModelProviders.of(this,
            AttendanceViewModel.AttendanceViewModelFactory(context!!.applicationContext as Application))
            .get(AttendanceViewModel::class.java)
    }

    private fun initViews() {
        binding.backText.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.imageViewSearch.setOnClickListener {
            binding.layoutSearch.visibility = View.VISIBLE
            binding.toolbarTitle.visibility = View.GONE
            binding.imageViewSearch.visibility = View.GONE
        }

        binding.imageViewSearchClose.setOnClickListener {
            binding.layoutSearch.visibility = View.GONE
            binding.toolbarTitle.visibility = View.VISIBLE
            binding.imageViewSearch.visibility = View.VISIBLE
        }

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val tempAttendanceList = ArrayList<StudentAttendanceReportDetailsResponse.StudentAttendanceReportDetails>()

                attendanceList.forEach { attendanceReport ->
                    if (attendanceReport.getStudentId()!!.toLowerCase().contains(s.toString(), true)) {
                        tempAttendanceList.add(attendanceReport)
                    }
                }

                setAdapter(tempAttendanceList)
            }
        })
    }

    private fun initViewModel() {

        Loaders.isLoading.observe(viewLifecycleOwner, Observer {
            if (it!= null && it)  {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_SHORT).show()
                Loaders.apiError.value=null
            }
        })

        attendanceViewModel.studentAttendanceList.observe(viewLifecycleOwner, Observer { attendanceList ->
            this.attendanceList = attendanceList as ArrayList<StudentAttendanceReportDetailsResponse.StudentAttendanceReportDetails>
            setAdapter(attendanceList)
        })
    }

    private fun setAdapter(attendanceList: List<StudentAttendanceReportDetailsResponse.StudentAttendanceReportDetails>) {
        binding.recyclerViewStudentAttendance.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewStudentAttendance.setHasFixedSize(true)
        binding.recyclerViewStudentAttendance.adapter = StudentAttendanceReportDetailsAdapter(
            context!!,
            attendanceList)
    }

    companion object {
        val TAG = StudentAttendanceReportDetailsFragment::class.java.simpleName
    }
}
