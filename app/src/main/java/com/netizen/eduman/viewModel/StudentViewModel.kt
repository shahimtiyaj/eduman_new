package com.netizen.eduman.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.google.gson.JsonObject
import com.netizen.eduman.model.SectionResponse
import com.netizen.eduman.repository.SemesterExamRepository
import com.netizen.eduman.repository.StudentRepository

class StudentViewModel(application: Application) : AndroidViewModel(application) {

    private val studentEnrollmentRepository = StudentRepository(application)
    private lateinit var semesterViewModel: SemesterExamViewModel
    private val semesterExamRepository = SemesterExamRepository(application)

    val academicYearList = studentEnrollmentRepository.academicYearList
    val sectionList = studentEnrollmentRepository.sectionList
    val categoryList = studentEnrollmentRepository.categoryList
    val groupList = studentEnrollmentRepository.groupList
    val studentReportList = studentEnrollmentRepository.studentReportList
    val isStudentReportListFound = studentEnrollmentRepository.isStudentReportListFound

    val sectionName = MutableLiveData<String>()

    fun getAllDataList() {
        studentEnrollmentRepository.getAcademicYearData()
        studentEnrollmentRepository.getSectionData()
        studentEnrollmentRepository.getCategoryData()
    }

    fun getSectionList() {
        studentEnrollmentRepository.getSectionData()
    }

    fun getGroupList(configId: String) {
        studentEnrollmentRepository.getGroupData(configId)
    }

    fun saveEnrollmentData(idType: String, jsonObject: JsonObject) {
        studentEnrollmentRepository.saveEnrollmentData(idType, jsonObject)
    }

    fun getStudentReports(sectionId: String) {
        studentEnrollmentRepository.getStudentReportList(sectionId)
    }

    //--------------------------------------------------------------
    //--------------------------------------------------------------
    var sectionId: String? = null
    val tempSectionList = Transformations.map(sectionList) {
        getTempSectionList(it)
    }
    private fun getTempSectionList(sectionList: List<SectionResponse.Section>): ArrayList<String> {
        val tempSectionList = ArrayList<String>()
        tempSectionList.add("Select Section")
        sectionList.forEach { section -> tempSectionList.add(section.getClassShiftSection().toString()) }
        return tempSectionList
    }

    fun getSectionId(sectionName: String) {
        sectionList.value!!.forEach { section ->
            if (section.getClassShiftSection().toString().equals(sectionName, true)) {
                sectionId = section.getClassConfigId().toString()
                Log.d("Section sdsds:", sectionId!!)
                //semesterViewModel.getExamListForGeneralExam(sectionId!!)
                 semesterExamRepository.getExamList(sectionId!!)
                return
            }
        }
    }

    class StudentViewModelFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(StudentViewModel::class.java)) {
                return StudentViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}