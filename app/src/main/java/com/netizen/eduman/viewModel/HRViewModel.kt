package com.netizen.eduman.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.eduman.repository.HRRepository

class HRViewModel(application: Application) : AndroidViewModel(application) {

    val hrDesignationRepository = HRRepository(application)

    val isLoading = hrDesignationRepository.isLoading
    val isLoadingSave = hrDesignationRepository.isLoadingSave
    val hrDesignationList = hrDesignationRepository.hrDesignationList
    val hrReportList = hrDesignationRepository.hrReportsList

    val apiError = hrDesignationRepository.apiError
    val apiSuccess = hrDesignationRepository.apiSuccess

    fun getHRDesignationList() {
        if (hrDesignationList.value.isNullOrEmpty()) {
            hrDesignationRepository.getHRDesignationData()
        }
    }

    fun postHREnlistData(hrEnlist: Array<*>, hrIdType: String) {
        hrDesignationRepository.hrEnlistPOstData(hrEnlist, hrIdType)
    }

    fun postHrReportGetData() {
        if (hrReportList.value.isNullOrEmpty()){
            hrDesignationRepository.getHrReportData()
        }
    }

    class HRViewModelFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HRViewModel::class.java)) {
                return HRViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}