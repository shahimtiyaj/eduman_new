package com.netizen.eduman.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.netizen.eduman.model.*
import com.netizen.eduman.repository.AttendanceRepository
import com.netizen.eduman.repository.SemesterExamRepository
import com.netizen.eduman.repository.StudentRepository
import com.netizen.eduman.utils.Loaders

class SemesterExamViewModel(application: Application) : AndroidViewModel(application) {

    private val semesterExamRepository = SemesterExamRepository(application)
    private val studentRepository = StudentRepository(application)
    private val attendanceRepository = AttendanceRepository(application)

    var sectionList = semesterExamRepository.sectionList
    private val groupList = studentRepository.groupList
    private val examList = semesterExamRepository.examList
    private val subjectList = semesterExamRepository.subjectList
    private val allExamList = semesterExamRepository.grandFinalClassList
    private val periodList = attendanceRepository.periodList

    private val _markInputList = semesterExamRepository.markInputList
    val markInputList: LiveData<List<MarkInputResponse.MarkInput>>
        get() = _markInputList

    private val _markScaleList = semesterExamRepository.shortCodeList
    val markScaleList: LiveData<List<MarkScaleResponse.MarkScale>>
        get() = _markScaleList

    private val _unassignedMarksList = semesterExamRepository.unassignedMarksList
    val unassignedMarksList: LiveData<List<UnAssignedResultResponse.UnAssignedResult>>
        get() = _unassignedMarksList

    private val _remarksUpdateList = semesterExamRepository.remarksUpdateList
    val remarksUpdateList: LiveData<List<RemarksUpdateResponse.RemarksUpdate>>
        get() = _remarksUpdateList

    var classConfigId: String? = null
    var groupId: String? = null
    var periodId: String? = null
    var examId: String? = null
    var subjectId: String? = null
    var examIdAtdCount: String? = null
    var remarkTitleId: String? = null
    var remarkTitleExamId: String? = null

    val isSectionEmpty = MutableLiveData<Boolean>()
    val isGroupEmpty = MutableLiveData<Boolean>()
    val isExamEmpty = MutableLiveData<Boolean>()
    val isPeriodEmpty = MutableLiveData<Boolean>()
    val isSubjectEmpty = MutableLiveData<Boolean>()
    val isGoToNext = MutableLiveData<Boolean>()
    val isListFound = semesterExamRepository.isListFound
    val isUnassignedResultFound = semesterExamRepository.isUnAssignedResultFound
    val isRemarkTitleEmpty = MutableLiveData<Boolean>()
    val sectionName = MutableLiveData<String>()

    private val grandFinalClassList = semesterExamRepository.grandFinalClassList
    private val grandFinalExamList = semesterExamRepository.grandFinalExamList
    var grandFinalExamId: String? = null
    var meritPositionExamId: String? = null
    var resultReportexamId: String? = null
    private val meritPositionExamList = semesterExamRepository.meritPositionExamList
    val studentResultReportList = semesterExamRepository.studentResultReportList
    val remarkAssignTitleList = semesterExamRepository.remarkAssignTitleList
    val remarkAssignDescription = semesterExamRepository.remarkAssignDes
    val remarkAssignStudentList = semesterExamRepository.remarkAssignStudentList
    val isRemarkListFound = semesterExamRepository.isRemarkListFound
    val attendanceCountPeriodList = attendanceRepository.periodList

    val unAssignedResult = MutableLiveData<UnAssignedResultResponse.UnAssignedResult>()

    val tempSectionList = Transformations.map(sectionList) {
        getTempSectionList(it)
    }

    val tempGroupList = Transformations.map(groupList) {
        getTempGroupList(it)
    }

    val tempExamList = Transformations.map(examList) {
        getTempExamList(it)
    }

    val tempSubjectList = Transformations.map(subjectList) {
        getTempSubjectList(it)
    }

    val tempAllExamList = Transformations.map(allExamList) {
        getTempAllExamList(it)
    }

    fun getRoleWiseSectionList() {
        semesterExamRepository.getRoleWiseSectionList()
    }

    private fun getTempSectionList(sectionList: List<SectionResponse.Section>): ArrayList<String> {
        val tempSectionList = ArrayList<String>()
        tempSectionList.add("Select Section")

        sectionList.forEach { section ->
            tempSectionList.add(section.getClassShiftSection().toString())
        }

        return tempSectionList
    }

    private fun getTempGroupList(groupList: List<GroupResponse.Group>): ArrayList<String> {
        val tempGroupList = ArrayList<String>()
        tempGroupList.add("Select Group")

        groupList.forEach { group ->
            tempGroupList.add(group.getGroupObject()!!.getName().toString())
        }

        return tempGroupList
    }

    private fun getTempExamList(examList: List<ExamResponse.Exam>): ArrayList<String> {
        val tempExamList = ArrayList<String>()
        tempExamList.add("Select Exam")

        examList.forEach { exam ->
            tempExamList.add(exam.getExamObject()!!.getName().toString())
        }

        return tempExamList
    }

    private fun getTempSubjectList(subjectList: List<SubjectResponse.Subject>): ArrayList<String> {
        val tempSubjectList = ArrayList<String>()
        tempSubjectList.add("Select Subject")

        subjectList.forEach { subject ->
            tempSubjectList.add(subject.getSubject()!!.getName().toString())
        }

        return tempSubjectList
    }

    private fun getTempAllExamList(grandFinalClassList: List<GrandFinalClassResponse.ClassSection>): ArrayList<String> {
        val tempAllExamList = ArrayList<String>()
        tempAllExamList.add("Select Exam")

        grandFinalClassList.forEach { classSection ->
            tempAllExamList.add(classSection.getClassShiftSection().toString())
        }

        return tempAllExamList
    }

    fun getSectionId(sectionName: String) {
        sectionList.value!!.forEach { section ->
            if (section.getClassShiftSection().toString().equals(sectionName, true)) {
                classConfigId = section.getClassConfigId().toString()
                studentRepository.getGroupData(classConfigId!!)
                return
            }
        }
    }


    fun getGroupId(groupName: String) {
        groupList.value!!.forEach { group ->
            if (group.getGroupObject()!!.getName().equals(groupName, true)) {
                groupId = group.getGroupObject()!!.getId().toString()
                semesterExamRepository.getExamList(classConfigId)
                return
            }
        }
    }

    fun getExamId(examName: String) {
        examList.value!!.forEach { exam ->
            if (exam.getExamObject()!!.getName().equals(examName, true)) {
                examId = exam.getExamConfigId()?.toString()
                semesterExamRepository.getSubjectList(classConfigId!!, groupId!!)
                return
            }
        }
    }

    fun getSubjectId(subjectName: String) {
        subjectList.value!!.forEach { subject ->
            if (subject.getSubject()!!.getName().equals(subjectName, true)) {
                subjectId = subject.getSubject()!!.getId().toString()
                return
            }
        }
    }

    fun getAllExamId(examName: String) {
        allExamList.value!!.forEach { exam->
            if (exam.getClassShiftSection()!!.equals(examName, true)) {
                examId = exam.getClassConfigId().toString()
            }
        }
    }

    fun getSectionIdInRemarks(sectionName: String) {
        sectionList.value!!.forEach { section ->
            if (section.getClassShiftSection().toString().equals(sectionName, true)) {
                classConfigId = section.getClassConfigId().toString()
                semesterExamRepository.getExamList(classConfigId!!)
                return
            }
        }
    }

    fun getExamIdInRemarks(examName: String) {
        examList.value!!.forEach { exam ->
            if (exam.getExamObject()!!.getName().equals(examName, true)) {
                examId = exam.getExamConfigId()!!.toString()
                return
            }
        }
    }

    fun getPeriodId(periodName: String) {
        periodList.value!!.forEach { period ->
            if (period.getName().equals(periodName, true)) {
                periodId = period.getId().toString()
                return
            }
        }
    }


    fun checkExamMarkSelection(isMarkInput: Boolean) {
        when {
            classConfigId.isNullOrEmpty() -> {
                isSectionEmpty.value = true
            }
            groupId.isNullOrEmpty() -> {
                isGroupEmpty.value = true
            }
            examId.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }
            subjectId.isNullOrEmpty() -> {
                isSubjectEmpty.value = true
            }
            else -> {
                if (isMarkInput) {
                    getMarkInputList()
                } else {
                    getMarkUpdateList()
                }
            }
        }
    }

    fun checkUnAssignedMarkSelection() {
        if (examId.isNullOrEmpty()) {
            isExamEmpty.value = true
        } else {
            semesterExamRepository.getUnAssignedResultSummary(examId!!)
        }
    }

    fun checkRemarksUpdate() {
        when {
            classConfigId.isNullOrEmpty() -> {
                isSectionEmpty.value = true
            }
            examId.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }
            else -> {
                getRemarksUpdateList()
            }
        }
    }

    fun checkRemarksUpdateForUpdating(remarksUpdateList: List<RemarksUpdateResponse.RemarksUpdate>) {
        if (remarksUpdateList.isNotEmpty()) {
            val jsonArray = JsonArray()

            remarksUpdateList.forEach {remarks ->
                if (remarks.isChecked) {
                    val jsonObj = JsonObject()

                    jsonObj.addProperty("examConfigId", remarks.getExamConfigId())//62
                    jsonObj.addProperty("identificationId", remarks.getIdentificationId())
                    jsonObj.addProperty("remarkId", remarks.getRemarkId())
                    jsonObj.addProperty("remarks", remarks.getRemarks())
                    jsonObj.addProperty("remarksTitle", remarks.getRemarksTitle())
                    jsonArray.add(jsonObj)
                }
            }

            if (jsonArray.size() > 0) {
                semesterExamRepository.updateRemarksList(jsonArray)
            } else {
                isRemarkListFound.value = false
            }
        }
    }

    private fun getMarkInputList() {
        semesterExamRepository.getMarkInputList(
            classConfigId!!, groupId!!, subjectId!!, examId!!
        )
    }

    private fun getMarkUpdateList() {
        semesterExamRepository.getMarkUpdateList(
            classConfigId!!, groupId!!, subjectId!!, examId!!
        )
    }

    private fun getRemarksUpdateList() {
        semesterExamRepository.getRemarksUpdateList(classConfigId!!, examId!!)
    }

    fun saveInputMark(markInputRqHelperList: List<ExamMarkInput.ExamMarkInputRqHelper>) {
        if (markInputRqHelperList.isNullOrEmpty()) {
            Loaders.apiError.value = "No changes detected!"
        } else {
            val examMarkInput = ExamMarkInput()
            examMarkInput.setClassConfigurationId(classConfigId)
            examMarkInput.setGroupId(groupId)
            examMarkInput.setExamConfigurationId(examId)
            examMarkInput.setSubjectId(subjectId)
            examMarkInput.setType("insert")
            examMarkInput.setExamMarkInputRqHelpers(markInputRqHelperList)

            semesterExamRepository.saveInputMark(examMarkInput)
        }
    }

    fun updateInputMark(markInputRqHelperList: List<ExamMarkInput.ExamMarkInputRqHelper>) {
        val examMarkInput = ExamMarkInput()
        examMarkInput.setClassConfigurationId(classConfigId)
        examMarkInput.setGroupId(groupId)
        examMarkInput.setExamConfigurationId(examId)
        examMarkInput.setSubjectId(subjectId)
        examMarkInput.setType("update")
        examMarkInput.setExamMarkInputRqHelpers(markInputRqHelperList)

        semesterExamRepository.updateInputMark(examMarkInput)
    }

    fun getAllExamList() {
        if (grandFinalClassList.value.isNullOrEmpty()) {
            semesterExamRepository.getGrandFinalClassData("2201")
        }
    }

    //----------------------------------------------------------------------------------------------
    fun checkGrandFinalValues() {
        when {
            classConfigId.isNullOrEmpty() -> {
                isSectionEmpty.value = true
            }
            grandFinalExamId.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }
            else -> {
                semesterExamRepository.postGrandFinalExamResultProcessData(grandFinalExamId!!)
            }
        }
    }

    fun checkMeritPositionValues() {
        when {
            classConfigId.isNullOrEmpty() -> {
                isSectionEmpty.value = true
            }
            meritPositionExamId.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }
            else -> {
                semesterExamRepository.postMeritPositionProcessData(
                    classConfigId!!,
                    meritPositionExamId!!
                )
            }
        }
    }
    fun getSectionIdForAtdCount(sectionName: String) {
        sectionList.value!!.forEach { section ->
            if (section.getClassShiftSection().toString().equals(sectionName, true)) {
                classConfigId = section.getClassConfigId().toString()
                semesterExamRepository.getExamList(sectionName)
                attendanceRepository.getPeriodListForSearchingStudentReport()
                return
            }
        }
    }


    fun checkAttendanceValues(classConfig: String, periodID: String) {
        when {
            /*classConfigId.isNullOrEmpty() -> {
                isSectionEmpty.value = true
            }
            periodId.isNullOrEmpty() -> {
                isPeriodEmpty.value = true
            }*/

            examIdAtdCount.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }
            else -> {
                semesterExamRepository.postAttendanceCountProcessData(
                    classConfig,
                    periodID,
                    examIdAtdCount!!
                )
            }
        }
    }

    fun checkResultReportsValues() {
        when {
            classConfigId.isNullOrEmpty() -> {
                isSectionEmpty.value = true
            }

            resultReportexamId.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }

            else -> {
               // isGoToNext.value = true
               getstudentResultReportsList(classConfigId!!, resultReportexamId!!)

            }
        }
    }

    fun checkRemarkAssignValues() {
        when {
            classConfigId.isNullOrEmpty() -> {
                isSectionEmpty.value = true
            }

            remarkTitleExamId.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }
            remarkTitleId.isNullOrEmpty() -> {
                isRemarkTitleEmpty.value = true
            }

            else -> {
               getRemarkAssignStudentList(classConfigId!!, remarkTitleExamId!!)
                // isGoToNext.value = true
            }
        }
    }


    fun getResultReportsExamId(examName: String) {
        examList.value!!.forEach { exam ->
            if (exam.getExamObject()!!.getName().equals(examName, true)) {
                resultReportexamId = exam.getExamConfigId()!!.toString()
                return
            }
        }
    }

    fun getExamIdForAtdCount(examName: String) {
        examList.value!!.forEach { exam ->
            if (exam.getExamObject()!!.getName().equals(examName, true)) {
                examIdAtdCount = exam.getExamObject()!!.getId().toString()
                return
            }
        }
    }

    fun getExamListForGeneralExam(classConfigId: String) {
        semesterExamRepository.getExamList(classConfigId)
    }


    fun getGrandFinalClassList() {
        if (grandFinalClassList.value.isNullOrEmpty()) {
            semesterExamRepository.getGrandFinalClassData("2102")
        }
    }

    val tempGrandFinalClassList = Transformations.map(grandFinalClassList) {
        getTempGrandFinalClassList(it)
    }

    val tempGrandFinalExamList = Transformations.map(grandFinalExamList) {
        getTempGrandFinalExamList(it)
    }

    val tempMeritPositionExamList = Transformations.map(meritPositionExamList) {
        getTempMeritPositionExamList(it)
    }


    val tempRemarkAssignTitleList = Transformations.map(remarkAssignTitleList) {
        getTempRemarkTitleList(it)
    }

    val tempAttendanceCountPeriodList = Transformations.map(attendanceCountPeriodList) {
        getTempAtdCountPeriodList(it)
    }

    fun geRemarkTitleId(titleName: String) {
        remarkAssignTitleList.value!!.forEach { remarkTitle ->
            if (remarkTitle.getRemarkTitle().equals(titleName, true)) {
                remarkTitleId = remarkTitle.geRemarkTemptId().toString()
                semesterExamRepository.getRemarksAssignDescription(remarkTitleId!!)
                return
            }
        }
    }

    fun getMeritPositionExamId(examName: String) {
        meritPositionExamList.value!!.forEach { exam ->
            if (exam.getExamObject()?.getName().equals(examName, true)) {
                meritPositionExamId = exam.getExamConfigId().toString()
                return
            }
        }
    }

    fun getMeritPositionClassId(className: String) {
        grandFinalClassList.value!!.forEach { classSection ->
            if (classSection.getClassShiftSection().toString().equals(className, true)) {
                classConfigId = classSection.getClassConfigId().toString()
                semesterExamRepository.getMeritPositionExamList(classConfigId!!)
                return
            }
        }
    }

    fun getGrandFinalExamId(examName: String) {
        grandFinalExamList.value!!.forEach { exam ->
            if (exam.getExamName().equals(examName, true)) {
                grandFinalExamId = exam.getExamConfigId().toString()
                return
            }
        }
    }

    fun getGrandFinalClassId(className: String) {
        grandFinalClassList.value!!.forEach { classSection ->
            if (classSection.getClassShiftSection().toString().equals(className, true)) {
                classConfigId = classSection.getClassConfigId().toString()
                semesterExamRepository.getGrandFinalExamList(classConfigId!!)
                return
            }
        }
    }

    fun getGeneralExamId(examName: String) {
        examList.value!!.forEach { exam ->
            if (exam.getExamObject()!!.getName().equals(examName, true)) {
                examId = exam.getExamObject()!!.getId().toString()
                return
            }
        }
    }

    fun getRemarkTitleExamId(examName: String) {
        examList.value!!.forEach { exam ->
            if (exam.getExamObject()!!.getName().equals(examName, true)) {
                remarkTitleExamId = exam.getExamConfigId().toString()
                semesterExamRepository.getRemarksAssignTitleList()
                return

            }
        }
    }

    fun postGeneralExamResultProcessData() {
        when {
            classConfigId.isNullOrEmpty() -> {
                isSectionEmpty.value = true
            }
            examId.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }
            else -> {
                semesterExamRepository.postGeneralExamResultProcessData(classConfigId!!, examId!!)
            }
        }
    }

    private fun getTempGrandFinalClassList(grandFinalClassList: List<GrandFinalClassResponse.ClassSection>): ArrayList<String> {
        val tempClassList = ArrayList<String>()
        tempClassList.add("Select Class")

        grandFinalClassList.forEach { classSection ->
            tempClassList.add(classSection.getClassShiftSection().toString())
        }

        return tempClassList
    }

    private fun getTempGrandFinalExamList(grandFinalExamList: List<GrandFinalExamResponse.GrandExam>): ArrayList<String> {
        val tempExamList = ArrayList<String>()
        tempExamList.add("Select Exam")

        grandFinalExamList.forEach { grandExam ->
            tempExamList.add(grandExam.getExamName().toString())
        }

        return tempExamList
    }

    private fun getTempMeritPositionExamList(meritPositionExamList: List<MeritPositionExamResponse.MeritPosition>): ArrayList<String> {
        val tempExamList = ArrayList<String>()
        tempExamList.add("Select Exam")

        meritPositionExamList.forEach { meritPositionExam ->
            tempExamList.add(meritPositionExam.getExamObject()?.getName().toString())
        }

        return tempExamList
    }

    private fun getTempRemarkTitleList(remarkAssignTitleList: List<RemarksTitleResponse.RemarksTitle>): ArrayList<String> {
        val tempRemarkTitleList = ArrayList<String>()
        tempRemarkTitleList.add("Select Remark Title")

        remarkAssignTitleList.forEach { remarkTitleList ->
            tempRemarkTitleList.add(remarkTitleList.getRemarkTitle().toString())
        }

        return tempRemarkTitleList
    }

    private fun getTempAtdCountPeriodList(atdCountPeriodList: List<PeriodResponse.Period>): ArrayList<String> {
        val tempAtdCountPeriodList = ArrayList<String>()
        tempAtdCountPeriodList.add("Select Period")

        atdCountPeriodList.forEach { attendacedCountPeriodList ->
            tempAtdCountPeriodList.add(attendacedCountPeriodList.getName().toString())
        }

        return tempAtdCountPeriodList
    }

    fun getstudentResultReportsList(sectionId: String, examId: String) {
       // if (studentResultReportList.value.isNullOrEmpty()) {
            semesterExamRepository.getStudentResultReportList(sectionId, examId)
       // }
    }

    fun getRemarkAssignStudentList(classConfigId: String, examConfigId: String) {
        semesterExamRepository.getRemarksAssignStudentList(classConfigId, examConfigId)
    }

    fun postRemarkAssignData(remarkAssignList: RemarkAssignPostData) {
        semesterExamRepository.postRemarksAssignData(remarkAssignList)
    }


    class SemesterExamViewModelFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SemesterExamViewModel::class.java)) {
                return SemesterExamViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}