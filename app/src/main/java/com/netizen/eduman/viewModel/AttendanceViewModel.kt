package com.netizen.eduman.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.eduman.model.TakeAtdStudentPost
import com.netizen.eduman.repository.AttendanceRepository

class AttendanceViewModel(application: Application) : AndroidViewModel(application) {

    private val attendanceRepository = AttendanceRepository(application)

    val periodList = attendanceRepository.periodList
    val takAdtPeriodList = attendanceRepository.takAdtPeriodList
    val takAdtStudentList = attendanceRepository.takAdtStudentList
    val studentAttendanceReportSummaryList = attendanceRepository.studentAttendanceReportSummaryList
    val studentAttendanceList = attendanceRepository.studentAttendanceList
    val hrAttendanceList = attendanceRepository.hrAttendanceList

    val isListFound = attendanceRepository.isListFound
    val sectionName = MutableLiveData<String>()

    var sectionId: String? = null
    var periodId: String? = null
    var date: String? = null

    fun getPeriodListForSearchingStudentReport() {
        if (periodList.value.isNullOrEmpty()) {
            attendanceRepository.getPeriodListForSearchingStudentReport()
        }
    }

    fun getPeriodListForTakeAtdStudentList(date: String, sectionID: String) {
       // if (takAdtPeriodList.value.isNullOrEmpty()) {
            attendanceRepository.getPeriodListForTakeAdtStudentList(date, sectionID)
       // }
    }

    fun getStudentListForTakeAttendance(date: String, sectionID: String, periodId: String) {
        attendanceRepository.getAllTakeAttendanceStudentList(date, sectionID, periodId)
    }

    fun getStudentAttendanceReportSummaryList(date: String, periodId: String) {
        attendanceRepository.getStudentAttendanceReportSummaryList(date, periodId)
    }

    fun postTakeAttendanceData(takeAttendanceList: TakeAtdStudentPost) {
        attendanceRepository.postTakeAttendanceData(takeAttendanceList)
    }

    fun getHRAttendanceListReports(date: String, status: String) {
        if (hrAttendanceList.value.isNullOrEmpty()) {
            attendanceRepository.getHRAttendanceReports(date, status)
        }
    }

    fun getStudentAttendanceReportList(date: String,
                                       periodId: String,
                                       configId: String,
                                       attendanceStatus: String) {
        if (studentAttendanceList.value.isNullOrEmpty()) {
            attendanceRepository.getStudentAttendanceReportList(date, periodId, configId, attendanceStatus)
        }
    }

    class AttendanceViewModelFactory(private val application: Application) :
            ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AttendanceViewModel::class.java)) {
                return AttendanceViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}