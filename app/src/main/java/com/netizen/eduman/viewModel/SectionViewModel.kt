package com.netizen.eduman.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.eduman.repository.SemesterExamRepository
import com.netizen.eduman.repository.StudentRepository

class SectionViewModel(application: Application) {

    private val semesterExamRepository = SemesterExamRepository(application)
    private val studentRepository = StudentRepository(application)

//    private var sectionList = studentRepository.sectionList
//    private var sectionList = semesterExamRepository.sectionList

    class SectionViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SectionViewModel::class.java)) {
                return SectionViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}