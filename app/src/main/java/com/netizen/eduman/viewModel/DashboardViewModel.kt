package com.netizen.eduman.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.eduman.repository.DashboardRepository

class DashboardViewModel(application: Application) : AndroidViewModel(application) {

    private val dashboardRepository = DashboardRepository(application)

    var instituteInfoMutableLiveData = dashboardRepository.instituteInfoMutableLiveData
    var errorStatus = dashboardRepository.errorStatus
    val lottieProgressDialog = dashboardRepository.lottieProgressDialog

    var smsBalanceInfoMutableLiveData = dashboardRepository.smsBalance
    var billPaymentInfoMutableLiveData = dashboardRepository.billPayment

    fun refreshInstituteInfoList(token: String) {
        //if (instituteInfoMutableLiveData?.value.isNullOrEmpty()) {
            dashboardRepository.getInstituteFromServer(token)
       // }
    }

    fun clearPreviousInstituteData() {
        dashboardRepository.clearInstituteInfoAll()
    }

    fun refreshSmsBalanceInfoList(token: String) {
        dashboardRepository.getSmsBalanceFromServer(token)
    }

    fun refreshBillPaymentInfoList(instituteId: String) {
        dashboardRepository.getBillPaymentInfoFromServer(instituteId)
    }

    class DashboardFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DashboardViewModel::class.java)) {
                return DashboardViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}