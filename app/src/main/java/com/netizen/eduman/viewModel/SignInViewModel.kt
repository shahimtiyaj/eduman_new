package com.netizen.eduman.viewModel

import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.eduman.model.LoginUser
import com.netizen.eduman.repository.SignInRepository


class SignInViewModel : ViewModel() {

    private val signInRepository = SignInRepository()

    val isUserNameEmpty = MutableLiveData<Boolean>()
    val isPasswordEmpty = MutableLiveData<Boolean>()
    val fragmentPosition = MutableLiveData<Int>()

    val lottieProgressDialog = signInRepository.lottieProgressDialog
    val isLoggedIn = signInRepository.isLoggedIn
    var accessToken = signInRepository.accessToken
    var successStatus = signInRepository.successStatus
    var errorStatus = signInRepository.errorStatus

    init {
        isUserNameEmpty.value = false
        isPasswordEmpty.value = false
        //isLoggedIn.value = false
    }

    fun onLogInClick(userName: String, password: String) {

        when {
            TextUtils.isEmpty(userName) -> {
                isUserNameEmpty.value = true
            }
            TextUtils.isEmpty(password) -> {
                isPasswordEmpty.value = true
            }
            else -> {
                val loginUser = LoginUser(userName, password)
                signInRepository.loginCheck(loginUser)
            }
        }
    }

    class SignInViewModelFactory : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SignInViewModel::class.java)) {
                return SignInViewModel() as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}