package com.netizen.eduman.utils

import android.text.InputFilter
import android.text.Spanned
import android.util.Log
import com.netizen.eduman.view.fragment.attendance.studentAttendance.StudentAttendanceReportDetailsFragment.Companion.TAG


class InputFilterMinMax(private val min: Float,
                        private val max: Float
) : InputFilter {

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence {
        try {
            val input = (dest.toString() + source.toString()).toFloat()
            if (isInRange(min, max, input)) {
                return source
            }
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
        return ""
    }

    private fun isInRange(min: Float, max: Float, input: Float): Boolean {
        return if (max > min) {
            input >= min && input <= max
        } else {
            input >= max && input <= min
        }
    }
}