package com.netizen.eduman.utils

import android.content.Context
import android.content.SharedPreferences

class AppPreferences(context: Context?) {

    private val PREFERENCE_NAME = "app_preferences"
    private val KEY_TOKEN = "access_token"
    private val KEY_POSITION_ACADEMIC_YEAR = "academic_year_position"
    private val KEY_POSITION_SECTION = "section_position"
    private val KEY_POSITION_GROUP = "group_position"
    private val KEY_POSITION_CATEGORY = "category_position"
    private val KEY_POSITION_ID_TYPE = "id_type_position"
    private val KEY_POSITION_HR_DESIGNATION = "hr_designation_position"
    private val KEY_IS_SPLASH_SHOWN = "is_splash_shown"


    private var mContext: Context? = null

    private var mPreferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null

    init {
        mContext = context
        mPreferences = mContext!!.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        editor = mPreferences!!.edit()
    }

    fun setToken(token: String?) {
        editor!!.putString(KEY_TOKEN, token)
        editor!!.apply()
    }

    fun getToken(): String? {
        var token: String? = null
        if (mPreferences!!.contains(KEY_TOKEN)) {
            token = mPreferences!!.getString(KEY_TOKEN, "")
        }
        return token
    }

    fun setAcademicYearPosition(position: Int) {
        editor!!.putInt(KEY_POSITION_ACADEMIC_YEAR, position)
        editor!!.apply()
    }

    fun getAcademicYearPosition(): Int {
        return mPreferences!!.getInt(KEY_POSITION_ACADEMIC_YEAR, 0)
    }

    fun setSectionPosition(position: Int) {
        editor!!.putInt(KEY_POSITION_SECTION, position)
        editor!!.apply()
    }

    fun getSectionPosition(): Int {
        return mPreferences!!.getInt(KEY_POSITION_SECTION, 0)
    }

    fun setGroupPosition(position: Int) {
        editor!!.putInt(KEY_POSITION_GROUP, position)
        editor!!.apply()
    }

    fun getGroupPosition(): Int {
        return mPreferences!!.getInt(KEY_POSITION_GROUP, 0)
    }

    fun setCategoryPosition(position: Int) {
        editor!!.putInt(KEY_POSITION_CATEGORY, position)
        editor!!.apply()
    }

    fun getCategoryPosition(): Int {
        return mPreferences!!.getInt(KEY_POSITION_CATEGORY, 0)
    }

    fun setIdTypePosition(position: Int) {
        editor!!.putInt(KEY_POSITION_ID_TYPE, position)
        editor!!.apply()
    }

    fun getIdTypePosition(): Int {
        return mPreferences!!.getInt(KEY_POSITION_ID_TYPE, 0)
    }

    fun setHrDesignationPosition(position: Int) {
        editor!!.putInt(KEY_POSITION_HR_DESIGNATION, position)
        editor!!.apply()
    }

    fun getHrDesignationPosition(): Int {
        return mPreferences!!.getInt(KEY_POSITION_HR_DESIGNATION, 0)
    }

    fun setIsSplashShown() {
        editor!!.putBoolean(KEY_IS_SPLASH_SHOWN, true)
        editor!!.apply()
    }

    fun isSplashShown(): Boolean {
        return mPreferences!!.getBoolean(KEY_IS_SPLASH_SHOWN, false)
    }
}