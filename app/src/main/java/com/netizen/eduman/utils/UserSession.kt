package com.netizen.eduman.utils

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor


class UserSession(context: Context?) {

    private val PREFERENCE_NAME = "user_session"
    private val IS_USER_LOGIN = "is_user_logged_in"
    private val KEY_USER_NAME = "user_name"

    private var mContext: Context? = null

    private var mPreferences: SharedPreferences? = null
    private var editor: Editor? = null

    init {
        mContext = context
        mPreferences = mContext!!.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        editor = mPreferences!!.edit()
    }

    fun setUserCredentials(userName: String?) {
        editor!!.putString(KEY_USER_NAME, userName)
        editor!!.apply()
    }

    fun getUserName(): String? {
        var userId: String? = null
        if (mPreferences!!.contains(KEY_USER_NAME)) {
            userId = mPreferences!!.getString(KEY_USER_NAME, "")
        }
        return userId
    }

    fun createUserLogInSession() {
        editor!!.putBoolean(IS_USER_LOGIN, true)
        editor!!.apply()
    }

    fun isUserLoggedIn(): Boolean {
        return mPreferences!!.getBoolean(IS_USER_LOGIN, false)
    }

    /**
     * Clear session details
     */
    fun logOutUser() { // Clearing all user data from Shared Preferences
        editor!!.putBoolean(IS_USER_LOGIN, false)
        editor!!.apply()
    }
}