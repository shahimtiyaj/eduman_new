package com.netizen.eduman.utils

import androidx.lifecycle.MutableLiveData

class Loaders {

    companion object {
        val isLoading = MutableLiveData<Boolean>()
        val isLoading1 = MutableLiveData<Boolean>()
        val isLoading2 = MutableLiveData<Boolean>()
        val isLoading3 = MutableLiveData<Boolean>()
        val isLoading4 = MutableLiveData<Boolean>()

        val apiError = MutableLiveData<String>()
        val apiSuccess = MutableLiveData<String>()

        val isLoadingRemarkExam = MutableLiveData<Boolean>()
        val isLoadingRemarkTitle = MutableLiveData<Boolean>()

        val sectionName = MutableLiveData<String>()
    }
}