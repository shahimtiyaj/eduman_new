package com.netizen.eduman.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.netizen.eduman.model.Institute

@Database(entities = [Institute.InstituteInfo::class], version = 10, exportSchema = false)
abstract class LocalDatabase : RoomDatabase() {

    abstract fun edumanDAO(): EdumanDAO

    companion object {

        @Volatile
        private var INSTANCE: LocalDatabase? = null

        @Synchronized
        fun getInstance(context: Context): LocalDatabase {

            var instance = INSTANCE

            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    LocalDatabase::class.java,
                    "local_database"
                ).fallbackToDestructiveMigration().build()

                INSTANCE = instance
            }

            return instance
        }
    }
}