package com.netizen.eduman.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.netizen.eduman.model.Institute

@Dao
interface EdumanDAO {

    @Insert
    fun insertInstituteInfo(institute: Institute.InstituteInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertInstituteInfoList(institute: Institute.InstituteInfo)

    @Query("Select * from InstituteInfo")
    fun getAllInstituteInfoList(): LiveData<List<Institute.InstituteInfo>>

    @Query("SELECT * FROM InstituteInfo WHERE instituteId = :instituteId")
    fun findByInstituteId(instituteId: String?): Institute.InstituteInfo?

    @Query("Delete from InstituteInfo")
    fun deleteAll()
}