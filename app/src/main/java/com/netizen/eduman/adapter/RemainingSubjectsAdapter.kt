package com.netizen.eduman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleRemainingSubjectLayoutBinding

class RemainingSubjectsAdapter(private val context: Context,
                               private val remainingSubjectList: List<String>?) : RecyclerView.Adapter<RemainingSubjectsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_remaining_subject_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return remainingSubjectList?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        remainingSubjectList?.get(position)?.let { holder.bind(position+1, it) }
    }

    class ViewHolder(private val itemBinding: SingleRemainingSubjectLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(position: Int, subjectName: String) {
            itemBinding.textViewSubjectNo.text = "Subject $position"
            itemBinding.textViewSubjectName.text = subjectName.trim()
        }
    }
}