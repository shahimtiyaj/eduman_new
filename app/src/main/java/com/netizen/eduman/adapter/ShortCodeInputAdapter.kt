package com.netizen.eduman.adapter

import android.content.Context
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleShortCodeLayoutBinding
import com.netizen.eduman.model.ExamMarkInput
import com.netizen.eduman.model.MarkScaleResponse
import com.netizen.eduman.utils.DecimalDigitsInputFilter
import com.netizen.eduman.utils.InputFilterMinMax


class ShortCodeInputAdapter(
    private val context: Context,
    private val markScaleList: List<MarkScaleResponse.MarkScale>,
    private val parentAdapterPosition: Int,
    private val listener: OnTextChange,
    private val markExamRqHelperInput: ExamMarkInput.ExamMarkInputRqHelper
) : RecyclerView.Adapter<ShortCodeInputAdapter.ViewHolder>() {

//    private lateinit var markExamRqHelperInput: ExamMarkInput.ExamMarkInputRqHelper

    interface OnTextChange {
        fun onTextChange(defaultId: Int, value: String, mainAdapterPosition: Int, markExamRqHelperInput: ExamMarkInput.ExamMarkInputRqHelper)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_short_code_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return markScaleList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        markExamRqHelperInput = ExamMarkInput.ExamMarkInputRqHelper()
        holder.bind(markScaleList[position])

        val totalMark = markScaleList[position].getTotalMark()!!.toInt()

        holder.itemBinding!!.editTextShortCodeNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                Log.e(TAG, s.toString() + " " + holder.itemBinding.editTextShortCodeNumber.toString())

                if (s!!.isNotEmpty()) {
                    val timer: CountDownTimer? = null
                    timer?.cancel()

                    object : CountDownTimer(1500, 1000) {
                        override fun onTick(millisUntilFinished: Long) {}
                        override fun onFinish() { //do what you wish
                            when(markScaleList[position].getDefaultId()!!) {
                                1 -> {
                                    markExamRqHelperInput.setShortCode1(s.toString())
                                }
                                2 -> {
                                    markExamRqHelperInput.setShortCode2(s.toString())
                                }
                                3 -> {
                                    markExamRqHelperInput.setShortCode3(s.toString())
                                }
                                4 -> {
                                    markExamRqHelperInput.setShortCode4(s.toString())
                                }
                                5 -> {
                                    markExamRqHelperInput.setShortCode5(s.toString())
                                }
                                6 -> {
                                    markExamRqHelperInput.setShortCode6(s.toString())
                                }
                                7 -> {
                                    markExamRqHelperInput.setShortCode7(s.toString())
                                }
                                8 -> {
                                    markExamRqHelperInput.setShortCode8(s.toString())
                                }
                            }

                            listener.onTextChange(markScaleList[position].getDefaultId()!!, s.toString(), parentAdapterPosition, markExamRqHelperInput)
                        }
                    }.start()
                }
            }
        })
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class ViewHolder(val itemBinding: SingleShortCodeLayoutBinding?) :
        RecyclerView.ViewHolder(itemBinding!!.root) {

        fun bind(markScale: MarkScaleResponse.MarkScale) {
            itemBinding!!.textViewShortCodeName.text = markScale.getShortCodeName().toString()
            itemBinding.editTextShortCodeNumber.imeOptions = EditorInfo.IME_ACTION_DONE
            itemBinding.editTextShortCodeNumber.filters = arrayOf(
                InputFilterMinMax(0.0F, markScale.getTotalMark()!!.toFloat()),
                DecimalDigitsInputFilter()
            )
        }
    }
}