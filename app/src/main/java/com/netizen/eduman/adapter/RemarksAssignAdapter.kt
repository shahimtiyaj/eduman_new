package com.netizen.eduman.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.RemarksAssignItemRowBinding
import com.netizen.eduman.databinding.SingleSaveButtonLayoutBinding
import com.netizen.eduman.model.RemarkAssignStudentListResponse

class RemarksAssignAdapter(
    private val context: Context,
    private val remarkAssignStudentList: List<RemarkAssignStudentListResponse.StudentRemarkAssign>,
    private val listener: OnSaveButtonClick
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val remarkInputRqHelperList = ArrayList<RemarkAssignStudentListResponse.StudentRemarkAssign>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == remarkAssignStudentList.size) {
            return ButtonViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.single_save_button_layout,
                    parent,
                    false
                )
            )
        } else {
            return RemarkAssignViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.remarks_assign_item_row,
                    parent, false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return remarkAssignStudentList.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface OnSaveButtonClick {
        fun onSave()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            if (position == remarkAssignStudentList.size) {
                (holder as ButtonViewHolder).bind()
                holder.itemBinding.buttonSave.setOnClickListener {
                    listener.onSave()
                }
            } else {
                (holder as RemarkAssignViewHolder).bind(remarkAssignStudentList[position])

                remarkAssignStudentList.get(position).getSelected()
                    .let { holder.itemBinding.remarksAssignCheck.isChecked = it }
                holder.itemBinding.remarksAssignCheck.tag = position
                holder.itemBinding.remarksAssignCheck.setOnClickListener {
                    val pos = holder.itemBinding.remarksAssignCheck.tag as Int
                    if (remarkAssignStudentList.get(pos).getSelected()) {
                        remarkAssignStudentList.get(pos).setSelected(false)
                    } else {
                        remarkAssignStudentList.get(pos).setSelected(true)
                    }
                }
            }

        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class RemarkAssignViewHolder(val itemBinding: RemarksAssignItemRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(remarkAssignReport: RemarkAssignStudentListResponse.StudentRemarkAssign) {
            itemBinding.remarksAssignStdId.text = remarkAssignReport.getCustomStudentId()
            itemBinding.remarksAssignStdRoll.text = remarkAssignReport.getStudentRoll()
            itemBinding.remarksAssignStdName.text = remarkAssignReport.getStudentName()
            itemBinding.remarksAssignStdTotalMarks.text = remarkAssignReport.getTotalMarks().toString()
            itemBinding.remarksAssignStdGrade.text = remarkAssignReport.getLetterGrade()
            itemBinding.remarksAssignStdGpa.text = remarkAssignReport.getGpa()
        }
    }

    class ButtonViewHolder(val itemBinding: SingleSaveButtonLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind() {
            itemBinding.buttonSave.setOnClickListener {
                Log.e("TEST", "Clicked")
            }
        }
    }

    companion object {
        private val TAG = RemarksAssignAdapter::class.java.simpleName
    }
}