package com.netizen.eduman.adapter

import android.content.Context
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.netizen.eduman.R
import com.netizen.eduman.model.HrReport
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class HRListAdapter(private val mContext: Context, private val hrList: List<HrReport.HRreportData?>) :
    RecyclerView.Adapter<HRListAdapter.HRViewHolder>(),
    Filterable {
    private var filteredresulthrList: List<HrReport.HRreportData?>? = null

    inner class HRViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var hr_id_: TextView
        var hr_name: TextView
        var hr_gender: TextView
        var hr_religion: TextView
        var hr_category: TextView
        var hr_designation: TextView
        var hr_blood_grp: TextView
        var hr_mobile_no: TextView

        var mProfileImageView: CircleImageView

        init {
            hr_id_ = view.findViewById(R.id.hr_id) as TextView
            hr_name = view.findViewById(R.id.hr_name) as TextView
            hr_gender = view.findViewById(R.id.hr_gender) as TextView
            hr_religion = view.findViewById(R.id.hr_religion) as TextView
            hr_category = view.findViewById(R.id.hr_category) as TextView
            hr_designation = view.findViewById(R.id.hr_designation) as TextView
            hr_blood_grp = view.findViewById(R.id.hr_blood_grp) as TextView
            hr_mobile_no = view.findViewById(R.id.hr_mobile) as TextView

            mProfileImageView = view.findViewById(R.id.hr_image_id) as CircleImageView
        }
    }

    init {
        this.filteredresulthrList = hrList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HRViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.hr_list_row, parent, false)
        // Return a new holder instance
        return HRViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: HRViewHolder, position: Int) {
        try {
        // Get the item model based on position
        val hr = filteredresulthrList?.get(position)

        holder.hr_id_.text = hr?.getHr_id()
        holder.hr_name.text = hr?.getHr_name()
        holder.hr_gender.text = hr?.getHr_gender()
        holder.hr_religion.text = hr?.getHr_religion()
        holder.hr_category.text = hr?.getHr_category()
        holder.hr_designation.text = hr?.getHr_designation()
        if (hr?.getHr_blood_grp().isNullOrEmpty()) {
            holder.hr_blood_grp.text = "---"
        } else {
            holder.hr_blood_grp.text = hr?.getHr_blood_grp()
        }

        holder.hr_mobile_no.text = hr?.getHr_phone()

        if (filteredresulthrList?.get(position)?.getHr_image() != null || filteredresulthrList?.get(position)?.getHr_image() != "") {
            Glide.with(mContext)
                .load(Base64.decode(filteredresulthrList?.get(position)?.getHr_image().toString(), Base64.DEFAULT))
                .asBitmap()
                .placeholder(R.drawable.ic_boy_icon)
                .into(holder.mProfileImageView)
        }

        } catch (e: Exception) {
            Log.e("EXCEPTION", e.toString())
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredresulthrList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredresulthrList = hrList
                } else {
                    val filteredList = ArrayList<HrReport.HRreportData?>()
                    for (row in hrList) {
                        // here we are looking for name or phone number match
                        if (row?.getHr_id()!!.toLowerCase().contains(charString.toLowerCase()) || row.getHr_name()!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredresulthrList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredresulthrList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredresulthrList = filterResults.values as ArrayList<HrReport.HRreportData>
                notifyDataSetChanged()
            }
        }
    }
}