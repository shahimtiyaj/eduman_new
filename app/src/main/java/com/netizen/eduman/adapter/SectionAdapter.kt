package com.netizen.eduman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleSpinnerLayoutBinding
import com.netizen.eduman.utils.Loaders

class SectionAdapter(private val context: Context,
                     private val sectionList: List<String>
) : RecyclerView.Adapter<SectionAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_spinner_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return sectionList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemBinding.textViewSpinnerItem.text = sectionList[position]
        if (position == (sectionList.size - 1)) {
            holder.itemBinding.viewUnderline.visibility = View.GONE
        }

        holder.itemBinding.textViewSpinnerItem.setOnClickListener {
            Loaders.sectionName.postValue(sectionList[position])
        }
    }

    class ViewHolder(val itemBinding: SingleSpinnerLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root)
}