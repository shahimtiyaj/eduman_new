package com.netizen.eduman.adapter

import android.content.Context
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleShortCodeLayoutBinding
import com.netizen.eduman.model.MarkInputResponse
import com.netizen.eduman.model.MarkScaleResponse
import com.netizen.eduman.utils.DecimalDigitsInputFilter
import com.netizen.eduman.utils.InputFilterMinMax


class ShortCodeUpdateAdapter(
    private val context: Context,
    private val markInput: MarkInputResponse.MarkInput,
    private val markScaleList: List<MarkScaleResponse.MarkScale>,
    private val parentAdapterPosition: Int,
    private val listener: OnTextChange
) : RecyclerView.Adapter<ShortCodeUpdateAdapter.ViewHolder>() {


    interface OnTextChange {
        fun onTextChange(defaultId: Int, value: Double, mainAdapterPosition: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_short_code_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return markScaleList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(markScaleList[position], markInput)

        holder.itemBinding!!.editTextShortCodeNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                Log.e(TAG, s.toString() + " " + mainAdapterPosition)

                val timer: CountDownTimer? = null
                timer?.cancel()

                object : CountDownTimer(1500, 1000) {
                    override fun onTick(millisUntilFinished: Long) {}
                    override fun onFinish() { //do what you wish
                        if (s.toString() == "") {
                            listener.onTextChange(markScaleList[position].getDefaultId()!!, 0.0, parentAdapterPosition)
                        } else {
                            listener.onTextChange(markScaleList[position].getDefaultId()!!, s.toString().toDouble(), parentAdapterPosition)
                        }
                    }
                }.start()
            }
        })
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class ViewHolder(val itemBinding: SingleShortCodeLayoutBinding?) :
        RecyclerView.ViewHolder(itemBinding!!.root) {

        fun bind(markScale: MarkScaleResponse.MarkScale, markInput: MarkInputResponse.MarkInput) {
            when(markScale.getDefaultId()) {
                1 -> {
                    itemBinding!!.editTextShortCodeNumber.setText(markInput.getShortCode1().toString())
                }
                2 -> {
                    itemBinding!!.editTextShortCodeNumber.setText(markInput.getShortCode2().toString())
                }
                3 -> {
                    itemBinding!!.editTextShortCodeNumber.setText(markInput.getShortCode3().toString())
                }
                4 -> {
                    itemBinding!!.editTextShortCodeNumber.setText(markInput.getShortCode4().toString())
                }
                5 -> {
                    itemBinding!!.editTextShortCodeNumber.setText(markInput.getShortCode5().toString())
                }
                6 -> {
                    itemBinding!!.editTextShortCodeNumber.setText(markInput.getShortCode6().toString())
                }
                7 -> {
                    itemBinding!!.editTextShortCodeNumber.setText(markInput.getShortCode7().toString())
                }
                8 -> {
                    itemBinding!!.editTextShortCodeNumber.setText(markInput.getShortCode8().toString())
                }
            }

            itemBinding!!.editTextShortCodeNumber.imeOptions = EditorInfo.IME_ACTION_DONE
            itemBinding.editTextShortCodeNumber.filters = arrayOf(
                InputFilterMinMax(0.0F, markScale.getTotalMark()!!.toFloat()),
                DecimalDigitsInputFilter()
            )
            itemBinding.textViewShortCodeName.text = markScale.getShortCodeName().toString()
        }
    }
}