package com.netizen.eduman.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.HrAttendanceReportRowBinding
import com.netizen.eduman.model.HRAttendanceReportsResponse

class HrAttendanceAdapter(
    private val context: Context,
    private val hrPresentList: List<HRAttendanceReportsResponse.HRAttendanceReports>
) : RecyclerView.Adapter<HrAttendanceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.hr_attendance_report_row,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return hrPresentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(hrPresentList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: HrAttendanceReportRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(attendanceReport: HRAttendanceReportsResponse.HRAttendanceReports) {
            itemBinding.hrPresentId.text = attendanceReport.getHr_p_id()
            itemBinding.hrPresentName.text = attendanceReport.getHr_p_name()
            if (attendanceReport.getHr_p_category().isNullOrEmpty()) {
                itemBinding.hrPresentCategory.text = "---"

            } else {
                itemBinding.hrPresentCategory.text = attendanceReport.getHr_p_category()
            }
            itemBinding.hrPresentDesignation.text = attendanceReport.getHr_p_designation()
            itemBinding.hrPresentMobileNo.text = attendanceReport.getHr_p_mobile()
            if (attendanceReport.getHr_p_in_time().isNullOrEmpty()) {
               itemBinding.hrInTimeLayout.visibility = View.GONE
            } else {
                itemBinding.hrPresentInTime.text = attendanceReport.getHr_p_in_time()
            }
        }
    }

    companion object {
        private val TAG = HrAttendanceAdapter::class.java.simpleName
    }
}