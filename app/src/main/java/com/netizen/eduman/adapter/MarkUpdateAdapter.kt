package com.netizen.eduman.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleMarkInputLayoutBinding
import com.netizen.eduman.databinding.SingleSaveButtonLayoutBinding
import com.netizen.eduman.model.ExamMarkInput
import com.netizen.eduman.model.MarkInputResponse
import com.netizen.eduman.model.MarkScaleResponse
import com.netizen.eduman.view.fragment.attendance.studentAttendance.StudentAttendanceReportDetailsFragment.Companion.TAG

class MarkUpdateAdapter(private val context: Context,
                        private val markInputList: List<MarkInputResponse.MarkInput>,
                        private val markScaleList: List<MarkScaleResponse.MarkScale>,
                        private val listener: OnSaveButtonClick
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ShortCodeUpdateAdapter.OnTextChange {

    private val markInputRqHelperList = ArrayList<ExamMarkInput.ExamMarkInputRqHelper>()
    private val viewPool = RecyclerView.RecycledViewPool()

    interface OnSaveButtonClick {
        fun onUpdate(markInputRqHelperList: List<ExamMarkInput.ExamMarkInputRqHelper>)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == markInputList.size) {
            return ButtonViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.single_save_button_layout,
                    parent,
                    false
                )
            )
        } else {
            return InputViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.single_mark_input_layout,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return (markInputList.size + 1)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == markInputList.size) {
            (holder as ButtonViewHolder).bind()
            holder.itemBinding.buttonSave.setOnClickListener {
//                Log.e(TAG, markInputRqHelperList[0].getShortCode1().toString())
//                Log.e(TAG, markInputRqHelperList.toString())
                processUpdateData()
            }
        } else {
            (holder as InputViewHolder).bind(markInputList[position])
            holder.itemBinding.recyclerViewShortCode.setRecycledViewPool(viewPool) // For keeping the data in the view.

            holder.itemBinding.recyclerViewShortCode.layoutManager = LinearLayoutManager(context)
            holder.itemBinding.recyclerViewShortCode.setHasFixedSize(true)
            holder.itemBinding.recyclerViewShortCode.adapter = ShortCodeUpdateAdapter(
                context,
                markInputList[position],
                markScaleList,
                position,
                this)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onTextChange(defaultId: Int, value: Double, mainAdapterPosition: Int) {
        Log.e(TAG, "$defaultId $value $mainAdapterPosition")

        when(defaultId) {
            1 -> {
                markInputList[mainAdapterPosition].setShortCode1(value)
            }
            2 -> {
                markInputList[mainAdapterPosition].setShortCode2(value)
            }
            3 -> {
                markInputList[mainAdapterPosition].setShortCode3(value)
            }
            4 -> {
                markInputList[mainAdapterPosition].setShortCode4(value)
            }
            5 -> {
                markInputList[mainAdapterPosition].setShortCode5(value)
            }
            6 -> {
                markInputList[mainAdapterPosition].setShortCode6(value)
            }
            7 -> {
                markInputList[mainAdapterPosition].setShortCode7(value)
            }
            8 -> {
                markInputList[mainAdapterPosition].setShortCode8(value)
            }
        }
    }

    private fun processUpdateData() {
        markInputRqHelperList.clear()

        markInputList.forEach {
            val markExamRqHelperInput = ExamMarkInput.ExamMarkInputRqHelper()

            markExamRqHelperInput.setIdentificationId(it.getIdentificationId().toString())
            markExamRqHelperInput.setMarkInputId(it.getMarkinputId().toString())
            markExamRqHelperInput.setShortCode1(it.getShortCode1().toString())
            markExamRqHelperInput.setShortCode2(it.getShortCode2().toString())
            markExamRqHelperInput.setShortCode3(it.getShortCode3().toString())
            markExamRqHelperInput.setShortCode4(it.getShortCode4().toString())
            markExamRqHelperInput.setShortCode5(it.getShortCode5().toString())
            markExamRqHelperInput.setShortCode6(it.getShortCode6().toString())
            markExamRqHelperInput.setShortCode7(it.getShortCode7().toString())
            markExamRqHelperInput.setShortCode8(it.getShortCode8().toString())

            markInputRqHelperList.add(markExamRqHelperInput)
        }

        listener.onUpdate(markInputRqHelperList)
    }

    class InputViewHolder(val itemBinding: SingleMarkInputLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(markInput: MarkInputResponse.MarkInput) {
            itemBinding.textViewId.text = markInput.getCustomStudentId().toString()
            itemBinding.textViewRoll.text = markInput.getStudentRoll().toString()
            itemBinding.textViewName.text = markInput.getStudentName().toString()
        }
    }

    class ButtonViewHolder(val itemBinding: SingleSaveButtonLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind() {
            itemBinding.buttonSave.text = "Update"
        }
    }
}