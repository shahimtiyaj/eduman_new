package com.netizen.eduman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleMarkInputLayoutBinding
import com.netizen.eduman.databinding.SingleSaveButtonLayoutBinding
import com.netizen.eduman.model.ExamMarkInput
import com.netizen.eduman.model.MarkInputResponse
import com.netizen.eduman.model.MarkScaleResponse
import java.security.NoSuchAlgorithmException

class MarkInputAdapter(
    private val context: Context,
    private val markInputList: List<MarkInputResponse.MarkInput>,
    private val markScaleList: List<MarkScaleResponse.MarkScale>,
    private val listener: OnSaveButtonClick
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ShortCodeInputAdapter.OnTextChange {

    private val viewPool = RecyclerView.RecycledViewPool()

    companion object {
        val markInputRqHelperList = ArrayList<ExamMarkInput.ExamMarkInputRqHelper>()
    }

    interface OnSaveButtonClick {
        fun onSave(markInputRqHelperList: List<ExamMarkInput.ExamMarkInputRqHelper>)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == markInputList.size) {
            return ButtonViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.single_save_button_layout,
                    parent,
                    false
                )
            )
        } else {
            return InputViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.single_mark_input_layout,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return (markInputList.size + 1)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == markInputList.size) {
            (holder as ButtonViewHolder).bind()
            holder.itemBinding.buttonSave.setOnClickListener {
//                Log.e(TAG, markInputRqHelperList[0].getShortCode1().toString())
//                Log.e(TAG, markInputRqHelperList.toString())
                listener.onSave(markInputRqHelperList)
            }
        } else {
            (holder as InputViewHolder).bind(markInputList[position])

            val markExamRqHelperInput = ExamMarkInput.ExamMarkInputRqHelper()
            markExamRqHelperInput.setIdentificationId(
                markInputList[position].getIdentificationId().toString()
            )
            markInputRqHelperList.add(markExamRqHelperInput)

            holder.itemBinding.recyclerViewShortCode.setRecycledViewPool(viewPool) // For keeping the data in the view.
            holder.itemBinding.recyclerViewShortCode.layoutManager = LinearLayoutManager(context)
            holder.itemBinding.recyclerViewShortCode.setHasFixedSize(true)
            holder.itemBinding.recyclerViewShortCode.adapter = ShortCodeInputAdapter(
                context,
                markScaleList,
                position,
                this,
                markExamRqHelperInput
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onTextChange(
        defaultId: Int,
        value: String,
        mainAdapterPosition: Int,
        markExamRqHelperInput: ExamMarkInput.ExamMarkInputRqHelper
    ) {
//        Log.e(TAG, "$defaultId $value $mainAdapterPosition")

//        if (markInputRqHelperList.contains(markExamRqHelperInput)) {
//            markInputRqHelperList[markInputRqHelperList.indexOf(markExamRqHelperInput)] = markExamRqHelperInput
//        } else {
//            markExamRqHelperInput.setIdentificationId(markInputList[mainAdapterPosition].getIdentificationId().toString())
//            markInputRqHelperList.add(markExamRqHelperInput)
//        }

        try {
            markInputRqHelperList[markInputRqHelperList.indexOf(markExamRqHelperInput)] =
                markExamRqHelperInput
        }
        catch (e: IllegalStateException) {
            e.printStackTrace()
        }
        catch (e: NoSuchAlgorithmException) {
            throw AssertionError(e)
        } catch (e: Throwable) {
            e.printStackTrace()
        }

        catch (e: Exception) {
            e.printStackTrace()
        }

//        markInputRqHelperList.forEach {
//            if (it.getAdapterPosition() == mainAdapterPosition && it.getDefaultId() == defaultId) {
//                when(defaultId) {
//                    1 -> {
//                        it.setShortCode1(value)
//                    }
//                    2 -> {
//                        it.setShortCode2(value)
//                    }
//                    3 -> {
//                        it.setShortCode3(value)
//                    }
//                    4 -> {
//                        it.setShortCode4(value)
//                    }
//                    5 -> {
//                        it.setShortCode5(value)
//                    }
//                    6 -> {
//                        it.setShortCode6(value)
//                    }
//                    7 -> {
//                        it.setShortCode7(value)
//                    }
//                    8 -> {
//                        it.setShortCode8(value)
//                    }
//                }
//
//                return
//            }
//        }
//
//        val markExamRqHelperInput = ExamMarkInput.ExamMarkInputRqHelper()
//        markExamRqHelperInput.setAdapterPosition(mainAdapterPosition)
//        markExamRqHelperInput.setDefaultId(defaultId)
//        markExamRqHelperInput.setIdentificationId(markInputList[mainAdapterPosition].getIdentificationId().toString())
//
//        when(defaultId) {
//            1 -> {
//                markExamRqHelperInput.setShortCode1(value)
//            }
//            2 -> {
//                markExamRqHelperInput.setShortCode2(value)
//            }
//            3 -> {
//                markExamRqHelperInput.setShortCode3(value)
//            }
//            4 -> {
//                markExamRqHelperInput.setShortCode4(value)
//            }
//            5 -> {
//                markExamRqHelperInput.setShortCode5(value)
//            }
//            6 -> {
//                markExamRqHelperInput.setShortCode6(value)
//            }
//            7 -> {
//                markExamRqHelperInput.setShortCode7(value)
//            }
//            8 -> {
//                markExamRqHelperInput.setShortCode8(value)
//            }
//        }
//
//        markInputRqHelperList.add(markExamRqHelperInput)
    }

    class InputViewHolder(val itemBinding: SingleMarkInputLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(markInput: MarkInputResponse.MarkInput) {
            itemBinding.textViewId.text = markInput.getCustomStudentId().toString()
            itemBinding.textViewRoll.text = markInput.getStudentRoll().toString()
            itemBinding.textViewName.text = markInput.getStudentName().toString()
        }
    }

    class ButtonViewHolder(val itemBinding: SingleSaveButtonLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind() {

        }
    }
}