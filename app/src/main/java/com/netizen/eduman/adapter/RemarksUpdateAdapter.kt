package com.netizen.eduman.adapter

import android.content.Context
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleRemarksUpdateLayoutBinding
import com.netizen.eduman.databinding.SingleSaveButtonLayoutBinding
import com.netizen.eduman.model.RemarksUpdateResponse
import com.netizen.eduman.view.fragment.attendance.studentAttendance.StudentAttendanceReportDetailsFragment.Companion.TAG
import com.netizen.eduman.viewModel.SemesterExamViewModel

class RemarksUpdateAdapter(private val context: Context,
                           private val remarksUpdateList: List<RemarksUpdateResponse.RemarksUpdate>,
                           private val semesterExamViewModel: SemesterExamViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == remarksUpdateList.size) {
            return ButtonViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.single_save_button_layout,
                    parent,
                    false
                )
            )
        } else {
            return ItemViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.single_remarks_update_layout,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return (remarksUpdateList.size + 1)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            if (position == remarksUpdateList.size) {
                (holder as ButtonViewHolder).bind()

                holder.itemBinding.buttonSave.setOnClickListener {
                    semesterExamViewModel.checkRemarksUpdateForUpdating(remarksUpdateList)
                }
            } else {
                (holder as ItemViewHolder).bind(remarksUpdateList[position])

                holder.itemBinding.editTextRemarks.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {

                    }

                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {

                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        val timer: CountDownTimer? = null
                        if (s!!.isNotEmpty()) {
                            timer?.cancel()

                            object : CountDownTimer(1500, 1000) {
                                override fun onTick(millisUntilFinished: Long) {}
                                override fun onFinish() { //do what you wish
                                    remarksUpdateList[position].setRemarks(s.toString())
                                }
                            }.start()
                        }
                    }
                })
            }
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class ItemViewHolder(val itemBinding: SingleRemarksUpdateLayoutBinding): RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(remarksUpdate: RemarksUpdateResponse.RemarksUpdate) {
            itemBinding.textViewName.text = remarksUpdate.getStudentName()
            itemBinding.textViewId.text = remarksUpdate.getCustomStudentId()
            itemBinding.textViewRoll.text = remarksUpdate.getStudentRoll().toString()
            itemBinding.textViewTotalMark.text = remarksUpdate.getTotalMarks().toString()
            itemBinding.textViewGpa.text = remarksUpdate.getGpa().toString()
            itemBinding.textViewGrade.text = remarksUpdate.getLetterGrade().toString()
            itemBinding.editTextRemarks.setText(remarksUpdate.getRemarks().toString())

            itemBinding.checkBoxMark.setOnCheckedChangeListener{ buttonView, isChecked ->
                remarksUpdate.isChecked = isChecked
            }
        }
    }

    class ButtonViewHolder(val itemBinding: SingleSaveButtonLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind() {
            itemBinding.buttonSave.text = "Update"
        }
    }
}