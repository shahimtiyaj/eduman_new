package com.netizen.eduman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.model.TakeAtdStudentResponse
import java.util.*

class AttendanceAdapter(private val mContext: Context,
                        private val attendancesList: List<TakeAtdStudentResponse.TakAtdStudent>,
                        private val listener: AttendanceAdapter.OnSaveButtonClick
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var filteredatdList: List<TakeAtdStudentResponse.TakAtdStudent>? = null

    interface OnSaveButtonClick {
        fun onSave(isChecked: Boolean)
    }

    init {
        this.filteredatdList = attendancesList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == attendancesList.size) {
            // Inflate the custom layout
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.single_attendance_layout, parent, false)
            // Return a new holder instance
            ButtonViewHolder(itemView)
        } else {
            // Inflate the custom layout
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.take_attendance_item_row, parent, false)
            // Return a new holder instance
            AttendanceViewHolder(itemView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == attendancesList.size) {
            var checked = false

            (holder as ButtonViewHolder).absentCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
                checked = isChecked
            }

            holder.saveButton.setOnClickListener {
                listener.onSave(checked)
            }
        } else {
            // Get the item model based on position
            val attendance = filteredatdList?.get(position)

            // Set item views based on our views and data model
            (holder as AttendanceViewHolder).student_id.text = attendance?.getCustomStudentId()
            holder.student_roll.text = attendance?.getStudentRoll()
            holder.student_name.text = attendance?.getStudentName()
            holder.student_gender.text = attendance?.getStudentGender()

            filteredatdList?.get(position)?.getSelected()?.let { holder.checkBox.isChecked = it }
            holder.checkBox.tag = position

            holder.checkBox.setOnClickListener {
                val pos = holder.checkBox.tag as Int

                if (filteredatdList?.get(pos)?.getSelected()!!) {
                    filteredatdList?.get(pos)?.setSelected(false)

                } else {
                    filteredatdList?.get(pos)?.setSelected(true)
                }
            }
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return (filteredatdList?.size!! + 1)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredatdList = attendancesList
                } else {
                    val filteredList = ArrayList<TakeAtdStudentResponse.TakAtdStudent>()
                    for (row in attendancesList) {
                        // here we are looking for name or phone number match
                        if (row.getCustomStudentId()!!.toLowerCase().contains(charString.toLowerCase()) || row.getStudentName()!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredatdList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredatdList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredatdList = filterResults.values as ArrayList<TakeAtdStudentResponse.TakAtdStudent>
                notifyDataSetChanged()
            }
        }
    }

    class AttendanceViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var student_id: TextView
        var student_roll: TextView
        var student_name: TextView
        var student_gender: TextView
        var checkBox: CheckBox

        init {
            student_id = view.findViewById<View>(R.id.attendance_std_id) as TextView
            student_roll = view.findViewById<View>(R.id.attendance_std_roll) as TextView
            student_name = view.findViewById<View>(R.id.attendance_std_name) as TextView
            student_gender = view.findViewById<View>(R.id.attendance_std_gender) as TextView
            checkBox = view.findViewById<View>(R.id.checkbox) as CheckBox
        }
    }

    class ButtonViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var saveButton: Button = view.findViewById(R.id.button_save)
        var absentCheckBox: CheckBox = view.findViewById(R.id.absent_sms_check)
    }
}
