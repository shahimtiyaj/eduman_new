package com.netizen.eduman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleUnassignedSubjectReportLayoutBinding
import com.netizen.eduman.model.UnAssignedResultResponse
import com.netizen.eduman.viewModel.SemesterExamViewModel

class UnAssignMarksDetailsAdapter(private val context: Context,
                                  private val unAssignMarkList: List<UnAssignedResultResponse.UnAssignedResult>,
                                  private val semesterExamViewModel: SemesterExamViewModel
) : RecyclerView.Adapter<UnAssignMarksDetailsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_unassigned_subject_report_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return unAssignMarkList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(unAssignMarkList[position])

        holder.itemBinding.imageViewRemainingSubjectNames.setOnClickListener {
            semesterExamViewModel.isGoToNext.value = true
            semesterExamViewModel.unAssignedResult.value = unAssignMarkList[position]
        }
    }

    class ViewHolder(val itemBinding: SingleUnassignedSubjectReportLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(unassignedResult: UnAssignedResultResponse.UnAssignedResult) {
            itemBinding.textViewSection.text = unassignedResult.getSectionName().toString()
            itemBinding.textViewTotalSubject.text = unassignedResult.getNumOfTotalSubjects().toString()
            itemBinding.textViewInputtedSubject.text = unassignedResult.getNumOfMarkInputtedSubjects().toString()
            itemBinding.textViewRemainingSubjects.text = unassignedResult.getNumOfMarkUnInputtedSubjects().toString()
        }
    }
}