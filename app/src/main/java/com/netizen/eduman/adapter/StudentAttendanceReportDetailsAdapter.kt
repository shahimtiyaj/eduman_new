package com.netizen.eduman.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleStudentAttendanceReportDetailsLayoutBinding
import com.netizen.eduman.model.StudentAttendanceReportDetailsResponse

class StudentAttendanceReportDetailsAdapter(
    private val context: Context,
    private val attendanceListDetailDetails: List<StudentAttendanceReportDetailsResponse.StudentAttendanceReportDetails>
) : RecyclerView.Adapter<StudentAttendanceReportDetailsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.single_student_attendance_report_details_layout,
            parent, false
        ))
    }

    override fun getItemCount(): Int {
        return attendanceListDetailDetails.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(attendanceListDetailDetails[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: SingleStudentAttendanceReportDetailsLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(attendanceReport: StudentAttendanceReportDetailsResponse.StudentAttendanceReportDetails) {
            itemBinding.textViewId.text = attendanceReport.getStudentId().toString()
            itemBinding.textViewRoll.text = attendanceReport.getStudentRoll().toString()
            itemBinding.textViewName.text = attendanceReport.getStudentName().toString()
            itemBinding.textViewGender.text = attendanceReport.getGender().toString()
            itemBinding.textViewMobile.text = attendanceReport.getMobileNo().toString()

            if (attendanceReport.getAttendanceStatus().toString().isEmpty() ||
                attendanceReport.getAttendanceStatus().toString().equals("null", true)) {
                itemBinding.textViewStatus.text = "---"
            } else {
                itemBinding.textViewStatus.text = attendanceReport.getAttendanceStatus().toString()
            }
        }
    }

    companion object {
        private val TAG = StudentAttendanceReportDetailsAdapter::class.java.simpleName
    }
}