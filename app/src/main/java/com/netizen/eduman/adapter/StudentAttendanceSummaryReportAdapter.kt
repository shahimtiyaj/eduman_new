package com.netizen.eduman.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleStudentAttendanceSummaryLayoutBinding
import com.netizen.eduman.model.AttendanceReportSummaryResponse
import es.dmoral.toasty.Toasty

class StudentAttendanceSummaryReportAdapter(
    private val context: Context,
    private val summaryReportList: List<AttendanceReportSummaryResponse.AttendanceReportSummary>,
    private val onNextClick: OnNextClick
) : RecyclerView.Adapter<StudentAttendanceSummaryReportAdapter.ViewHolder>() {

    companion object {
        private val TAG = StudentAttendanceSummaryReportAdapter::class.java.simpleName
    }

    interface OnNextClick {
        fun showDetails(sectionName: String,
                        classConfigId: String,
                        attendanceDate: String,
                        status: String,
                        header: String) {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.single_student_attendance_summary_layout,
                parent,
                false)
        )
    }

    override fun getItemCount(): Int {
        return summaryReportList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(summaryReportList[position])

            val sectionName = summaryReportList[position].getClassName() + "-" +
                    summaryReportList[position].getShiftName() + "-" +
                    summaryReportList[position].getSectionName()

            val dateList: List<String> = summaryReportList[position].getAttendanceDate().toString().split("-")
            val date = dateList[1] + "/" + dateList[2] + "/" + dateList[0]

            holder.itemBinding.imageViewTotalNext.setOnClickListener {
                if (summaryReportList[position].getTotalStds()!! > 0) {
                    onNextClick.showDetails(sectionName,
                        summaryReportList[position].getClassConfigId().toString(),
                        date,
                        "5",
                        "Student Attendance List")
                } else {
                    showToastyError()
                }
            }

            holder.itemBinding.imageViewTotalPresentNext.setOnClickListener {
                if (summaryReportList[position].getTotalPresentPercent()!! > 0) {
                    onNextClick.showDetails(sectionName,
                        summaryReportList[position].getClassConfigId().toString(),
                        date,
                        "1",
                        "Present Student List")
                } else {
                    showToastyError()
                }
            }

            holder.itemBinding.imageViewTotalAbsentNext.setOnClickListener {
                if (summaryReportList[position].getTotalAbsentPercent()!! > 0) {
                    onNextClick.showDetails(sectionName,
                        summaryReportList[position].getClassConfigId().toString(),
                        date,
                        "2",
                        "Absent Student List")
                } else {
                    showToastyError()
                }
            }

            holder.itemBinding.imageViewTotalLeaveNext.setOnClickListener {
                if (summaryReportList[position].getTotalLeavePercent()!! > 0) {
                    onNextClick.showDetails(sectionName,
                        summaryReportList[position].getClassConfigId().toString(),
                        date,
                        "3",
                        "Leave Student List")
                } else {
                    showToastyError()
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    private fun showToastyError() {
        Toasty.error(context, "Nothing to show!", Toasty.LENGTH_LONG).show()
    }

    class ViewHolder(val itemBinding: SingleStudentAttendanceSummaryLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(attendanceSummary: AttendanceReportSummaryResponse.AttendanceReportSummary) {
            itemBinding.textViewSection.text = attendanceSummary.getClassName().plus(
                "-" + attendanceSummary.getShiftName() + "-" +
                    attendanceSummary.getSectionName())
            itemBinding.textViewTotal.text = attendanceSummary.getTotalStds().toString()
            itemBinding.textViewPresent.text = attendanceSummary.getPresentStds().toString().plus(
                " (" + attendanceSummary.getTotalPresentPercent().toString() + "%)")
            itemBinding.textViewAbsent.text = attendanceSummary.getAbsentStds().toString().plus(
                " (" + attendanceSummary.getTotalAbsentPercent().toString() + "%)")
            itemBinding.textViewLeave.text = attendanceSummary.getLeaveStds().toString().plus(
                " (" + attendanceSummary.getTotalLeavePercent().toString() + "%)")
        }
    }
}