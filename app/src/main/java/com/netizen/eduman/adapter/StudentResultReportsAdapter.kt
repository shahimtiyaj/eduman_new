package com.netizen.eduman.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.FragmentStudentResultReportsItemBinding
import com.netizen.eduman.model.StudentResultReportResponse

class StudentResultReportsAdapter(
    private val context: Context,
    private val resultReportsList: List<StudentResultReportResponse.StudentReport>
) : RecyclerView.Adapter<StudentResultReportsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.fragment_student_result_reports_item,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return resultReportsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(resultReportsList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: FragmentStudentResultReportsItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(resultReport: StudentResultReportResponse.StudentReport) {
            itemBinding.textViewId.text = resultReport.getCustomStudentId()
            itemBinding.textViewRoll.text = resultReport.getStudentRoll().toString()
            itemBinding.textViewName.text = resultReport.getStudentName()
            itemBinding.resultStdTotalMarks.text = resultReport.getTotalMarks()
            itemBinding.resultStdGrade.text = resultReport.getLetterGrade()
            itemBinding.resultStdGpa.text = resultReport.getGradingPoint()
            itemBinding.resultStdStatus.text = resultReport.getPassFailStatus()
            itemBinding.resultStdSectionMerit.text = resultReport.getSectionPosition()
            itemBinding.resultStdNoOfFailSubject.text = resultReport.getNumOfFailedSubjects()
        }
    }

    companion object {
        private val TAG = StudentResultReportsAdapter::class.java.simpleName
    }
}