package com.netizen.eduman.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.viewModel.SignInViewModel


class CycleAdapter(private val context: Context,
                   private val signInViewModel: SignInViewModel
) : RecyclerView.Adapter<CycleAdapter.ViewHolder>() {


    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.single_cycle_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return 8
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.textView.text = position.toString()
//        holder.textView.background = context.getDrawable(R.color.colorUnfocused)

//        Log.e(TAG, position.toString())

        if (position == 0 || position == 1 || position == 6 || position == 7) {
            holder.itemView.visibility = View.INVISIBLE
        } else {
            holder.textView.setOnClickListener {
//                Log.e(CycleAdapter::class.java.simpleName, position.toString())

                signInViewModel.fragmentPosition.postValue(position)
                holder.textView.background = context.getDrawable(R.drawable.cycle_background)
            }

//            setAnimation(holder.itemView, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
//        Log.e(TAG, position.toString())
        return position
    }

    private fun setAnimation(
        viewToAnimate: View,
        position: Int
    ) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation: Animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var textView: TextView = itemView.findViewById(R.id.text_view)
    }
}