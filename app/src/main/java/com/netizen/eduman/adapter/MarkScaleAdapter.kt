package com.netizen.eduman.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.eduman.R
import com.netizen.eduman.databinding.SingleMarkScaleLayoutBinding
import com.netizen.eduman.model.MarkScaleResponse

class MarkScaleAdapter(
    private val context: Context,
    private val markScaleList: List<MarkScaleResponse.MarkScale>
) : RecyclerView.Adapter<MarkScaleAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_mark_scale_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return markScaleList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(markScaleList[position])
    }

    class ViewHolder(private val itemBinding: SingleMarkScaleLayoutBinding?) :
        RecyclerView.ViewHolder(itemBinding!!.root) {

        fun bind(markScale: MarkScaleResponse.MarkScale) {
            itemBinding!!.textViewExamCode.text = markScale.getShortCodeName().toString()
            itemBinding.textViewTotalMark.text = markScale.getTotalMark().toString()
            itemBinding.textViewPassMark.text = markScale.getPassMark().toString()
            itemBinding.textViewAcceptance.text = markScale.getAcceptance().toString()
        }
    }
}