package com.netizen.eduman.adapter

import android.content.Context
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.netizen.eduman.R
import com.netizen.eduman.model.StudentReportResponse


class StudentReportAdapter(private val context: Context,
                           private val reportList: List<StudentReportResponse.StudentReport>,
                           private val listener: OnItemClick
) : RecyclerView.Adapter<StudentReportAdapter.ViewHolder>() {

    interface OnItemClick {
        fun goToDetailsFragment(studentReport: StudentReportResponse.StudentReport)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.single_report_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return reportList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.idTextView.text = reportList[position].getCustomStudentId().toString()
            holder.nameTextView.text = reportList[position].getStudentName().toString()
            holder.rollNoTextView.text = reportList[position].getStudentRoll().toString()

            if (reportList[position].getPureByteImage() != null || reportList[position].getImage() != "") {
                Glide.with(context)
                    .load(Base64.decode(reportList[position].getPureByteImage().toString(), Base64.DEFAULT))
                    .asBitmap()
                    .placeholder(R.drawable.ic_boy_icon)
                    .into(holder.studentImageView)
            }

            holder.nextImageView.setOnClickListener {
                listener.goToDetailsFragment(reportList[position])
            }
        } catch (e: Exception) {
            Log.e("EXCEPTION", e.toString())
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val nameTextView = itemView.findViewById<TextView>(R.id.text_view_name)!!
        val idTextView = itemView.findViewById<TextView>(R.id.text_view_id)!!
        val rollNoTextView = itemView.findViewById<TextView>(R.id.text_view_roll)!!
        val studentImageView = itemView.findViewById<ImageView>(R.id.image_view_student)!!
        val nextImageView = itemView.findViewById<ImageView>(R.id.image_view_next)!!
    }
}