package com.netizen.eduman.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.netizen.eduman.apiService.ApiClient
import com.netizen.eduman.apiService.ApiInterface
import com.netizen.eduman.model.HRDesignation
import com.netizen.eduman.model.HrReport
import com.netizen.eduman.model.ValidUserResponse
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders.Companion.isLoading1
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HRRepository(val application: Application) {

    private val appPreferences = AppPreferences(application.applicationContext)

    val isLoading = MutableLiveData<Boolean>()
    val isLoadingSave = MutableLiveData<Boolean>()
    val apiError = MutableLiveData<String>()
    val apiSuccess = MutableLiveData<String>()
    var hrDesignationList = MutableLiveData<List<HRDesignation.Designation>>()
    var hrReportsList = MutableLiveData<List<HrReport.HRreportData?>>()

    var messageType = MutableLiveData<Int>()

    fun getHRDesignationData() {

        isLoading1.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.getHRDesignation(appPreferences.getToken()!!, "2601")

        call.enqueue(object : Callback<HRDesignation> {

            override fun onFailure(call: Call<HRDesignation>, t: Throwable) {
                isLoading1.value = false
                apiError.value = "Could Not Get HR Designation List!"
                Log.e("ON_FAILURE", t.toString())
            }

            override fun onResponse(call: Call<HRDesignation>, response: Response<HRDesignation>) {
                try {

                    if (response.isSuccessful) {
                        hrDesignationList.value =
                            response.body()?.getItem() as List<HRDesignation.Designation>
                    } else {
                        apiError.value = response.body()?.getMessage().toString()
                    }

                    isLoading1.value = false

                } catch (e: Exception) {
                    isLoading1.value = false
                    apiError.value = "Something went wrong! Please try again."
                    Log.e("EXCEPTION", e.toString())
                }
            }
        })
    }


    fun hrEnlistPOstData(hrEnlist: Array<*>, hrIdType: String) {

        isLoadingSave.value = true
        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        val call = if (hrIdType == "1") {
            service?.postHREnlistDataHRcustomID(hrIdType, appPreferences.getToken()!!, hrEnlist)
        } else {
            service?.postHREnlistDataHRAutoID(appPreferences.getToken()!!, hrEnlist)
        }

        //calling the api
        call?.enqueue(object : Callback<ValidUserResponse> {
            override fun onResponse(
                call: Call<ValidUserResponse>,
                response: Response<ValidUserResponse>
            ) {
                Log.d("onResponse", "Success:" + response.body())

                try {
                    Log.e("RESPONSE", response.toString())

                    if (response.isSuccessful) {
                        apiSuccess.value = "HR Registration Successfully Completed"
                    } else {
                        apiError.value = "Couldn't Save HR Data! Please try again."
                    }

                    isLoadingSave.value = false
                } catch (e: Exception) {
                    apiError.value = "Something went wrong! Please try again."
                    isLoadingSave.value = false
                }
            }

            override fun onFailure(call: Call<ValidUserResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
                apiError.value = "Couldn't Save HR Data! Please try again."
                isLoadingSave.value = false
            }
        })
    }

    fun getHrReportData() {

        isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.getHRreports(appPreferences.getToken()!!)

        call.enqueue(object : Callback<HrReport> {

            override fun onFailure(call: Call<HrReport>, t: Throwable) {
                isLoading.value = false
                apiError.value = "Could Not Get HR Reports List!"
                Log.e("ON_FAILURE", t.toString())
            }

            override fun onResponse(call: Call<HrReport>, response: Response<HrReport>) {
                try {

                    if (response.isSuccessful) {
                        hrReportsList.value = response.body()?.getItem()
                    } else {
                        apiError.value = response.body()?.getMessage().toString()
                    }

                    isLoading.value = false

                } catch (e: Exception) {
                    isLoading.value = false
                    apiError.value = "Something went wrong! Please try again."
                    Log.e("EXCEPTION", e.toString())
                }
            }
        })
    }

}