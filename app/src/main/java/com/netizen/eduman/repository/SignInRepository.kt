package com.netizen.eduman.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.netizen.eduman.apiService.ApiClient
import com.netizen.eduman.apiService.ApiInterface
import com.netizen.eduman.model.LoginUser
import com.netizen.eduman.model.TokenResponse
import com.netizen.eduman.model.ValidUserResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignInRepository {

    val lottieProgressDialog = MutableLiveData<Boolean>()
    val isLoggedIn = MutableLiveData<Boolean>()
    var accessToken = MutableLiveData<String>()
    var successStatus = MutableLiveData<String>()
    var errorStatus = MutableLiveData<String>()
    var messageType = MutableLiveData<Int>()

    //01750316386
    fun loginCheck(loginUser: LoginUser) {

        lottieProgressDialog.value = true

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.userValidation(loginUser.getStrUser()!!, loginUser.getStrPassword()!!)

        //calling the api
        call?.enqueue(object : Callback<ValidUserResponse> {
            override fun onResponse(
                call: Call<ValidUserResponse>,
                response: Response<ValidUserResponse>
            ) {
                try {
                    Log.d("onResponse", "Message:" + response.body()?.message)

                    if (response.code() == 200) {
                        messageType.value = response.body()?.msgType

                        if (messageType.value == 1) {
                            Log.d("Success", " Send Data to Server")
                            getTokenFromServer(loginUser)
                        } else {
                            errorStatus.value = response.body()?.message
                            lottieProgressDialog.value = false
                            Log.d("Message From Server: ", errorStatus.value!!)
                        }
                    } else {
                        lottieProgressDialog.value = false
                        isLoggedIn.value = false
                        errorStatus.value = "Something went wrong! Please try again."
                    }
                } catch (e: Exception) {
                    isLoggedIn.value = false
                    errorStatus.value = "Something went wrong! Please try again."
                }
            }

            override fun onFailure(call: Call<ValidUserResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
                lottieProgressDialog.value = false
                isLoggedIn.value = false
                errorStatus.value = "Something went wrong! Please try again."
            }
        })
    }

    fun getTokenFromServer(loginUser: LoginUser) {

        val params = HashMap<String?, String?>()
        params["client_id"] = "eduman-web-read-write-client"
        params["grant_type"] = "password"
        params["username"] = loginUser.getStrUser()
        params["password"] = loginUser.getStrPassword()

        val clientKey = "Basic ZWR1bWFuLXdlYi1yZWFkLXdyaXRlLWNsaWVudDpzcHJpbmctc2VjdXJpdHktb2F1dGgyLXJlYWQtd3JpdGUtY2xpZW50LXBhc3N3b3JkMTIzNA=="

        val header = HashMap<String?, String?>()
        header["Content-Type"] = "application/x-www-form-urlencoded"
        header["Authorization"] = clientKey

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.postData(params, header)

        //calling the api
        call?.enqueue(object : Callback<TokenResponse> {
            override fun onResponse(call: Call<TokenResponse>, response: Response<TokenResponse>) {
                try {
                    Log.d("onResponse", "Access Token:" + response.body()?.getToken())

                    if (response.code() == 200) {
                        isLoggedIn.value = true
                        successStatus.value = "Log in Successful."
                        accessToken.value = response.body()?.getToken().toString()
                    } else {
                        isLoggedIn.value = false
                        errorStatus.value = "Invalid token!"
                    }

                    lottieProgressDialog.value = false
                } catch (e: Exception) {
                    isLoggedIn.value = false
                    errorStatus.value = "Something went wrong! Please try again."
                }
            }

            override fun onFailure(call: Call<TokenResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
                lottieProgressDialog.value = false
                isLoggedIn.value = false
                errorStatus.value = "Something went wrong! Please try again."
            }
        })
    }
}