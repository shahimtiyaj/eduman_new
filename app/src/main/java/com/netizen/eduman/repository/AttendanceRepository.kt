package com.netizen.eduman.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.netizen.eduman.apiService.ApiClient
import com.netizen.eduman.apiService.ApiInterface
import com.netizen.eduman.model.*
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders.Companion.apiError
import com.netizen.eduman.utils.Loaders.Companion.apiSuccess
import com.netizen.eduman.utils.Loaders.Companion.isLoading
import com.netizen.eduman.utils.Loaders.Companion.isLoading2
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AttendanceRepository(val application: Application) {

    private val appPreferences = AppPreferences(application.applicationContext)

    val periodList = MutableLiveData<List<PeriodResponse.Period>>()
    val takAdtPeriodList = MutableLiveData<List<TakeAtdPeriodResponse.TakAtdPeriod>>()
    val takAdtStudentList = MutableLiveData<List<*>>()
    val hrAttendanceList = MutableLiveData<List<*>>()

//    val apiError = MutableLiveData<String>()
//    val apiSuccess = MutableLiveData<String>()
    val isListFound = MutableLiveData<Boolean>()

    val studentAttendanceReportSummaryList =
        MutableLiveData<List<AttendanceReportSummaryResponse.AttendanceReportSummary>>()
    val studentAttendanceList =
        MutableLiveData<List<StudentAttendanceReportDetailsResponse.StudentAttendanceReportDetails>>()

    fun getPeriodListForSearchingStudentReport() {
        isLoading2.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getPeriodListForSearchingStudentReport(
            appPreferences.getToken().toString(),
            "2301"
        )

        val emptyList = ArrayList<PeriodResponse.Period>()

        call.enqueue(object : Callback<PeriodResponse> {
            override fun onFailure(call: Call<PeriodResponse>, t: Throwable) {
                isLoading2.value = false
                apiError.value = "Couldn't get period list!"
                periodList.postValue(emptyList)
            }

            override fun onResponse(
                call: Call<PeriodResponse>,
                response: Response<PeriodResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        periodList.value =
                            response.body()!!.getItem() as List<PeriodResponse.Period>
                    } else {
                        periodList.postValue(emptyList)
                        apiError.value = response.body()!!.getMessage().toString()
                    }

                    isLoading2.value = false
                } catch (e: Exception) {
                    isLoading2.value = false
                    apiError.value = "Something went wrong! Please try again."
                    periodList.postValue(emptyList)
                }
            }
        })
    }

    fun getStudentAttendanceReportSummaryList(date: String, periodId: String) {
        isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getStudentAttendanceReportSummaryList(
            appPreferences.getToken().toString(),
            date,
            periodId
        )

        call.enqueue(object : Callback<AttendanceReportSummaryResponse> {
            override fun onFailure(call: Call<AttendanceReportSummaryResponse>, t: Throwable) {
                isListFound.postValue(false)
                isLoading.value = false
                apiError.value = "Couldn't get attendance summary report list!"
            }

            override fun onResponse(
                call: Call<AttendanceReportSummaryResponse>,
                response: Response<AttendanceReportSummaryResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        if (response.body()!!.getItem()!!.isNotEmpty()) {
                            isListFound.postValue(true)
                            studentAttendanceReportSummaryList.value = response.body()?.getItem()
                                    as List<AttendanceReportSummaryResponse.AttendanceReportSummary>
                        } else {
                            isListFound.postValue(false)
                            apiError.postValue("No data found!")
                        }
                    } else {
                        isListFound.postValue(false)
                        apiError.value = response.body()!!.getMessage().toString()
                    }

                    isLoading.value = false
                } catch (e: Exception) {
                    isListFound.postValue(false)
                    isLoading.value = false
                    apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getStudentAttendanceReportList(
        date: String,
        periodId: String,
        configId: String,
        attendanceStatus: String
    ) {
        isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getStudentAttendanceReportList(
            appPreferences.getToken().toString(),
            date,
            periodId,
            configId,
            attendanceStatus
        )

        call.enqueue(object : Callback<StudentAttendanceReportDetailsResponse> {
            override fun onFailure(
                call: Call<StudentAttendanceReportDetailsResponse>,
                t: Throwable
            ) {
                isLoading.value = false
                apiError.value = "Couldn't get attendance summary report list!"
            }

            override fun onResponse(
                call: Call<StudentAttendanceReportDetailsResponse>,
                detailsResponse: Response<StudentAttendanceReportDetailsResponse>
            ) {
                try {
                    if (detailsResponse.isSuccessful) {
                        studentAttendanceList.value = detailsResponse.body()?.getItem()
                                as List<StudentAttendanceReportDetailsResponse.StudentAttendanceReportDetails>
//                        Log.e("SIZE", studentAttendanceReportSummaryList.value!![0].getClassName().toString())
                    } else {
                        apiError.value = detailsResponse.body()!!.getMessage().toString()
                    }

                    isLoading.value = false
                } catch (e: Exception) {
                    isLoading.value = false
                    apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    /*
      Period List when taking Student Attendance
     */
    fun getPeriodListForTakeAdtStudentList(date: String, sectionID: String) {

        isLoading2.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        /*val call = apiService.getTakeAdtPeriodList(
            appPreferences.getToken().toString(),
            "2020/01/23",
            "114102"
        )*/

        val call = apiService.getTakeAdtPeriodList(
            appPreferences.getToken().toString(),
            date,
            sectionID
        )

        val emptyList = ArrayList<TakeAtdPeriodResponse.TakAtdPeriod>()

        call.enqueue(object : Callback<TakeAtdPeriodResponse> {
            override fun onFailure(call: Call<TakeAtdPeriodResponse>, t: Throwable) {
                isLoading2.value = false
                apiError.value = "Couldn't get Take Attendance Period list!"
                takAdtPeriodList.postValue(emptyList)
            }

            override fun onResponse(
                call: Call<TakeAtdPeriodResponse>,
                response: Response<TakeAtdPeriodResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        takAdtPeriodList.postValue(response.body()!!.getItem() as List<TakeAtdPeriodResponse.TakAtdPeriod>)
                    } else {
                        takAdtPeriodList.postValue(emptyList)
                        apiError.value = response.body()!!.getMessage().toString()
                    }

                    isLoading2.value = false
                } catch (e: Exception) {
                    isLoading2.value = false
                    apiError.value = "Something went wrong! Please try again."
                    takAdtPeriodList.postValue(emptyList)
                }
            }
        })
    }

    /*
   Take Student Attendance List when taking Student Attendance
  */
    fun getAllTakeAttendanceStudentList(date: String, sectionID: String, periodId: String) {

        isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        /* val call = apiService.getAllTakeAttendanceStudent(
              appPreferences.getToken().toString(),
              "26-01-2020",
              "114102",
              "922102301"
          )*/

        val call = apiService.getAllTakeAttendanceStudent(
            appPreferences.getToken().toString(),
            date,
            sectionID,
            periodId
        )

        call.enqueue(object : Callback<TakeAtdStudentResponse> {
            override fun onFailure(call: Call<TakeAtdStudentResponse>, t: Throwable) {
                isListFound.postValue(false)
                isLoading.value = false
                apiError.value = "Couldn't get Take Attendance Student list!"
            }

            override fun onResponse(
                call: Call<TakeAtdStudentResponse>,
                response: Response<TakeAtdStudentResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        if (response.body()!!.getItem()!!.isNotEmpty()) {
                            isListFound.postValue(true)
                            takAdtStudentList.postValue( response.body()!!.getItem() as List<*>)
                        } else {
                            isListFound.postValue(false)
                            apiError.postValue("No data found!")
                        }
                    } else {
                        isListFound.postValue(false)
                        apiError.value = response.body()!!.getMessage().toString()
                    }
                    isLoading.value = false
                } catch (e: Exception) {
                    isListFound.postValue(false)
                    isLoading.value = false
                    apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    /*
   Take Student Attendance Save data after taking Student Attendance
   */
    fun postTakeAttendanceData(takeAttendanceList: TakeAtdStudentPost) {

        isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.postTakeAttendanceData(
            appPreferences.getToken().toString(),
            takeAttendanceList
        )

        call.enqueue(object : Callback<TakeAtdStudentPost> {
            override fun onFailure(call: Call<TakeAtdStudentPost>, t: Throwable) {
                isLoading.value = false
                apiError.value = "Couldn't get Take Attendance Student list Save!"
            }

            override fun onResponse(
                call: Call<TakeAtdStudentPost>,
                response: Response<TakeAtdStudentPost>
            ) {
                try {
                    if (response.isSuccessful) {
                        apiSuccess.value = "Student Attendance Successfully Saved !"
                    } else {
                        apiError.value = "Couldn't Save Attendance Data! Please try again."
                    }
                    isLoading.value = false
                } catch (e: Exception) {
                    isLoading.value = false
                    apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }


    /*
        HR Attendance List such as Present , Absent , Leave
        Identify using status , 1, 2, 3

     */

    fun getHRAttendanceReports(date: String, status: String) {

        isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.getHRAttendanceReports(date, status, appPreferences.getToken().toString())

        call.enqueue(object : Callback<HRAttendanceReportsResponse> {
            override fun onFailure(call: Call<HRAttendanceReportsResponse>, t: Throwable) {
                isLoading.value = false
                apiError.value = "Couldn't get HR Attendance list!"
            }

            override fun onResponse(
                call: Call<HRAttendanceReportsResponse>,
                response: Response<HRAttendanceReportsResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        hrAttendanceList.postValue(response.body()!!.getItem() as List<*>)
                    } else {
                        apiError.value = response.body()!!.getMessage().toString()
                    }
                    isLoading.value = false
                } catch (e: Exception) {
                    isLoading.value = false
                    apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }
}