package com.netizen.eduman.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.netizen.eduman.apiService.ApiClient
import com.netizen.eduman.apiService.ApiInterface
import com.netizen.eduman.database.LocalDatabase
import com.netizen.eduman.model.*
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders
import com.netizen.eduman.utils.Loaders.Companion.apiError
import com.netizen.eduman.utils.Loaders.Companion.apiSuccess
import com.netizen.eduman.utils.Loaders.Companion.isLoading
import com.netizen.eduman.utils.Loaders.Companion.isLoading2
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StudentRepository(val application: Application) {

    private val appPreferences = AppPreferences(application.applicationContext)
    private val edumanDAO = LocalDatabase.getInstance(application).edumanDAO()

    var academicYearList = MutableLiveData<List<AcademicYearResponse.AcademicYear>>()
    var sectionList = MutableLiveData<List<SectionResponse.Section>>()
    var groupList = MutableLiveData<List<GroupResponse.Group>>()
    var categoryList = MutableLiveData<List<CategoryResponse.Category>>()
    var studentReportList = MutableLiveData<List<StudentReportResponse.StudentReport>>()

    var isStudentReportListFound = MutableLiveData<Boolean>()

    fun getAcademicYearData() {
        isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.getAcademicYearList(appPreferences.getToken()!!, "2101")
        call.enqueue(object: Callback<AcademicYearResponse> {
            override fun onFailure(call: Call<AcademicYearResponse>, t: Throwable) {
                isLoading.value = false
                apiError.value = "Could not get academic year list!"
                Log.e("ON_FAILURE", t.toString())
            }

            override fun onResponse(
                call: Call<AcademicYearResponse>,
                response: Response<AcademicYearResponse>
            ) {
                try {
//                    Log.e("RESPONSE", response.body()?.getItem()?..toString())

                    if (response.isSuccessful) {
                        academicYearList.value = response.body()?.getItem() as List<AcademicYearResponse.AcademicYear>?
                    } else {
                        apiError.value = response.body()?.getMessage().toString()
                    }

                    isLoading.value = false
                } catch (e: Exception) {
                    isLoading.value = false
                    apiError.value = "Something went wrong! Please try again."
                    Log.e("EXCEPTION", e.toString())
                }
            }
        })
    }

    fun getSectionData() {
        Loaders.isLoading1.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.getSectionList(appPreferences.getToken()!!)
        call.enqueue(object: Callback<SectionResponse> {
            override fun onFailure(call: Call<SectionResponse>, t: Throwable) {
                Loaders.isLoading1.value = false
                apiError.value = "Could not get section list!"
                Log.e("ON_FAILURE", t.toString())
            }

            override fun onResponse(
                call: Call<SectionResponse>,
                response: Response<SectionResponse>
            ) {
                try {

                    if (response.isSuccessful) {
                        sectionList.value = response.body()?.getItem() as List<SectionResponse.Section>
                    } else {
                        apiError.value = response.body()?.getMessage().toString()
                    }

                    Loaders.isLoading1.value = false
                } catch (e: Exception) {
                    Loaders.isLoading1.value = false
                    apiError.value = "Something went wrong! Please try again."
                    Log.e("EXCEPTION", e.toString())
                }
            }
        })
    }

    fun getGroupData(configId: String) {
        isLoading2.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.getGroupList(appPreferences.getToken()!!, configId)
        call.enqueue(object: Callback<GroupResponse> {
            override fun onFailure(call: Call<GroupResponse>, t: Throwable) {
                isLoading2.value = false
                apiError.value = "Could not get group list!"
//                Log.e("ON_FAILURE", t.toString())
            }

            override fun onResponse(
                call: Call<GroupResponse>,
                response: Response<GroupResponse>
            ) {
                try {
//                    Log.e("RESPONSE", response.body()?.getItem().toString())

                    if (response.isSuccessful) {
//                        groupList.postValue(response.body()?.getItem() as List<GroupResponse.GroupObject>)
                        groupList.value = response.body()?.getItem() as List<GroupResponse.Group>?
//                        Log.e("GROUP_ITEM", response.body()?.getItem()?.get(0)?.getGroupObject()?.getName().toString())
//                        Log.e("GROUP_ITEM", groupList.value?.get(0)?.getGroupObject()?.getName().toString())
                    } else {
                        apiError.value = response.body()?.getMessage().toString()
                    }

                    isLoading2.value = false
                } catch (e: Exception) {
                    isLoading2.value = false
                    apiError.value = "Something went wrong! Please try again."
//                    Log.e("EXCEPTION", e.toString())
                }
            }
        })
    }

    fun getCategoryData() {
        isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.getCategoryList(appPreferences.getToken()!!, "2106")
        call.enqueue(object: Callback<CategoryResponse> {
            override fun onFailure(call: Call<CategoryResponse>, t: Throwable) {
                isLoading.value = false
                apiError.value = "Could not get category list!"
//                Log.e("ON_FAILURE", t.toString())
            }

            override fun onResponse(
                call: Call<CategoryResponse>,
                response: Response<CategoryResponse>
            ) {
                try {
//                    Log.e("RESPONSE", response.body()?.getItem().toString())

                    if (response.isSuccessful) {
                        categoryList.value = response.body()?.getItem() as List<CategoryResponse.Category>?
                    } else {
                        apiError.value = response.body()?.getMessage().toString()
                    }

                    isLoading.value = false
                } catch (e: Exception) {
                    isLoading.value = false
                    apiError.value = "Something went wrong! Please try again."
//                    Log.e("EXCEPTION", e.toString())
                }
            }
        })
    }

    fun saveEnrollmentData(idType: String, jsonObject: JsonObject) {
        isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.saveStudentEnrollmentData(appPreferences.getToken()!!,
            idType,
            jsonObject)

        call.enqueue(object: Callback<JsonObject>{
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                apiError.value = "Couldn't save data! Please try again."
                isLoading.value = false
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                try {
                    when {
                        response.isSuccessful -> {
                            apiSuccess.value = "Student Registration Successfully Completed."
                        }
                        response.code() == 409 -> {
                            apiError.value = "Student ID Already Exists!"
                        }
                        else -> {
                            apiError.value = "Couldn't save data! Please try again."
                        }
                    }

                    isLoading.value = false
                } catch (e: Exception) {
                    apiError.value = "Something went wrong! Please try again."
                    isLoading.value = false
                }
            }
        })
    }

    fun getStudentReportList(sectionId: String) {
        isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getStudentReportList(
            appPreferences.getToken()!!,
            sectionId)

        call.enqueue(object : Callback<StudentReportResponse> {
            override fun onFailure(call: Call<StudentReportResponse>, t: Throwable) {
                isLoading.value = false
                isStudentReportListFound.postValue(false)
                apiError.value = "Couldn't get student reports!"
            }

            override fun onResponse(
                call: Call<StudentReportResponse>,
                response: Response<StudentReportResponse>
            ) {
                try {
                    if (response.isSuccessful && response.body()?.getItem()!!.isNotEmpty()) {
                        isStudentReportListFound.postValue(true)
                        studentReportList.postValue(response.body()?.getItem() as List<StudentReportResponse.StudentReport>)
                    } else {
                        isStudentReportListFound.postValue(false)
                        apiError.value = "Sorry! No Data Found!!"
                    }

                    isLoading.value = false
                } catch (e: Exception) {
                    isLoading.value = false
                    isStudentReportListFound.postValue(false)
                    apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }
}