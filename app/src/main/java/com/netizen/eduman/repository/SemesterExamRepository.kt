package com.netizen.eduman.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.netizen.eduman.apiService.ApiClient
import com.netizen.eduman.apiService.ApiInterface
import com.netizen.eduman.model.*
import com.netizen.eduman.utils.AppPreferences
import com.netizen.eduman.utils.Loaders
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SemesterExamRepository(val application: Application) {

    private val appPreferences = AppPreferences(application)

    var isListFound = MutableLiveData<Boolean>()

    val sectionList = MutableLiveData<List<SectionResponse.Section>>()
    val examList = MutableLiveData<List<ExamResponse.Exam>>()
    val subjectList = MutableLiveData<List<SubjectResponse.Subject>>()
    val markInputList = MutableLiveData<List<MarkInputResponse.MarkInput>>()
    val shortCodeList = MutableLiveData<List<MarkScaleResponse.MarkScale>>()
    val unassignedMarksList = MutableLiveData<List<UnAssignedResultResponse.UnAssignedResult>>()
    val remarksUpdateList = MutableLiveData<List<RemarksUpdateResponse.RemarksUpdate>>()

    val grandFinalClassList = MutableLiveData<List<GrandFinalClassResponse.ClassSection>>()
    val grandFinalExamList = MutableLiveData<List<GrandFinalExamResponse.GrandExam>>()
    val meritPositionExamList = MutableLiveData<List<MeritPositionExamResponse.MeritPosition>>()
    val studentResultReportList = MutableLiveData<List<StudentResultReportResponse.StudentReport>>()
    val remarkAssignTitleList = MutableLiveData<List<RemarksTitleResponse.RemarksTitle>>()
    val remarkAssignDes = MutableLiveData<String>()
    val remarkAssignStudentList = MutableLiveData<List<RemarkAssignStudentListResponse.StudentRemarkAssign>>()

    var isRemarkListFound = MutableLiveData<Boolean>()
    var isUnAssignedResultFound = MutableLiveData<Boolean>()

    fun getRoleWiseSectionList() {
        Loaders.isLoading1.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getRoleWiseSectionList(appPreferences.getToken().toString())

        call.enqueue(object : Callback<SectionResponse> {
            override fun onFailure(call: Call<SectionResponse>, t: Throwable) {
                Loaders.isLoading1.value = false
                Loaders.apiError.value = "Couldn't get the section list!"
            }

            override fun onResponse(
                call: Call<SectionResponse>,
                response: Response<SectionResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        sectionList.value = response.body()!!.getItem() as List<SectionResponse.Section>
                    } else {
                        Loaders.apiError.value = response.body()!!.getMessage().toString()
                    }

                    Loaders.isLoading1.value = false
                } catch (e: Exception) {
                    Loaders.isLoading1.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getExamList(classConfigId: String?) {

        Loaders.isLoading3.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getExamList(appPreferences.getToken().toString(), classConfigId)

        val  emptyList=ArrayList<ExamResponse.Exam>()

        call.enqueue(object : Callback<ExamResponse> {
            override fun onFailure(call: Call<ExamResponse>, t: Throwable) {
                Loaders.isLoading3.value = false
                Loaders.apiError.value = "Couldn't get the exam list!"
                examList.postValue(emptyList)
            }

            override fun onResponse(
                call: Call<ExamResponse>,
                response: Response<ExamResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        examList.postValue(response.body()!!.getItem() as List<ExamResponse.Exam>)
                    } else {
                        examList.postValue(emptyList)
                        Loaders.apiError.value = response.body()!!.getMessage().toString()
                    }

                    Loaders.isLoading3.value = false
                } catch (e: Exception) {
                    Loaders.isLoading3.value = false
                    Loaders.apiError.postValue("Something went wrong! Please try again.")
                    examList.postValue(emptyList)
                }
            }
        })
    }


    fun getSubjectList(
        classConfigId: String,
        groupId: String
    ) {
        Loaders.isLoading4.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getSubjectList(
            appPreferences.getToken().toString(),
            classConfigId,
            groupId
        )

        val  emptyList=ArrayList<SubjectResponse.Subject>()

        call.enqueue(object : Callback<SubjectResponse> {
            override fun onFailure(call: Call<SubjectResponse>, t: Throwable) {
                Loaders.isLoading4.value = false
                Loaders.apiError.value = "Couldn't get the subject list!"
                subjectList.postValue(emptyList)
            }

            override fun onResponse(
                call: Call<SubjectResponse>,
                response: Response<SubjectResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        subjectList.postValue(response.body()!!.getItem() as List<SubjectResponse.Subject>)
                    } else {
                        subjectList.postValue(emptyList)
                        Loaders.apiError.value = response.body()!!.getMessage().toString()
                    }

                    Loaders.isLoading4.value = false
                } catch (e: Exception) {
                    Loaders.isLoading4.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    subjectList.postValue(emptyList)
                }
            }
        })
    }

    fun getMarkInputList(
        classConfigId: String,
        groupId: String,
        subjectId: String,
        examConfigId: String
    ) {
        Loaders.isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getMarkInputList(
            appPreferences.getToken().toString(),
            classConfigId,
            groupId,
            subjectId,
            examConfigId)

        call.enqueue(object : Callback<MarkInputResponse>{
            override fun onFailure(call: Call<MarkInputResponse>, t: Throwable) {
                Loaders.isLoading4.value = false
                Loaders.apiError.value = "Couldn't get the mark input list!"
                isListFound.value = false
            }

            override fun onResponse(
                call: Call<MarkInputResponse>,
                response: Response<MarkInputResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        if (!response.body()!!.getItem().isNullOrEmpty()) {
                            markInputList.postValue(response.body()!!.getItem() as List<MarkInputResponse.MarkInput>)
                            getMarkScaleList(classConfigId, groupId, subjectId, examConfigId)
                            isListFound.value = true
                        } else {
                            isListFound.value = false
                            Loaders.isLoading.value = false
                            Loaders.apiError.value = response.body()!!.getMessage().toString()
                        }
                    } else {
                        isListFound.value = false
                        Loaders.isLoading.value = false
                        Loaders.apiError.postValue(response.body()!!.getMessage().toString())
                    }
                } catch (e: Exception) {
                    isListFound.value = false
                    Loaders.isLoading.value = false
                    Loaders.apiError.postValue("Something went wrong! Please try again.")
                }
            }

        })
    }

    fun getMarkUpdateList(
        classConfigId: String,
        groupId: String,
        subjectId: String,
        examConfigId: String
    ) {
        Loaders.isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getMarkUpdateList(
            appPreferences.getToken().toString(),
            classConfigId,
            groupId,
            subjectId,
            examConfigId)

        call.enqueue(object : Callback<MarkInputResponse>{
            override fun onFailure(call: Call<MarkInputResponse>, t: Throwable) {
                isListFound.value = false
                Loaders.isLoading4.value = false
                Loaders.apiError.value = "Couldn't get the mark update list!"
            }

            override fun onResponse(
                call: Call<MarkInputResponse>,
                response: Response<MarkInputResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        if (!response.body()!!.getItem().isNullOrEmpty()) {
                            markInputList.postValue(response.body()!!.getItem() as List<MarkInputResponse.MarkInput>)
                            getMarkScaleList(classConfigId, groupId, subjectId, examConfigId)
                            isListFound.value = true
                        } else {
                            isListFound.value = false
                            Loaders.isLoading.value = false
                            Loaders.apiError.postValue(response.body()!!.getMessage().toString())
                        }
                    } else {
                        isListFound.value = false
                        Loaders.isLoading.value = false
                        Loaders.apiError.postValue(response.body()!!.getMessage().toString())
                    }
                } catch (e: Exception) {
                    isListFound.value = false
                    Loaders.isLoading.value = false
                    Loaders.apiError.postValue("Something went wrong! Please try again.")
                }
            }
        })
    }

    private fun getMarkScaleList(
        classConfigId: String,
        groupId: String,
        subjectId: String,
        examConfigId: String
    ) {
        Loaders.isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getMarkScaleList(
            appPreferences.getToken().toString(),
            classConfigId,
            groupId,
            subjectId,
            examConfigId)

        call.enqueue(object : Callback<MarkScaleResponse>{
            override fun onFailure(call: Call<MarkScaleResponse>, t: Throwable) {
                Loaders.isLoading.value = false
                Loaders.apiError.value = "Couldn't get the mark scale list!"
            }

            override fun onResponse(
                call: Call<MarkScaleResponse>,
                response: Response<MarkScaleResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        shortCodeList.postValue(response.body()!!.getItem() as List<MarkScaleResponse.MarkScale>)
                    } else {
                        Loaders.apiError.postValue(response.body()!!.getMessage().toString())
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.isLoading.value = false
                    Loaders.apiError.postValue("Something went wrong! Please try again.")
                }
            }
        })
    }

    fun saveInputMark(examMarkInput: ExamMarkInput) {
        Loaders.isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.saveExamMarkInput(appPreferences.getToken().toString(), examMarkInput)

        call.enqueue(object : Callback<JsonObject>{
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Loaders.isLoading.value = false
                Loaders.apiError.value = "Couldn't save input data!"
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.postValue("Inputted Mark Successfully Saved.")
                    } else {
                        Loaders.apiError.postValue(response.body()!!.get("message").toString())
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.isLoading.value = false
                    Loaders.apiError.postValue("Something went wrong! Please try again.")
                }
            }
        })
    }

    fun updateInputMark(examMarkInput: ExamMarkInput) {
        Loaders.isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.updateExamMarkInput(appPreferences.getToken().toString(), examMarkInput)

        call.enqueue(object : Callback<JsonObject>{
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Loaders.isLoading.value = false
                Loaders.apiError.value = "Couldn't update input data!"
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.postValue("Marks Successfully Updated.")
                    } else {
                        Loaders.apiError.postValue(response.errorBody().toString())
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.isLoading.value = false
                    Loaders.apiError.postValue("Something went wrong! Please try again.")
                }
            }
        })
    }

    fun getUnAssignedResultSummary(examId: String) {
        Loaders.isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getUnAssignedResultSummary(appPreferences.getToken().toString(), examId)

        call.enqueue(object : Callback<UnAssignedResultResponse>{
            override fun onFailure(call: Call<UnAssignedResultResponse>, t: Throwable) {
                Loaders.isLoading.value = false
                isUnAssignedResultFound.postValue(false)
                Loaders.apiError.postValue("Couldn't get unassigned data!")
            }

            override fun onResponse(
                call: Call<UnAssignedResultResponse>,
                response: Response<UnAssignedResultResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        if (response.body()!!.getItem()!!.isNotEmpty()) {
                            unassignedMarksList.postValue(response.body()!!.getItem() as List<UnAssignedResultResponse.UnAssignedResult>)
                            isUnAssignedResultFound.postValue(true)
                        } else {
                            isUnAssignedResultFound.postValue(false)
                            Loaders.apiError.value = "No marks found!"
                        }
                    } else {
                        isUnAssignedResultFound.postValue(false)
                        Loaders.apiError.postValue(response.body()!!.getMessage().toString())
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    isUnAssignedResultFound.postValue(false)
                    Loaders.isLoading.value = false
                    Loaders.apiError.postValue("Something went wrong! Please try again.")
                }
            }
        })
    }

    fun getRemarksUpdateList(
        classConfigId: String,
        examId: String
    ) {
        Loaders.isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getRemarkUpdateList(
            appPreferences.getToken().toString(),
            classConfigId,
            examId
        )

        call.enqueue(object : Callback<RemarksUpdateResponse>{
            override fun onFailure(call: Call<RemarksUpdateResponse>, t: Throwable) {
                Loaders.isLoading.value = false
                Loaders.apiError.postValue("Couldn't get remarks update list!")
            }

            override fun onResponse(
                call: Call<RemarksUpdateResponse>,
                response: Response<RemarksUpdateResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        if (!response.body()!!.getItem().isNullOrEmpty()) {
                            isListFound.postValue(true)
                            remarksUpdateList.postValue(response.body()!!.getItem() as List<RemarksUpdateResponse.RemarksUpdate>)
                        } else {
                            Loaders.apiError.postValue("No data found!")
                            isListFound.postValue(false)
                        }
                    } else {
                        isListFound.postValue(false)
                        Loaders.apiError.postValue(response.body()!!.getMessage().toString())
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.isLoading.value = false
                    Loaders.apiError.postValue("Something went wrong! Please try again.")
                }
            }
        })
    }

    fun updateRemarksList(jsonArray: JsonArray) {
        Loaders.isLoading.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.updateRemarksList(appPreferences.getToken().toString(), jsonArray)

        call.enqueue(object : Callback<JsonObject>{
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Loaders.isLoading.value = false
                Loaders.apiError.postValue("Couldn't update remarks list!")
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.postValue("Result Remarks Successfully updated.")
                    } else {
                        Loaders.apiError.postValue(response.body()!!.get("message").asString)
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.isLoading.value = false
                    Loaders.apiError.postValue("Something went wrong! Please try again.")
                }
            }
        })
    }

    //---------------------------------------------------------------------------------

    fun postGeneralExamResultProcessData(classConfigId: String, examConfigId: String) {
        Loaders.isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.generalExamResultProcess(appPreferences.getToken()!!,
            classConfigId,
            examConfigId,
            "insert")

        call.enqueue(object: Callback<ValidUserResponse>{
            override fun onFailure(call: Call<ValidUserResponse>, t: Throwable) {
                Loaders.apiError.value = "Couldn't process data! Please try again."
                Loaders.isLoading.value = false
            }

            override fun onResponse(call: Call<ValidUserResponse>, response: Response<ValidUserResponse>) {
                try {

                    when {
                        response.isSuccessful -> {
                            if (response.body()?.msgType==1){
                                Loaders.apiSuccess.value = "Section Wise Result Successfully Processed"
                            }
                            else
                            {
                                Loaders.apiError.value = response.body()?.message

                            }
                        }
                        response.code() == 500 -> {
                            Loaders.apiError.value = "Server could not found!"
                        }
                        else -> {
                            Loaders.apiError.value = "Couldn't process data! Please try again."
                        }
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    Loaders.isLoading.value = false
                }
            }
        })
    }


    /**
     * typeId = "2102" for getting grand final class data
     *
     * typeId = "2201" for getting all exam data in Unassigned Marks Report
     */
    fun getGrandFinalClassData(typeId: String) {
        Loaders.isLoading1.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.getGrandFinalClassData(appPreferences.getToken()!!, typeId)
        call.enqueue(object: Callback<GrandFinalClassResponse> {
            override fun onFailure(call: Call<GrandFinalClassResponse>, t: Throwable) {
                Loaders.isLoading1.value = false
                Loaders.apiError.value = "Could not get section list!"
                Log.e("ON_FAILURE", t.toString())
            }

            override fun onResponse(
                call: Call<GrandFinalClassResponse>,
                response: Response<GrandFinalClassResponse>
            ) {
                try {

                    if (response.isSuccessful) {
                        grandFinalClassList.value = response.body()?.getItem() as List<GrandFinalClassResponse.ClassSection>
                    } else {
                        Loaders.apiError.value = response.body()?.getMessage().toString()
                    }

                    Loaders.isLoading1.value = false
                } catch (e: Exception) {
                    Loaders.isLoading1.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    Log.e("EXCEPTION", e.toString())
                }
            }
        })
    }

    fun getGrandFinalExamList(classId: String) {

        Loaders.isLoading3.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getGrandFinalExamList(classId, appPreferences.getToken().toString())
        val emptyList = ArrayList<GrandFinalExamResponse.GrandExam>()
        call.enqueue(object : Callback<GrandFinalExamResponse> {
            override fun onFailure(call: Call<GrandFinalExamResponse>, t: Throwable) {
                Loaders.isLoading3.value = false
                Loaders.apiError.value = "Couldn't get the grand exam list!"
                grandFinalExamList.postValue(emptyList)
            }

            override fun onResponse(
                call: Call<GrandFinalExamResponse>,
                response: Response<GrandFinalExamResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        grandFinalExamList.postValue(response.body()!!.getItem() as List<GrandFinalExamResponse.GrandExam>)
                    } else {
                        grandFinalExamList.postValue(emptyList)
                        Loaders.apiError.value = response.body()!!.getMessage().toString()
                    }

                    Loaders.isLoading3.value = false
                } catch (e: Exception) {
                    Loaders.isLoading3.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    grandFinalExamList.postValue(emptyList)
                }
            }
        })
    }


    fun postGrandFinalExamResultProcessData(examConfigId: String) {
        Loaders.isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.grandFinalExamResultProcess(appPreferences.getToken()!!, examConfigId)

        call.enqueue(object: Callback<ValidUserResponse>{
            override fun onFailure(call: Call<ValidUserResponse>, t: Throwable) {
                Loaders.apiError.value = "Couldn't process data! Please try again."
                Loaders.isLoading.value = false
            }

            override fun onResponse(call: Call<ValidUserResponse>, response: Response<ValidUserResponse>) {
                try {

                    when {
                        response.isSuccessful -> {
                            if (response.body()?.msgType==1){
                                Loaders.apiSuccess.value = "Grand Final Result Successfully Processed"
                            }
                            else
                            {
                                Loaders.apiError.value = response.body()?.message
                            }
                        }
                        response.code() == 500 -> {
                            Loaders.apiError.value = "Server could not found!"
                        }
                        else -> {
                            Loaders.apiError.value = "Couldn't process data! Please try again."
                        }
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    Loaders.isLoading.value = false
                }
            }
        })
    }

    fun getMeritPositionExamList(classId: String) {

        Loaders.isLoading3.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getMeritPositionExamList(appPreferences.getToken().toString(), classId)

        val emptyList = ArrayList<MeritPositionExamResponse.MeritPosition>()

        call.enqueue(object : Callback<MeritPositionExamResponse> {
            override fun onFailure(call: Call<MeritPositionExamResponse>, t: Throwable) {
                Loaders.isLoading3.value = false
                Loaders.apiError.value = "Couldn't get the Merit Position exam list!"
                meritPositionExamList.postValue(emptyList)
            }

            override fun onResponse(
                call: Call<MeritPositionExamResponse>,
                response: Response<MeritPositionExamResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        meritPositionExamList.postValue(response.body()!!.getItem() as List<MeritPositionExamResponse.MeritPosition>)
                    } else {
                        meritPositionExamList.postValue(emptyList)
                        Loaders.apiError.value = response.body()!!.getMessage().toString()
                    }

                    Loaders.isLoading3.value = false
                } catch (e: Exception) {
                    Loaders.isLoading3.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    meritPositionExamList.postValue(emptyList)
                }
            }
        })
    }

    fun postMeritPositionProcessData(classConfigId: String, examConfigId: String) {
        Loaders.isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.meritPositionProcess(appPreferences.getToken()!!, classConfigId, examConfigId, "insert")

        call.enqueue(object: Callback<ValidUserResponse>{
            override fun onFailure(call: Call<ValidUserResponse>, t: Throwable) {
                Loaders.apiError.value = "Couldn't process data! Please try again."
                Loaders.isLoading.value = false
            }

            override fun onResponse(call: Call<ValidUserResponse>, response: Response<ValidUserResponse>) {
                try {

                    when {
                        response.isSuccessful -> {
                            if (response.body()?.msgType==1){
                                Loaders.apiSuccess.value = "Merit Position Successfully Generated"
                            }
                            else
                            {
                                Loaders.apiError.value = response.body()?.message
                            }
                        }
                        response.code() == 500 -> {
                            Loaders.apiError.value = "Server could not found!"
                        }
                        else -> {
                            Loaders.apiError.value = "Couldn't process data! Please try again."
                        }
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    Loaders.isLoading.value = false
                }
            }
        })
    }

    fun postAttendanceCountProcessData(classConfigId: String, periodId: String,  examConfigId: String) {

        Loaders.isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.attendanceCountProcess(appPreferences.getToken()!!, classConfigId, periodId, examConfigId, "insert")

        call.enqueue(object: Callback<ValidUserResponse>{
            override fun onFailure(call: Call<ValidUserResponse>, t: Throwable) {
                Loaders.apiError.value = "Couldn't process data! Please try again."
                Loaders.isLoading.value = false
            }

            override fun onResponse(call: Call<ValidUserResponse>, response: Response<ValidUserResponse>) {
                try {
                    when {
                        response.isSuccessful -> {
                            if (response.body()?.msgType==1){
                                Loaders.apiSuccess.value = "Merit Position Successfully Generated"
                            }
                            else
                            {
                                Loaders.apiError.value = response.body()?.message
                            }
                        }
                        response.code() == 500 -> {
                            Loaders.apiError.value = "Server could not found!"
                        }
                        else -> {
                            Loaders.apiError.value = "Couldn't process data! Please try again."
                        }
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    Loaders.isLoading.value = false
                }
            }
        })
    }

    fun getStudentResultReportList(sectionId: String, examId: String) {

        Loaders.isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getStudentResultReportList(
            appPreferences.getToken()!!,
            sectionId, examId)

        call.enqueue(object : Callback<StudentResultReportResponse> {
            override fun onFailure(call: Call<StudentResultReportResponse>, t: Throwable) {
                Loaders.isLoading.value = false
                isListFound.value = false
                Loaders.apiError.value = "Couldn't get student result reports!"
            }

            override fun onResponse(
                call: Call<StudentResultReportResponse>,
                response: Response<StudentResultReportResponse>
            ) {

                try {
                    if (response.isSuccessful) {
                        if (!response.body()!!.getItem().isNullOrEmpty()) {
                            studentResultReportList.value =
                                response.body()?.getItem() as List<StudentResultReportResponse.StudentReport>
                            isListFound.value = true
                        } else {
                            isListFound.value = false
                            Loaders.apiError.value = "Sorry ! No Data found."
                        }

                        Loaders.isLoading.value = false
                    } else {
                        isListFound.value = false
                        Loaders.isLoading.value = false
                        Loaders.apiError.value = "Something went wrong! Please try again."
                    }
                } catch (e: Exception) {
                    isListFound.value = false
                    Loaders.isLoading.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }


                /*try {
                    if (response.isSuccessful) {
                        studentResultReportList.value = response.body()?.getItem() as List<StudentResultReportResponse.StudentReport>
                    } else {
                        Loaders.apiError.value = response.message()
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.isLoading.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }*/
            }
        })
    }

    fun getRemarksAssignTitleList() {

        Loaders.isLoadingRemarkTitle.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getRemarksAssignTitleList(appPreferences.getToken()!!)
        val  emptyList=ArrayList<RemarksTitleResponse.RemarksTitle>()

        call.enqueue(object : Callback<RemarksTitleResponse> {
            override fun onFailure(call: Call<RemarksTitleResponse>, t: Throwable) {
                Loaders.isLoadingRemarkTitle.value = false
                Loaders.apiError.value = "Couldn't get Remark Assign Title!"
                remarkAssignTitleList.postValue(emptyList)
            }

            override fun onResponse(
                call: Call<RemarksTitleResponse>,
                response: Response<RemarksTitleResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        remarkAssignTitleList.postValue(response.body()?.getItem() as List<RemarksTitleResponse.RemarksTitle>)
                    } else {
                        remarkAssignTitleList.postValue(emptyList)
                        Loaders.apiError.value = response.message()
                    }

                    Loaders.isLoadingRemarkTitle.value = false
                } catch (e: Exception) {
                    Loaders.isLoadingRemarkTitle.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    remarkAssignTitleList.postValue(emptyList)
                }
            }
        })
    }

    fun getRemarksAssignDescription(remarkTempId: String) {

       // Loaders.isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getRemarksAssignDescription(appPreferences.getToken()!!, remarkTempId)

        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
               // Loaders.isLoading.value = false
                Loaders.apiError.value = "Couldn't get Remark Description !"
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                try {
                    if (response.isSuccessful) {
                        remarkAssignDes.value = response.body()?.get("item")?.asString
                    } else {
                        Loaders.apiError.value = response.message()
                    }

                   // Loaders.isLoading.value = false
                } catch (e: Exception) {
                   // Loaders.isLoading.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }



    fun getRemarksAssignStudentList(classConfigId: String, examConfigId: String) {

        Loaders.isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getRemarkAssignStudentList(appPreferences.getToken()!!, classConfigId, examConfigId)

        call.enqueue(object : Callback<RemarkAssignStudentListResponse> {
            override fun onFailure(call: Call<RemarkAssignStudentListResponse>, t: Throwable) {
                Loaders.isLoading.value = false
                isRemarkListFound.value = false
                Loaders.apiError.value = "Couldn't get Remark Assign Student List!"
            }

            override fun onResponse(
                call: Call<RemarkAssignStudentListResponse>,
                response: Response<RemarkAssignStudentListResponse>
            ) {
                try {

                    if (response.isSuccessful) {
                        if (!response.body()?.getItem().isNullOrEmpty()) {
                            remarkAssignStudentList.postValue(response.body()?.getItem() as List<RemarkAssignStudentListResponse.StudentRemarkAssign>)
                            isRemarkListFound.postValue(true)
                        } else {
                            isRemarkListFound.value = false
                            Loaders.apiError.postValue("Sorry !! No Data Found.")
                        }
                    } else {
                        isRemarkListFound.value = false
                        Loaders.apiError.postValue("Sorry !! No Data Found.")
                    }

                 /*   if (response.isSuccessful) {
                        remarkAssignStudentList.value = response.body()?.getItem() as List<RemarkAssignStudentListResponse.StudentRemarkAssign>
                    } else {
                        Loaders.apiError.value = response.message()
                    }

                    Loaders.isLoading.value = false
                    */

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.isLoading.value = false
                    isRemarkListFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun postRemarksAssignData(remarkAssignList: RemarkAssignPostData) {

        Loaders.isLoading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.postRemarkAssignData(appPreferences.getToken()!!, remarkAssignList)

        call.enqueue(object: Callback<ValidUserResponse>{
            override fun onFailure(call: Call<ValidUserResponse>, t: Throwable) {
                Loaders.apiError.value = "Couldn't process data! Please try again."
                Loaders.isLoading.value = false
            }

            override fun onResponse(call: Call<ValidUserResponse>, response: Response<ValidUserResponse>) {
                try {

                    when {
                        response.isSuccessful -> {
                            if (response.body()?.msgType==1){
                                Loaders.apiSuccess.value = "Result Remarks Successfully Assigned"
                            }
                            else
                            {
                                Loaders.apiError.value = response.body()?.message
                            }
                        }
                        response.code() == 500 -> {
                            Loaders.apiError.value = "Server could not found!"
                        }
                        else -> {
                            Loaders.apiError.value = "Couldn't process data! Please try again."
                        }
                    }

                    Loaders.isLoading.value = false
                } catch (e: Exception) {
                    Loaders.apiError.value = "Something went wrong! Please try again."
                    Loaders.isLoading.value = false
                }
            }
        })
    }
}