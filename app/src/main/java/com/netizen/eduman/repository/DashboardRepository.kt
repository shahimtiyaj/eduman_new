package com.netizen.eduman.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.netizen.eduman.apiService.ApiClient
import com.netizen.eduman.apiService.ApiInterface
import com.netizen.eduman.database.LocalDatabase
import com.netizen.eduman.model.Institute
import com.netizen.eduman.model.SmsInfo
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DashboardRepository(application: Application) {

    private val balanceDepositDAO = LocalDatabase.getInstance(application).edumanDAO()
    val instituteInfoMutableLiveData: LiveData<List<Institute.InstituteInfo>>? =
        balanceDepositDAO.getAllInstituteInfoList()
    val lottieProgressDialog = MutableLiveData<Boolean>()
    var errorStatus = MutableLiveData<String>()
    var smsBalance = MutableLiveData<String>()
    var billPayment = MutableLiveData<String>()

    fun getInstituteFromServer(accessToken: String) {

        val header = HashMap<String?, String?>()
        val tokenType = "bearer"
        header["Content-Type"] = "application/x-www-form-urlencoded"
        header["Authorization"] = "$tokenType $accessToken"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getInstituteInfo(header)

        //calling the api
        call?.enqueue(object : Callback<Institute> {
            override fun onResponse(call: Call<Institute>, response: Response<Institute>) {
                try {

                    Log.d("onResponse", "Institute Info:" + response.body())

                    if (response.code() == 200) {

                        val institute = response.body()

                        if (response.body() != null) {
                            lottieProgressDialog.value = false
                            institute?.item?.let {
                                GlobalScope.launch {
                                    saveInstituteInfoList(it)
                                }
                            }
                        } else {
                            errorStatus.value = "Institute Information Not Found !"
                        }
                    }

                    lottieProgressDialog.value = false

                } catch (e: Exception) {
                    errorStatus.value = "Something went wrong! Please try again. "
                }
            }

            override fun onFailure(call: Call<Institute>, t: Throwable) {
                Log.d("onFailure", t.toString())
                lottieProgressDialog.value = false
                errorStatus.value = "Something went wrong! Please try again. "
            }
        })
    }

    private suspend fun saveInstituteInfoList(instituteInfoList: Institute.InstituteInfo) {
        Dispatchers.IO {
            try {
                instituteInfoList.let { balanceDepositDAO.insertInstituteInfoList(it) }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    lottieProgressDialog.value = false
                }
                Log.e("SQL_EXCEPTION", e.toString())
            }
        }
    }

    fun clearInstituteInfoAll() {
        GlobalScope.launch {
            balanceDepositDAO.deleteAll()
        }
    }


    fun getSmsBalanceFromServer(accessToken: String) {

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getSmsBalance(accessToken)

        //calling the api
        call?.enqueue(object : Callback<SmsInfo> {
            override fun onResponse(call: Call<SmsInfo>, response: Response<SmsInfo>) {
                try {

                    Log.d("onResponse", "SMS Info:" + response.body())

                    if (response.code() == 200) {

                        val messageType = response.body()?.msgType
                        if (messageType == 1) {
                            smsBalance.value = response.body()?.item
                        }
                    }

                } catch (e: Exception) {
                    Log.d("SMS Balance not Found", "Something went wrong! Please try again.")
                }
            }

            override fun onFailure(call: Call<SmsInfo>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }


    fun getBillPaymentInfoFromServer(instituteID: String) {

        val serviceNeti = Retrofit.Builder().baseUrl("https://api.netiworld.com/em/")
            .addConverterFactory(GsonConverterFactory.create()).build()

        val call = serviceNeti.create(ApiInterface::class.java).getBillPayment(instituteID)

        //calling the api
        call.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                try {

                    if (response.isSuccessful) {
                        val unpaidAmt = response.body()?.get("totalDue")
                        when {
                            unpaidAmt.toString() > 10.toString() -> {
                                billPayment.value = "UNPAID"
                            }
                            unpaidAmt.toString() < 10.toString() ->
                                billPayment.value = "PAID"
                            else ->
                                billPayment.value = "N/A"
                        }

                        Log.d("Bill Payment", "Total Due Amt:$unpaidAmt")
                    }

                } catch (e: Exception) {
                    Log.d("Bill Payment not Found ", "Something went wrong! Please try again.")
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }
}