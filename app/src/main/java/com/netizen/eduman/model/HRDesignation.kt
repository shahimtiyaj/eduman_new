package com.netizen.eduman.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class HRDesignation {

    @SerializedName("message")
    @Expose
    private var message: Any? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<Designation?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Designation?>? {
        return item
    }

    fun setItem(item: List<Designation?>?) {
        this.item = item
    }

    class Designation {

        @SerializedName("id")
        @Expose
         var id: String? = null

        @SerializedName("name")
        @Expose
         var name: String? = null

        fun getDesigID(): String? {
            return id
        }

        fun setDesigID(id: String?) {
            this.id = id
        }


        fun getDesigName(): String? {
            return name
        }

        fun setDesigName(name: String?) {
            this.name = name
        }
    }
}