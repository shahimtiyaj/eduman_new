package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TakeAtdStudentPost {
    @SerializedName("identificationIds")
    @Expose
    private var identificationIds: List<String?>? = ArrayList()

    @SerializedName("attendanceDate")
    @Expose
    private var attendanceDate: String? = null

    @SerializedName("classConfigId")
    @Expose
    private var classConfigId: String? = null

    @SerializedName("periodId")
    @Expose
    private  var periodId: String? = null

   @SerializedName("smsSendingStatus")
    @Expose
    private  var smsSendingStatus: Int? = 0

    fun getIdentificationIds(): List<String?>? {
        return identificationIds
    }

    fun setIdentificationIds(identificationIds: List<String?>?) {
        this.identificationIds = identificationIds
    }

    fun getAttendanceDate(): String? {
        return attendanceDate
    }

    fun setAttendanceDate(attendanceDate: String?) {
        this.attendanceDate = attendanceDate
    }

    fun getClassConfigId(): String? {
        return classConfigId
    }

    fun setClassConfigId(classConfigId: String?) {
        this.classConfigId = classConfigId
    }

    fun getPeriodId(): String? {
        return periodId
    }

    fun setPeriodId(periodId: String?) {
        this.periodId = periodId
    }

    fun getSmsSendingStatus(): Int? {
        return smsSendingStatus
    }

    fun setSmsSendingStatus(smsSendingStatus: Int?) {
        this.smsSendingStatus = smsSendingStatus
    }
}