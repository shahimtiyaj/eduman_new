package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MarkInputResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<MarkInput?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<MarkInput?>? {
        return item
    }

    fun setItem(item: List<MarkInput?>?) {
        this.item = item
    }

    class MarkInput {

        @SerializedName("markinputId")
        @Expose
        private var markinputId: String? = null
        @SerializedName("shortCode1")
        @Expose
        private var shortCode1: Double? = null
        @SerializedName("shortCode2")
        @Expose
        private var shortCode2: Double? = null
        @SerializedName("shortCode3")
        @Expose
        private var shortCode3: Double? = null
        @SerializedName("shortCode4")
        @Expose
        private var shortCode4: Double? = null
        @SerializedName("shortCode5")
        @Expose
        private var shortCode5: Double? = null
        @SerializedName("shortCode6")
        @Expose
        private var shortCode6: Double? = null
        @SerializedName("shortCode7")
        @Expose
        private var shortCode7: Double? = null
        @SerializedName("shortCode8")
        @Expose
        private var shortCode8: Double? = null
        @SerializedName("sc1Passmark")
        @Expose
        private var sc1Passmark: Double? = null
        @SerializedName("sc2Passmark")
        @Expose
        private var sc2Passmark: Double? = null
        @SerializedName("sc3Passmark")
        @Expose
        private var sc3Passmark: Double? = null
        @SerializedName("sc4Passmark")
        @Expose
        private var sc4Passmark: Double? = null
        @SerializedName("sc5Passmark")
        @Expose
        private var sc5Passmark: Double? = null
        @SerializedName("sc6Passmark")
        @Expose
        private var sc6Passmark: Double? = null
        @SerializedName("sc7Passmark")
        @Expose
        private var sc7Passmark: Double? = null
        @SerializedName("sc8Passmark")
        @Expose
        private var sc8Passmark: Double? = null
        @SerializedName("gradPoint")
        @Expose
        private var gradPoint: Double? = null
        @SerializedName("letterGrade")
        @Expose
        private var letterGrade: Any? = null
        @SerializedName("fullMark")
        @Expose
        private var fullMark: Double? = null
        @SerializedName("totalMark")
        @Expose
        private var totalMark: Double? = null
        @SerializedName("totalScore")
        @Expose
        private var totalScore: Double? = null
        @SerializedName("shortCode1Visibled")
        @Expose
        private var shortCode1Visibled: Boolean? = null
        @SerializedName("shortCode2Visibled")
        @Expose
        private var shortCode2Visibled: Boolean? = null
        @SerializedName("shortCode3Visibled")
        @Expose
        private var shortCode3Visibled: Boolean? = null
        @SerializedName("shortCode4Visibled")
        @Expose
        private var shortCode4Visibled: Boolean? = null
        @SerializedName("shortCode5Visibled")
        @Expose
        private var shortCode5Visibled: Boolean? = null
        @SerializedName("shortCode6Visibled")
        @Expose
        private var shortCode6Visibled: Boolean? = null
        @SerializedName("shortCode7Visibled")
        @Expose
        private var shortCode7Visibled: Boolean? = null
        @SerializedName("shortCode8Visibled")
        @Expose
        private var shortCode8Visibled: Boolean? = null
        @SerializedName("identificationId")
        @Expose
        private var identificationId: Int? = null
        @SerializedName("examConfigId")
        @Expose
        private var examConfigId: Any? = null
        @SerializedName("coreSettingSubjectId")
        @Expose
        private var coreSettingSubjectId: Any? = null
        @SerializedName("customStudentId")
        @Expose
        private var customStudentId: String? = null
        @SerializedName("studentName")
        @Expose
        private var studentName: String? = null
        @SerializedName("studentRoll")
        @Expose
        private var studentRoll: Int? = null
        @SerializedName("check")
        @Expose
        private var check: Boolean? = null
        @SerializedName("sbjCountableStatus")
        @Expose
        private var sbjCountableStatus: Any? = null

        fun getMarkinputId(): String? {
            return markinputId
        }

        fun setMarkinputId(markinputId: String?) {
            this.markinputId = markinputId
        }

        fun getShortCode1(): Double? {
            return shortCode1
        }

        fun setShortCode1(shortCode1: Double?) {
            this.shortCode1 = shortCode1
        }

        fun getShortCode2(): Double? {
            return shortCode2
        }

        fun setShortCode2(shortCode2: Double?) {
            this.shortCode2 = shortCode2
        }

        fun getShortCode3(): Double? {
            return shortCode3
        }

        fun setShortCode3(shortCode3: Double?) {
            this.shortCode3 = shortCode3
        }

        fun getShortCode4(): Double? {
            return shortCode4
        }

        fun setShortCode4(shortCode4: Double?) {
            this.shortCode4 = shortCode4
        }

        fun getShortCode5(): Double? {
            return shortCode5
        }

        fun setShortCode5(shortCode5: Double?) {
            this.shortCode5 = shortCode5
        }

        fun getShortCode6(): Double? {
            return shortCode6
        }

        fun setShortCode6(shortCode6: Double?) {
            this.shortCode6 = shortCode6
        }

        fun getShortCode7(): Double? {
            return shortCode7
        }

        fun setShortCode7(shortCode7: Double?) {
            this.shortCode7 = shortCode7
        }

        fun getShortCode8(): Double? {
            return shortCode8
        }

        fun setShortCode8(shortCode8: Double?) {
            this.shortCode8 = shortCode8
        }

        fun getSc1Passmark(): Double? {
            return sc1Passmark
        }

        fun setSc1Passmark(sc1Passmark: Double?) {
            this.sc1Passmark = sc1Passmark
        }

        fun getSc2Passmark(): Double? {
            return sc2Passmark
        }

        fun setSc2Passmark(sc2Passmark: Double?) {
            this.sc2Passmark = sc2Passmark
        }

        fun getSc3Passmark(): Double? {
            return sc3Passmark
        }

        fun setSc3Passmark(sc3Passmark: Double?) {
            this.sc3Passmark = sc3Passmark
        }

        fun getSc4Passmark(): Double? {
            return sc4Passmark
        }

        fun setSc4Passmark(sc4Passmark: Double?) {
            this.sc4Passmark = sc4Passmark
        }

        fun getSc5Passmark(): Double? {
            return sc5Passmark
        }

        fun setSc5Passmark(sc5Passmark: Double?) {
            this.sc5Passmark = sc5Passmark
        }

        fun getSc6Passmark(): Double? {
            return sc6Passmark
        }

        fun setSc6Passmark(sc6Passmark: Double?) {
            this.sc6Passmark = sc6Passmark
        }

        fun getSc7Passmark(): Double? {
            return sc7Passmark
        }

        fun setSc7Passmark(sc7Passmark: Double?) {
            this.sc7Passmark = sc7Passmark
        }

        fun getSc8Passmark(): Double? {
            return sc8Passmark
        }

        fun setSc8Passmark(sc8Passmark: Double?) {
            this.sc8Passmark = sc8Passmark
        }

        fun getGradPoint(): Double? {
            return gradPoint
        }

        fun setGradPoint(gradPoint: Double?) {
            this.gradPoint = gradPoint
        }

        fun getLetterGrade(): Any? {
            return letterGrade
        }

        fun setLetterGrade(letterGrade: Any?) {
            this.letterGrade = letterGrade
        }

        fun getFullMark(): Double? {
            return fullMark
        }

        fun setFullMark(fullMark: Double?) {
            this.fullMark = fullMark
        }

        fun getTotalMark(): Double? {
            return totalMark
        }

        fun setTotalMark(totalMark: Double?) {
            this.totalMark = totalMark
        }

        fun getTotalScore(): Double? {
            return totalScore
        }

        fun setTotalScore(totalScore: Double?) {
            this.totalScore = totalScore
        }

        fun getShortCode1Visibled(): Boolean? {
            return shortCode1Visibled
        }

        fun setShortCode1Visibled(shortCode1Visibled: Boolean?) {
            this.shortCode1Visibled = shortCode1Visibled
        }

        fun getShortCode2Visibled(): Boolean? {
            return shortCode2Visibled
        }

        fun setShortCode2Visibled(shortCode2Visibled: Boolean?) {
            this.shortCode2Visibled = shortCode2Visibled
        }

        fun getShortCode3Visibled(): Boolean? {
            return shortCode3Visibled
        }

        fun setShortCode3Visibled(shortCode3Visibled: Boolean?) {
            this.shortCode3Visibled = shortCode3Visibled
        }

        fun getShortCode4Visibled(): Boolean? {
            return shortCode4Visibled
        }

        fun setShortCode4Visibled(shortCode4Visibled: Boolean?) {
            this.shortCode4Visibled = shortCode4Visibled
        }

        fun getShortCode5Visibled(): Boolean? {
            return shortCode5Visibled
        }

        fun setShortCode5Visibled(shortCode5Visibled: Boolean?) {
            this.shortCode5Visibled = shortCode5Visibled
        }

        fun getShortCode6Visibled(): Boolean? {
            return shortCode6Visibled
        }

        fun setShortCode6Visibled(shortCode6Visibled: Boolean?) {
            this.shortCode6Visibled = shortCode6Visibled
        }

        fun getShortCode7Visibled(): Boolean? {
            return shortCode7Visibled
        }

        fun setShortCode7Visibled(shortCode7Visibled: Boolean?) {
            this.shortCode7Visibled = shortCode7Visibled
        }

        fun getShortCode8Visibled(): Boolean? {
            return shortCode8Visibled
        }

        fun setShortCode8Visibled(shortCode8Visibled: Boolean?) {
            this.shortCode8Visibled = shortCode8Visibled
        }

        fun getIdentificationId(): Int? {
            return identificationId
        }

        fun setIdentificationId(identificationId: Int?) {
            this.identificationId = identificationId
        }

        fun getExamConfigId(): Any? {
            return examConfigId
        }

        fun setExamConfigId(examConfigId: Any?) {
            this.examConfigId = examConfigId
        }

        fun getCoreSettingSubjectId(): Any? {
            return coreSettingSubjectId
        }

        fun setCoreSettingSubjectId(coreSettingSubjectId: Any?) {
            this.coreSettingSubjectId = coreSettingSubjectId
        }

        fun getCustomStudentId(): String? {
            return customStudentId
        }

        fun setCustomStudentId(customStudentId: String?) {
            this.customStudentId = customStudentId
        }

        fun getStudentName(): String? {
            return studentName
        }

        fun setStudentName(studentName: String?) {
            this.studentName = studentName
        }

        fun getStudentRoll(): Int? {
            return studentRoll
        }

        fun setStudentRoll(studentRoll: Int?) {
            this.studentRoll = studentRoll
        }

        fun getCheck(): Boolean? {
            return check
        }

        fun setCheck(check: Boolean?) {
            this.check = check
        }

        fun getSbjCountableStatus(): Any? {
            return sbjCountableStatus
        }

        fun setSbjCountableStatus(sbjCountableStatus: Any?) {
            this.sbjCountableStatus = sbjCountableStatus
        }
    }
}