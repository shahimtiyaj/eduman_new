package com.netizen.eduman.model

import com.google.gson.annotations.SerializedName

class TokenResponse {
    @SerializedName("access_token")
    var access_token: String? = ""

    fun getToken(): String? {
        return access_token
    }

    fun setToken(access_token: String?) {
        this.access_token = access_token
    }
}