package com.netizen.eduman.model

import com.google.gson.annotations.SerializedName

class ValidUserResponse {

    @SerializedName("message")
    var message: String? = ""

    @SerializedName("msgType")
    var msgType: Int?=0
}