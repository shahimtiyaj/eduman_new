package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class StudentAttendanceReportDetailsResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<StudentAttendanceReportDetails?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<StudentAttendanceReportDetails?>? {
        return item
    }

    fun setItem(item: List<StudentAttendanceReportDetails?>?) {
        this.item = item
    }

    class StudentAttendanceReportDetails {

        @SerializedName("customStudentId")
        @Expose
        private var customStudentId: String? = null
        @SerializedName("studentId")
        @Expose
        private var studentId: String? = null
        @SerializedName("studentRoll")
        @Expose
        private var studentRoll: Int? = null
        @SerializedName("studentName")
        @Expose
        private var studentName: String? = null
        @SerializedName("fatherName")
        @Expose
        private var fatherName: String? = null
        @SerializedName("motherName")
        @Expose
        private var motherName: String? = null
        @SerializedName("mobileNo")
        @Expose
        private var mobileNo: String? = null
        @SerializedName("startDate")
        @Expose
        private var startDate: Any? = null
        @SerializedName("endDate")
        @Expose
        private var endDate: Any? = null
        @SerializedName("gender")
        @Expose
        private var gender: String? = null
        @SerializedName("attendanceDetailsId")
        @Expose
        private var attendanceDetailsId: Int? = null
        @SerializedName("attendanceStatus")
        @Expose
        private var attendanceStatus: Any? = null
        @SerializedName("classConfigId")
        @Expose
        private var classConfigId: String? = null
        @SerializedName("sectionName")
        @Expose
        private var sectionName: String? = null

        fun getCustomStudentId(): String? {
            return customStudentId
        }

        fun setCustomStudentId(customStudentId: String?) {
            this.customStudentId = customStudentId
        }

        fun getStudentId(): String? {
            return studentId
        }

        fun setStudentId(studentId: String?) {
            this.studentId = studentId
        }

        fun getStudentRoll(): Int? {
            return studentRoll
        }

        fun setStudentRoll(studentRoll: Int?) {
            this.studentRoll = studentRoll
        }

        fun getStudentName(): String? {
            return studentName
        }

        fun setStudentName(studentName: String?) {
            this.studentName = studentName
        }

        fun getFatherName(): String? {
            return fatherName
        }

        fun setFatherName(fatherName: String?) {
            this.fatherName = fatherName
        }

        fun getMotherName(): String? {
            return motherName
        }

        fun setMotherName(motherName: String?) {
            this.motherName = motherName
        }

        fun getMobileNo(): String? {
            return mobileNo
        }

        fun setMobileNo(mobileNo: String?) {
            this.mobileNo = mobileNo
        }

        fun getStartDate(): Any? {
            return startDate
        }

        fun setStartDate(startDate: Any?) {
            this.startDate = startDate
        }

        fun getEndDate(): Any? {
            return endDate
        }

        fun setEndDate(endDate: Any?) {
            this.endDate = endDate
        }

        fun getGender(): String? {
            return gender
        }

        fun setGender(gender: String?) {
            this.gender = gender
        }

        fun getAttendanceDetailsId(): Int? {
            return attendanceDetailsId
        }

        fun setAttendanceDetailsId(attendanceDetailsId: Int?) {
            this.attendanceDetailsId = attendanceDetailsId
        }

        fun getAttendanceStatus(): Any? {
            return attendanceStatus
        }

        fun setAttendanceStatus(attendanceStatus: Any?) {
            this.attendanceStatus = attendanceStatus
        }

        fun getClassConfigId(): String? {
            return classConfigId
        }

        fun setClassConfigId(classConfigId: String?) {
            this.classConfigId = classConfigId
        }

        fun getSectionName(): String? {
            return sectionName
        }

        fun setSectionName(sectionName: String?) {
            this.sectionName = sectionName
        }
    }
}