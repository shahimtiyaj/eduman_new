package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GrandFinalExamResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<GrandExam?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<GrandExam?>? {
        return item
    }

    fun setItem(item: List<GrandExam?>?) {
        this.item = item
    }

    class GrandExam {

        @SerializedName("examConfigId")
        @Expose
        private var examConfigId: String? = null

        @SerializedName("examName")
        @Expose
        private var examName: String? = null

        fun getExamConfigId(): String? {
            return examConfigId
        }

        fun setExamConfigId(examConfigId: String?) {
            this.examConfigId = examConfigId
        }

        fun getExamName(): String? {
            return examName
        }

        fun setExamName(examName: String?) {
            this.examName = examName
        }
    }
}