package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RemarksTitleResponse {
    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<RemarksTitle?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<RemarksTitle?>? {
        return item
    }

    fun setItem(item: List<RemarksTitle?>?) {
        this.item = item
    }

    class RemarksTitle {
        @SerializedName("remarkTempId")
        @Expose
        private var remarkTempId: String? = null
        @SerializedName("remarkTitle")
        @Expose
        private var remarkTitle: String? = null

        fun geRemarkTemptId(): String? {
            return remarkTempId
        }

        fun seRemarkTemptId(remarkTempId: String?) {
            this.remarkTempId = remarkTempId
        }

        fun getRemarkTitle(): String? {
            return remarkTitle
        }

        fun setRemarkTitle(remarkTitle: String?) {
            this.remarkTitle = remarkTitle
        }
    }
}