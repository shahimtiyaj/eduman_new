package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class HREnlist {

    @SerializedName("category")
    @Expose
    private var category: String? = null
    @SerializedName("customId")
    @Expose
    private var customId: String? = null
    @SerializedName("designationId")
    @Expose
    private var designationId: String? = null
    @SerializedName("gender")
    @Expose
    private var gender: String? = null
    @SerializedName("staffMobile1")
    @Expose
    private var staffMobile1: String? = null
    @SerializedName("staffName")
    @Expose
    private var staffName: String? = null

    @SerializedName("staffReligion")
    @Expose
    private var staffReligion: String? = null

    constructor() {

    }


    constructor(
        category: String?,
        customId: String?,
        designationId: String?,
        gender: String?,
        staffMobile1: String?,
        staffName: String?,
        staffReligion: String?
    ) {
        this.category = category
        this.customId = customId
        this.designationId = designationId
        this.gender = gender
        this.staffMobile1 = staffMobile1
        this.staffName = staffName
        this.staffReligion = staffReligion
    }

    constructor(
        category: String?,
        designationId: String?,
        gender: String?,
        staffMobile1: String?,
        staffName: String?,
        staffReligion: String?
    ) {
        this.category = category
        this.designationId = designationId
        this.gender = gender
        this.staffMobile1 = staffMobile1
        this.staffName = staffName
        this.staffReligion = staffReligion
    }


    fun getCategory(): String? {
        return category
    }

    fun setCategory(category: String?) {
        this.category = category
    }

    fun getCustomId(): String? {
        return customId
    }

    fun setCustomId(customId: String?) {
        this.customId = customId
    }

    fun getDesignationId(): String? {
        return designationId
    }

    fun setDesignationId(designationId: String?) {
        this.designationId = designationId
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String?) {
        this.gender = gender
    }

    fun getStaffMobile1(): String? {
        return staffMobile1
    }

    fun setStaffMobile1(staffMobile1: String?) {
        this.staffMobile1 = staffMobile1
    }

    fun getStaffName(): String? {
        return staffName
    }

    fun setStaffName(staffName: String?) {
        this.staffName = staffName
    }

    fun getStaffReligion(): String? {
        return staffReligion
    }

    fun setStaffReligion(staffReligion: String?) {
        this.staffReligion = staffReligion
    }
}