package com.netizen.eduman.model

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Entity
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

class Institute{

    @SerializedName("item")
    var item: InstituteInfo? = null

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Parcelize
    @Entity(tableName = "InstituteInfo", primaryKeys = ["instituteId"])
    class InstituteInfo :Parcelable {

        @NonNull
        @SerializedName("instituteId")
        var instituteId: String? = null

        @SerializedName("instituteName")
        var instituteName: String? = null

        @SerializedName("instituteAddress")
        var instituteAddress: String? = null

        @SerializedName("academicYear")
        var academic_year: String? = null

        @SerializedName("instituteLogo")
        var instituteLogo: String? = null
    }
}

