package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GrandFinalClassResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<ClassSection?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<ClassSection?>? {
        return item
    }

    fun setItem(item: List<ClassSection?>?) {
        this.item = item
    }

    class ClassSection {

        @SerializedName("id")
        @Expose
        private var classConfigId: String? = null

        @SerializedName("name")
        @Expose
        private var classShiftSection: String? = null

        fun getClassConfigId(): String? {
            return classConfigId
        }

        fun setClassConfigId(classConfigId: String?) {
            this.classConfigId = classConfigId
        }

        fun getClassShiftSection(): String? {
            return classShiftSection
        }

        fun setClassShiftSection(classShiftSection: String?) {
            this.classShiftSection = classShiftSection
        }
    }
}