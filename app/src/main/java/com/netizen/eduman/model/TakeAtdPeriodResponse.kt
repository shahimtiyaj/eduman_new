package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*
Student Taking Attendance getting period List
 */
class TakeAtdPeriodResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<TakAtdPeriod?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<TakAtdPeriod?>? {
        return item
    }

    fun setItem(item: List<TakAtdPeriod?>?) {
        this.item = item
    }

    class TakAtdPeriod {

        @SerializedName("periodId")
        @Expose
        private var periodId: String? = null

        @SerializedName("periodName")
        @Expose
        private var periodName: String? = null


        fun getTakAtdPeriodId(): String? {
            return periodId
        }

        fun setTakAtdPeriodId(periodId: String?) {
            this.periodId = periodId
        }

        fun getTakAtdPeriodName(): String? {
            return periodName
        }

        fun setTakAtdPeriodName(periodName: String?) {
            this.periodName = periodName
        }

    }
}