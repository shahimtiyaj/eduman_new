package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SubjectResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<Subject?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Subject?>? {
        return item
    }

    fun setItem(item: List<Subject?>?) {
        this.item = item
    }

    class Subject {

        @SerializedName("subjectConfigurationId")
        @Expose
        private var subjectConfigurationId: String? = null
        @SerializedName("subjectSerial")
        @Expose
        private var subjectSerial: String? = null
        @SerializedName("subjectStatus")
        @Expose
        private var subjectStatus: String? = null
        @SerializedName("instituteId")
        @Expose
        private var instituteId: String? = null
        @SerializedName("classId")
        @Expose
        private var classId: String? = null
        @SerializedName("subjectId")
        @Expose
        private var subjectId: String? = null
        @SerializedName("instituteClass")
        @Expose
        private var instituteClass: InstituteClass? = null
        @SerializedName("instituteGroup")
        @Expose
        private var instituteGroup: InstituteGroup? = null
        @SerializedName("subject")
        @Expose
        private var subject: SubjectObject? = null
        @SerializedName("subjectName")
        @Expose
        private var subjectName: String? = null
        @SerializedName("subjectMergeId")
        @Expose
        private var subjectMergeId: String? = null

        fun getSubjectConfigurationId(): String? {
            return subjectConfigurationId
        }

        fun setSubjectConfigurationId(subjectConfigurationId: String?) {
            this.subjectConfigurationId = subjectConfigurationId
        }

        fun getSubjectSerial(): String? {
            return subjectSerial
        }

        fun setSubjectSerial(subjectSerial: String?) {
            this.subjectSerial = subjectSerial
        }

        fun getSubjectStatus(): String? {
            return subjectStatus
        }

        fun setSubjectStatus(subjectStatus: String?) {
            this.subjectStatus = subjectStatus
        }

        fun getInstituteId(): String? {
            return instituteId
        }

        fun setInstituteId(instituteId: String?) {
            this.instituteId = instituteId
        }

        fun getClassId(): String? {
            return classId
        }

        fun setClassId(classId: String?) {
            this.classId = classId
        }

        fun getSubjectId(): String? {
            return subjectId
        }

        fun setSubjectId(subjectId: String?) {
            this.subjectId = subjectId
        }

        fun getInstituteClass(): InstituteClass? {
            return instituteClass
        }

        fun setInstituteClass(instituteClass: InstituteClass?) {
            this.instituteClass = instituteClass
        }

        fun getInstituteGroup(): InstituteGroup? {
            return instituteGroup
        }

        fun setInstituteGroup(instituteGroup: InstituteGroup?) {
            this.instituteGroup = instituteGroup
        }

        fun getSubject(): SubjectObject? {
            return subject
        }

        fun setSubject(subject: SubjectObject?) {
            this.subject = subject
        }

        fun getSubjectName(): Any? {
            return subjectName
        }

        fun setSubjectName(subjectName: String?) {
            this.subjectName = subjectName
        }

        fun getSubjectMergeId(): String? {
            return subjectMergeId
        }

        fun setSubjectMergeId(subjectMergeId: String?) {
            this.subjectMergeId = subjectMergeId
        }

        class SubjectObject {

            @SerializedName("id")
            @Expose
            private var id: String? = null
            @SerializedName("name")
            @Expose
            private var name: String? = null
            @SerializedName("typeId")
            @Expose
            private var typeId: String? = null
            @SerializedName("typeName")
            @Expose
            private var typeName: String? = null
            @SerializedName("defaultId")
            @Expose
            private var defaultId: String? = null
            @SerializedName("viewStatus")
            @Expose
            private var viewStatus: Int? = null
            @SerializedName("viewSerial")
            @Expose
            private var viewSerial: Int? = null

            fun getId(): String? {
                return id
            }

            fun setId(id: String?) {
                this.id = id
            }

            fun getName(): String? {
                return name
            }

            fun setName(name: String?) {
                this.name = name
            }

            fun getTypeId(): String? {
                return typeId
            }

            fun setTypeId(typeId: String?) {
                this.typeId = typeId
            }

            fun getTypeName(): String? {
                return typeName
            }

            fun setTypeName(typeName: String?) {
                this.typeName = typeName
            }

            fun getDefaultId(): String? {
                return defaultId
            }

            fun setDefaultId(defaultId: String?) {
                this.defaultId = defaultId
            }

            fun getViewStatus(): Int? {
                return viewStatus
            }

            fun setViewStatus(viewStatus: Int?) {
                this.viewStatus = viewStatus
            }

            fun getViewSerial(): Int? {
                return viewSerial
            }

            fun setViewSerial(viewSerial: Int?) {
                this.viewSerial = viewSerial
            }
        }

        class InstituteClass {

            @SerializedName("id")
            @Expose
            private var id: String? = null
            @SerializedName("name")
            @Expose
            private var name: String? = null
            @SerializedName("typeId")
            @Expose
            private var typeId: String? = null
            @SerializedName("typeName")
            @Expose
            private var typeName: String? = null
            @SerializedName("defaultId")
            @Expose
            private var defaultId: String? = null
            @SerializedName("viewStatus")
            @Expose
            private var viewStatus: Int? = null
            @SerializedName("viewSerial")
            @Expose
            private var viewSerial: Int? = null

            fun getId(): String? {
                return id
            }

            fun setId(id: String?) {
                this.id = id
            }

            fun getName(): String? {
                return name
            }

            fun setName(name: String?) {
                this.name = name
            }

            fun getTypeId(): String? {
                return typeId
            }

            fun setTypeId(typeId: String?) {
                this.typeId = typeId
            }

            fun getTypeName(): String? {
                return typeName
            }

            fun setTypeName(typeName: String?) {
                this.typeName = typeName
            }

            fun getDefaultId(): String? {
                return defaultId
            }

            fun setDefaultId(defaultId: String?) {
                this.defaultId = defaultId
            }

            fun getViewStatus(): Int? {
                return viewStatus
            }

            fun setViewStatus(viewStatus: Int?) {
                this.viewStatus = viewStatus
            }

            fun getViewSerial(): Int? {
                return viewSerial
            }

            fun setViewSerial(viewSerial: Int?) {
                this.viewSerial = viewSerial
            }
        }

        class InstituteGroup {

            @SerializedName("id")
            @Expose
            private var id: String? = null
            @SerializedName("name")
            @Expose
            private var name: String? = null
            @SerializedName("typeId")
            @Expose
            private var typeId: String? = null
            @SerializedName("typeName")
            @Expose
            private var typeName: String? = null
            @SerializedName("defaultId")
            @Expose
            private var defaultId: String? = null
            @SerializedName("viewStatus")
            @Expose
            private var viewStatus: Int? = null
            @SerializedName("viewSerial")
            @Expose
            private var viewSerial: Int? = null

            fun getId(): String? {
                return id
            }

            fun setId(id: String?) {
                this.id = id
            }

            fun getName(): String? {
                return name
            }

            fun setName(name: String?) {
                this.name = name
            }

            fun getTypeId(): String? {
                return typeId
            }

            fun setTypeId(typeId: String?) {
                this.typeId = typeId
            }

            fun getTypeName(): String? {
                return typeName
            }

            fun setTypeName(typeName: String?) {
                this.typeName = typeName
            }

            fun getDefaultId(): String? {
                return defaultId
            }

            fun setDefaultId(defaultId: String?) {
                this.defaultId = defaultId
            }

            fun getViewStatus(): Int? {
                return viewStatus
            }

            fun setViewStatus(viewStatus: Int?) {
                this.viewStatus = viewStatus
            }

            fun getViewSerial(): Int? {
                return viewSerial
            }

            fun setViewSerial(viewSerial: Int?) {
                this.viewSerial = viewSerial
            }
        }
    }
}