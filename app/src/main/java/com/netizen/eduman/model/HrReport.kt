package com.netizen.eduman.model

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@JsonIgnoreProperties(ignoreUnknown = true)
class HrReport {
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<HRreportData?>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<HRreportData?>? {
        return item
    }

    fun setItem(item: List<HRreportData?>?) {
        this.item = item
    }

    @Parcelize
    class HRreportData : Parcelable {

        @SerializedName("customStaffId")
        @Expose
        private var customStaffId: String? = null

        @SerializedName("staffName")
        @Expose
        private var staffName: String? = null

        @SerializedName("gender")
        @Expose
        private var gender: String? = null

        @SerializedName("staffReligion")
        @Expose
        private var staffReligion: String? = null

        @SerializedName("staffCategory")
        @Expose
        private var staffCategory: String? = null

        @SerializedName("designationName")
        @Expose
        private var designationName: String? = null

        @SerializedName("bloodGroup")
        @Expose
        private var bloodGroup: String? = null

        @SerializedName("staffMobile1")
        @Expose
        private var staffMobile1: String? = null

        @SerializedName("image")
        @Expose
        private var image: String? = null


        fun getHr_id(): String? {
            return customStaffId
        }

        fun setHr_id(customStaffId: String) {
            this.customStaffId = customStaffId
        }

        fun getHr_name(): String? {
            return staffName
        }

        fun setHr_name(staffName: String) {
            this.staffName = staffName
        }

        fun getHr_gender(): String? {
            return gender
        }

        fun setHr_gender(hr_gender: String) {
            this.gender = hr_gender
        }

        fun getHr_religion(): String? {
            return staffReligion
        }

        fun setHr_religion(hr_religion: String) {
            this.staffReligion = hr_religion
        }

        fun getHr_category(): String? {
            return staffCategory
        }

        fun setHr_category(hr_category: String) {
            this.staffCategory = hr_category
        }

        fun getHr_designation(): String? {
            return designationName
        }

        fun setHr_designation(hr_designation: String) {
            this.designationName = hr_designation
        }

        fun getHr_blood_grp(): String? {
            return bloodGroup
        }

        fun setHr_blood_grp(hr_blood_grp: String) {
            this.bloodGroup = hr_blood_grp
        }

        fun getHr_phone(): String? {
            return staffMobile1
        }

        fun setHr_phone(hr_phone: String) {
            this.staffMobile1 = hr_phone
        }

        fun getHr_image(): String? {
            return image
        }

        fun setHr_image(hr_image: String) {
            this.image = hr_image
        }
    }
}