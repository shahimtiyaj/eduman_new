package com.netizen.eduman.model


class LoginUser {

    private var strUser: String? = null
    private var strPassword: String? = null

    constructor() {

    }

     constructor(userName: String?, userPassword: String?) {
        strUser = userName
        strPassword = userPassword
    }

    fun getStrUser(): String? {
        return strUser
    }

    fun getStrPassword(): String? {
        return strPassword
    }
}