package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class RemarksUpdateResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<RemarksUpdate?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<RemarksUpdate?>? {
        return item
    }

    fun setItem(item: List<RemarksUpdate?>?) {
        this.item = item
    }

    class RemarksUpdate {

        var isChecked: Boolean = false

        @SerializedName("remarkId")
        @Expose
        private var remarkId: String? = null
        @SerializedName("instituteId")
        @Expose
        private var instituteId: String? = null
        @SerializedName("remarks")
        @Expose
        private var remarks: String? = null
        @SerializedName("remarksTitle")
        @Expose
        private var remarksTitle: String? = null
        @SerializedName("examConfigId")
        @Expose
        private var examConfigId: String? = null
        @SerializedName("identificationId")
        @Expose
        private var identificationId: Int? = null
        @SerializedName("customStudentId")
        @Expose
        private var customStudentId: String? = null
        @SerializedName("studentName")
        @Expose
        private var studentName: String? = null
        @SerializedName("studentRoll")
        @Expose
        private var studentRoll: Int? = null
        @SerializedName("totalMarks")
        @Expose
        private var totalMarks: Double? = null
        @SerializedName("letterGrade")
        @Expose
        private var letterGrade: Any? = null
        @SerializedName("gpa")
        @Expose
        private var gpa: Double? = null

        fun getRemarkId(): String? {
            return remarkId
        }

        fun setRemarkId(remarkId: String?) {
            this.remarkId = remarkId
        }

        fun getInstituteId(): String? {
            return instituteId
        }

        fun setInstituteId(instituteId: String?) {
            this.instituteId = instituteId
        }

        fun getRemarks(): String? {
            return remarks
        }

        fun setRemarks(remarks: String?) {
            this.remarks = remarks
        }

        fun getRemarksTitle(): String? {
            return remarksTitle
        }

        fun setRemarksTitle(remarksTitle: String?) {
            this.remarksTitle = remarksTitle
        }

        fun getExamConfigId(): String? {
            return examConfigId
        }

        fun setExamConfigId(examConfigId: String?) {
            this.examConfigId = examConfigId
        }

        fun getIdentificationId(): Int? {
            return identificationId
        }

        fun setIdentificationId(identificationId: Int?) {
            this.identificationId = identificationId
        }

        fun getCustomStudentId(): String? {
            return customStudentId
        }

        fun setCustomStudentId(customStudentId: String?) {
            this.customStudentId = customStudentId
        }

        fun getStudentName(): String? {
            return studentName
        }

        fun setStudentName(studentName: String?) {
            this.studentName = studentName
        }

        fun getStudentRoll(): Int? {
            return studentRoll
        }

        fun setStudentRoll(studentRoll: Int?) {
            this.studentRoll = studentRoll
        }

        fun getTotalMarks(): Double? {
            return totalMarks
        }

        fun setTotalMarks(totalMarks: Double?) {
            this.totalMarks = totalMarks
        }

        fun getLetterGrade(): Any? {
            return letterGrade
        }

        fun setLetterGrade(letterGrade: Any?) {
            this.letterGrade = letterGrade
        }

        fun getGpa(): Double? {
            return gpa
        }

        fun setGpa(gpa: Double?) {
            this.gpa = gpa
        }
    }
}