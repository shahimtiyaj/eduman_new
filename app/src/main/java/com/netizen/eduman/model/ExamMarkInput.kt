package com.netizen.eduman.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ExamMarkInput {

    @SerializedName("examConfigurationId")
    @Expose
    private var examConfigurationId: String? = null
    @SerializedName("classConfigurationId")
    @Expose
    private var classConfigurationId: String? = null
    @SerializedName("groupId")
    @Expose
    private var groupId: String? = null
    @SerializedName("subjectId")
    @Expose
    private var subjectId: String? = null
    @SerializedName("type")
    @Expose
    private var type: String? = null
    @SerializedName("examMarkInputRqHelpers")
    @Expose
    private var examMarkInputRqHelpers: List<ExamMarkInputRqHelper?>? = null

    fun getExamConfigurationId(): String? {
        return examConfigurationId
    }

    fun setExamConfigurationId(examConfigurationId: String?) {
        this.examConfigurationId = examConfigurationId
    }

    fun getClassConfigurationId(): String? {
        return classConfigurationId
    }

    fun setClassConfigurationId(classConfigurationId: String?) {
        this.classConfigurationId = classConfigurationId
    }

    fun getGroupId(): String? {
        return groupId
    }

    fun setGroupId(groupId: String?) {
        this.groupId = groupId
    }

    fun getSubjectId(): String? {
        return subjectId
    }

    fun setSubjectId(subjectId: String?) {
        this.subjectId = subjectId
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String?) {
        this.type = type
    }

    fun getExamMarkInputRqHelpers(): List<ExamMarkInputRqHelper?>? {
        return examMarkInputRqHelpers
    }

    fun setExamMarkInputRqHelpers(examMarkInputRqHelpers: List<ExamMarkInputRqHelper?>?) {
        this.examMarkInputRqHelpers = examMarkInputRqHelpers
    }


    class ExamMarkInputRqHelper {

        private var adapterPosition: Int? = null
        private var defaultId: Int? = null
        @SerializedName("identificationId")
        @Expose
        private var identificationId: String? = null
        @SerializedName("markinputId")
        @Expose
        private var markInputId: String? = null
        @SerializedName("shortCode1")
        @Expose
        private var shortCode1: String? = "0.0"
        @SerializedName("shortCode2")
        @Expose
        private var shortCode2: String? = "0.0"
        @SerializedName("shortCode3")
        @Expose
        private var shortCode3: String? = "0.0"
        @SerializedName("shortCode4")
        @Expose
        private var shortCode4: String? = "0.0"
        @SerializedName("shortCode5")
        @Expose
        private var shortCode5: String? = "0.0"
        @SerializedName("shortCode6")
        @Expose
        private var shortCode6: String? = "0.0"
        @SerializedName("shortCode7")
        @Expose
        private var shortCode7: String? = "0.0"
        @SerializedName("shortCode8")
        @Expose
        private var shortCode8: String? = "0.0"

        fun getAdapterPosition(): Int? {
            return adapterPosition
        }

        fun setAdapterPosition(adapterPosition: Int?) {
            this.adapterPosition = adapterPosition
        }

        fun getDefaultId(): Int? {
            return defaultId
        }

        fun setDefaultId(defaultId: Int?) {
            this.defaultId = defaultId
        }

        fun getIdentificationId(): String? {
            return identificationId
        }

        fun setIdentificationId(identificationId: String?) {
            this.identificationId = identificationId
        }

        fun getMarkInputId(): String? {
            return markInputId
        }

        fun setMarkInputId(markInputId: String?) {
            this.markInputId = markInputId
        }

        fun getShortCode1(): String? {
            return shortCode1
        }

        fun setShortCode1(shortCode1: String?) {
            this.shortCode1 = shortCode1
        }

        fun getShortCode2(): String? {
            return shortCode2
        }

        fun setShortCode2(shortCode2: String?) {
            this.shortCode2 = shortCode2
        }

        fun getShortCode3(): String? {
            return shortCode3
        }

        fun setShortCode3(shortCode3: String?) {
            this.shortCode3 = shortCode3
        }

        fun getShortCode4(): String? {
            return shortCode4
        }

        fun setShortCode4(shortCode4: String?) {
            this.shortCode4 = shortCode4
        }

        fun getShortCode5(): String? {
            return shortCode5
        }

        fun setShortCode5(shortCode5: String?) {
            this.shortCode5 = shortCode5
        }

        fun getShortCode6(): String? {
            return shortCode6
        }

        fun setShortCode6(shortCode6: String?) {
            this.shortCode6 = shortCode6
        }

        fun getShortCode7(): String? {
            return shortCode7
        }

        fun setShortCode7(shortCode7: String?) {
            this.shortCode7 = shortCode7
        }

        fun getShortCode8(): String? {
            return shortCode8
        }

        fun setShortCode8(shortCode8: String?) {
            this.shortCode8 = shortCode8
        }
    }
}