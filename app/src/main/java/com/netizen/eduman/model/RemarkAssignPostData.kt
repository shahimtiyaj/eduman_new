package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class RemarkAssignPostData {
    @SerializedName("identificationIds")
    @Expose
    private var identificationIds: List<String?>? = ArrayList()

    @SerializedName("examConfigId")
    @Expose
    private var examConfigId: String? = null

    @SerializedName("remarks")
    @Expose
    private var remarks: String? = null

    constructor() {

    }

    constructor(identificationIds: List<String?>?, examConfigId: String?, remarks: String?) {
        this.identificationIds = identificationIds
        this.examConfigId = examConfigId
        this.remarks = remarks
    }

    fun getIdentificationIds(): List<String?>? {
        return identificationIds
    }

    fun setIdentificationIds(identificationIds: List<String?>?) {
        this.identificationIds = identificationIds
    }

    fun getExamConfigId(): String? {
        return examConfigId
    }

    fun setExamConfigId(examConfigId: String?) {
        this.examConfigId = examConfigId
    }

    fun getRemarks(): String? {
        return remarks
    }

    fun setRemarks(remarks: String?) {
        this.remarks = remarks
    }
}