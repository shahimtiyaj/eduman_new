package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GroupResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<Group?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Group?>? {
        return item
    }

    fun setItem(item: List<Group?>?) {
        this.item = item
    }

    class Group {

        @SerializedName("groupConfigId")
        @Expose
        private var groupConfigId: String? = null
        @SerializedName("instituteId")
        @Expose
        private var instituteId: String? = null
        @SerializedName("createDate")
        @Expose
        private var createDate: String? = null
        @SerializedName("viewStatus")
        @Expose
        private var viewStatus: Int? = null
        @SerializedName("classObject")
        @Expose
        private var classObject: ClassObject? = null
        @SerializedName("groupObject")
        @Expose
        private var groupObject: GroupObject? = null

        fun getGroupConfigId(): String? {
            return groupConfigId
        }

        fun setGroupConfigId(groupConfigId: String?) {
            this.groupConfigId = groupConfigId
        }

        fun getInstituteId(): String? {
            return instituteId
        }

        fun setInstituteId(instituteId: String?) {
            this.instituteId = instituteId
        }

        fun getCreateDate(): String? {
            return createDate
        }

        fun setCreateDate(createDate: String?) {
            this.createDate = createDate
        }

        fun getViewStatus(): Int? {
            return viewStatus
        }

        fun setViewStatus(viewStatus: Int?) {
            this.viewStatus = viewStatus
        }

        fun getClassObject(): ClassObject? {
            return classObject
        }

        fun setClassObject(classObject: ClassObject?) {
            this.classObject = classObject
        }

        fun getGroupObject(): GroupObject? {
            return groupObject
        }

        fun setGroupObject(groupObject: GroupObject?) {
            this.groupObject = groupObject
        }

        class ClassObject {
            @SerializedName("id")
            @Expose
            private var id: String? = null
            @SerializedName("name")
            @Expose
            private var name: String? = null
            @SerializedName("typeId")
            @Expose
            private var typeId: Int? = null
            @SerializedName("typeName")
            @Expose
            private var typeName: String? = null
            @SerializedName("defaultId")
            @Expose
            private var defaultId: String? = null
            @SerializedName("viewStatus")
            @Expose
            private var viewStatus: Int? = null
            @SerializedName("viewSerial")
            @Expose
            private var viewSerial: Int? = null

            fun getId(): String? {
                return id
            }

            fun setId(id: String?) {
                this.id = id
            }

            fun getName(): String? {
                return name
            }

            fun setName(name: String?) {
                this.name = name
            }

            fun getTypeId(): Int? {
                return typeId
            }

            fun setTypeId(typeId: Int?) {
                this.typeId = typeId
            }

            fun getTypeName(): String? {
                return typeName
            }

            fun setTypeName(typeName: String?) {
                this.typeName = typeName
            }

            fun getDefaultId(): String? {
                return defaultId
            }

            fun setDefaultId(defaultId: String?) {
                this.defaultId = defaultId
            }

            fun getViewStatus(): Int? {
                return viewStatus
            }

            fun setViewStatus(viewStatus: Int?) {
                this.viewStatus = viewStatus
            }

            fun getViewSerial(): Int? {
                return viewSerial
            }

            fun setViewSerial(viewSerial: Int?) {
                this.viewSerial = viewSerial
            }
        }

        class GroupObject {

            @SerializedName("id")
            @Expose
            private var id: String? = null
            @SerializedName("name")
            @Expose
            private var name: String? = null
            @SerializedName("typeId")
            @Expose
            private var typeId: Int? = null
            @SerializedName("typeName")
            @Expose
            private var typeName: String? = null
            @SerializedName("defaultId")
            @Expose
            private var defaultId: String? = null
            @SerializedName("viewStatus")
            @Expose
            private var viewStatus: Int? = null
            @SerializedName("viewSerial")
            @Expose
            private var viewSerial: Int? = null

            fun getId(): String? {
                return id
            }

            fun setId(id: String?) {
                this.id = id
            }

            fun getName(): String? {
                return name
            }

            fun setName(name: String?) {
                this.name = name
            }

            fun getTypeId(): Int? {
                return typeId
            }

            fun setTypeId(typeId: Int?) {
                this.typeId = typeId
            }

            fun getTypeName(): String? {
                return typeName
            }

            fun setTypeName(typeName: String?) {
                this.typeName = typeName
            }

            fun getDefaultId(): String? {
                return defaultId
            }

            fun setDefaultId(defaultId: String?) {
                this.defaultId = defaultId
            }

            fun getViewStatus(): Int? {
                return viewStatus
            }

            fun setViewStatus(viewStatus: Int?) {
                this.viewStatus = viewStatus
            }

            fun getViewSerial(): Int? {
                return viewSerial
            }

            fun setViewSerial(viewSerial: Int?) {
                this.viewSerial = viewSerial
            }
        }
    }
}