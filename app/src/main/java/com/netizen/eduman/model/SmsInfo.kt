package com.netizen.eduman.model

import com.google.gson.annotations.SerializedName

class SmsInfo {
    @SerializedName("item")
    var item: String? = ""

    @SerializedName("msgType")
    var msgType: Int? = 0
}