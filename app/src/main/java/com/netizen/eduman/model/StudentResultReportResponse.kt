package com.netizen.eduman.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

class StudentResultReportResponse {

    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<StudentReport?>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<StudentReport?>? {
        return item
    }

    fun setItem(item: List<StudentReport?>?) {
        this.item = item
    }


    @Parcelize
    class StudentReport : Parcelable {

        @SerializedName("customStudentId")
        @Expose
        private var customStudentId: String? = null

        @SerializedName("studentRoll")
        @Expose
        private var studentRoll: Int? = null

        @SerializedName("studentName")
        @Expose
        private var studentName: String? = null

        @SerializedName("totalMarks")
        @Expose
        private var totalMarks: String? = null

        @SerializedName("letterGrade")
        @Expose
        private var letterGrade: String? = null

        @SerializedName("gradingPoint")
        @Expose
        private var gradingPoint: String? = null

        @SerializedName("passFailStatus")
        @Expose
        private var passFailStatus: String? = null

        @SerializedName("sectionPosition")
        @Expose
        private var sectionPosition: String? = null

        @SerializedName("numOfFailedSubjects")
        @Expose
        private var numOfFailedSubjects: String? = null

        fun getCustomStudentId(): String? {
            return customStudentId
        }

        fun setCustomStudentId(customStudentId: String?) {
            this.customStudentId = customStudentId
        }

        fun getStudentRoll(): Int? {
            return studentRoll
        }

        fun setStudentRoll(studentRoll: Int?) {
            this.studentRoll = studentRoll
        }

        fun getStudentName(): String? {
            return studentName
        }

        fun setStudentName(studentName: String?) {
            this.studentName = studentName
        }

        fun getTotalMarks(): String? {
            return totalMarks
        }

        fun setTotalMarks(totalMarks: String?) {
            this.totalMarks = totalMarks
        }

        fun getLetterGrade(): String? {
            return letterGrade
        }

        fun setLetterGrade(letterGrade: String?) {
            this.letterGrade = letterGrade
        }

        fun getGradingPoint(): String? {
            return gradingPoint
        }

        fun setGradingPoint(gradingPoint: String?) {
            this.gradingPoint = gradingPoint
        }

        fun getPassFailStatus(): String? {
            return passFailStatus
        }

        fun setPassFailStatus(passFailStatus: String?) {
            this.passFailStatus = passFailStatus
        }

        fun getSectionPosition(): String? {
            return sectionPosition
        }

        fun setSectionPosition(sectionPosition: String?) {
            this.sectionPosition = sectionPosition
        }

        fun getNumOfFailedSubjects(): String? {
            return numOfFailedSubjects
        }

        fun setNumOfFailedSubjects(numOfFailedSubjects: String?) {
            this.numOfFailedSubjects = numOfFailedSubjects
        }
    }
}