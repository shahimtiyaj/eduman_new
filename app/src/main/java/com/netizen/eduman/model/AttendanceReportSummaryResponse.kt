package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AttendanceReportSummaryResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<AttendanceReportSummary?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<AttendanceReportSummary?>? {
        return item
    }

    fun setItem(item: List<AttendanceReportSummary?>?) {
        this.item = item
    }

    class AttendanceReportSummary {

        @SerializedName("attendanceDate")
        @Expose
        private var attendanceDate: String? = null
        @SerializedName("className")
        @Expose
        private var className: String? = null
        @SerializedName("shiftName")
        @Expose
        private var shiftName: String? = null
        @SerializedName("sectionName")
        @Expose
        private var sectionName: String? = null
        @SerializedName("totalStds")
        @Expose
        private var totalStds: Int? = null
        @SerializedName("presentStds")
        @Expose
        private var presentStds: Int? = null
        @SerializedName("absentStds")
        @Expose
        private var absentStds: Int? = null
        @SerializedName("leaveStds")
        @Expose
        private var leaveStds: Int? = null
        @SerializedName("attendanceTakenStds")
        @Expose
        private var attendanceTakenStds: Int? = null
        @SerializedName("totalLeavePercent")
        @Expose
        private var totalLeavePercent: Double? = null
        @SerializedName("totalPresentPercent")
        @Expose
        private var totalPresentPercent: Double? = null
        @SerializedName("totalAbsentPercent")
        @Expose
        private var totalAbsentPercent: Double? = null
        @SerializedName("classConfigId")
        @Expose
        private var classConfigId: String? = null

        fun getAttendanceDate(): String? {
            return attendanceDate
        }

        fun setAttendanceDate(attendanceDate: String?) {
            this.attendanceDate = attendanceDate
        }

        fun getClassName(): String? {
            return className
        }

        fun setClassName(className: String?) {
            this.className = className
        }

        fun getShiftName(): String? {
            return shiftName
        }

        fun setShiftName(shiftName: String?) {
            this.shiftName = shiftName
        }

        fun getSectionName(): String? {
            return sectionName
        }

        fun setSectionName(sectionName: String?) {
            this.sectionName = sectionName
        }

        fun getTotalStds(): Int? {
            return totalStds
        }

        fun setTotalStds(totalStds: Int?) {
            this.totalStds = totalStds
        }

        fun getPresentStds(): Int? {
            return presentStds
        }

        fun setPresentStds(presentStds: Int?) {
            this.presentStds = presentStds
        }

        fun getAbsentStds(): Int? {
            return absentStds
        }

        fun setAbsentStds(absentStds: Int?) {
            this.absentStds = absentStds
        }

        fun getLeaveStds(): Int? {
            return leaveStds
        }

        fun setLeaveStds(leaveStds: Int?) {
            this.leaveStds = leaveStds
        }

        fun getAttendanceTakenStds(): Int? {
            return attendanceTakenStds
        }

        fun setAttendanceTakenStds(attendanceTakenStds: Int?) {
            this.attendanceTakenStds = attendanceTakenStds
        }

        fun getTotalLeavePercent(): Double? {
            return totalLeavePercent
        }

        fun setTotalLeavePercent(totalLeavePercent: Double?) {
            this.totalLeavePercent = totalLeavePercent
        }

        fun getTotalPresentPercent(): Double? {
            return totalPresentPercent
        }

        fun setTotalPresentPercent(totalPresentPercent: Double?) {
            this.totalPresentPercent = totalPresentPercent
        }

        fun getTotalAbsentPercent(): Double? {
            return totalAbsentPercent
        }

        fun setTotalAbsentPercent(totalAbsentPercent: Double?) {
            this.totalAbsentPercent = totalAbsentPercent
        }

        fun getClassConfigId(): String? {
            return classConfigId
        }

        fun setClassConfigId(classConfigId: String?) {
            this.classConfigId = classConfigId
        }
    }
}