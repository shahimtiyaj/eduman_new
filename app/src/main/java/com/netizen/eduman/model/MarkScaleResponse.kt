package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MarkScaleResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<MarkScale?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<MarkScale?>? {
        return item
    }

    fun setItem(item: List<MarkScale?>?) {
        this.item = item
    }

    class MarkScale {

        @SerializedName("subjectName")
        @Expose
        private var subjectName: Any? = null
        @SerializedName("shortCodeName")
        @Expose
        private var shortCodeName: String? = null
        @SerializedName("passMark")
        @Expose
        private var passMark: Double? = null
        @SerializedName("totalMark")
        @Expose
        private var totalMark: Double? = null
        @SerializedName("acceptance")
        @Expose
        private var acceptance: Double? = null
        @SerializedName("defaultId")
        @Expose
        private var defaultId: Int? = null
        @SerializedName("subjectSerial")
        @Expose
        private var subjectSerial: Int? = null
        @SerializedName("perdefinedId")
        @Expose
        private var perdefinedId: Int? = null

        fun getSubjectName(): Any? {
            return subjectName
        }

        fun setSubjectName(subjectName: Any?) {
            this.subjectName = subjectName
        }

        fun getShortCodeName(): String? {
            return shortCodeName
        }

        fun setShortCodeName(shortCodeName: String?) {
            this.shortCodeName = shortCodeName
        }

        fun getPassMark(): Double? {
            return passMark
        }

        fun setPassMark(passMark: Double?) {
            this.passMark = passMark
        }

        fun getTotalMark(): Double? {
            return totalMark
        }

        fun setTotalMark(totalMark: Double?) {
            this.totalMark = totalMark
        }

        fun getAcceptance(): Double? {
            return acceptance
        }

        fun setAcceptance(acceptance: Double?) {
            this.acceptance = acceptance
        }

        fun getDefaultId(): Int? {
            return defaultId
        }

        fun setDefaultId(defaultId: Int?) {
            this.defaultId = defaultId
        }

        fun getSubjectSerial(): Int? {
            return subjectSerial
        }

        fun setSubjectSerial(subjectSerial: Int?) {
            this.subjectSerial = subjectSerial
        }

        fun getPerdefinedId(): Int? {
            return perdefinedId
        }

        fun setPerdefinedId(perdefinedId: Int?) {
            this.perdefinedId = perdefinedId
        }
    }
}