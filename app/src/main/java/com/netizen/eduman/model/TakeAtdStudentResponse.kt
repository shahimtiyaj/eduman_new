package com.netizen.eduman.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/*
Student Taking Attendance getting Student List
 */
class TakeAtdStudentResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<TakAtdStudent?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<TakAtdStudent?>? {
        return item
    }

    fun setItem(item: List<TakAtdStudent?>?) {
        this.item = item
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    class TakAtdStudent {

        @SerializedName("customStudentId")
        @Expose
        private var customStudentId: String? = null

        @SerializedName("studentRoll")
        @Expose
        private var studentRoll: String? = null

        @SerializedName("studentName")
        @Expose
        private var studentName: String? = null

        @SerializedName("studentGender")
        @Expose
        private var studentGender: String? = null

        @SerializedName("identificationId")
        @Expose
        private var identificationId: String? = null


        fun getCustomStudentId(): String? {
            return customStudentId
        }

        fun setCustomStudentId(customStudentId: String?) {
            this.customStudentId = customStudentId
        }

        fun getStudentRoll(): String? {
            return studentRoll
        }

        fun setStudentRoll(studentRoll: String?) {
            this.studentRoll = studentRoll
        }

        fun getStudentName(): String? {
            return studentName
        }

        fun setStudentName(studentName: String?) {
            this.studentRoll = studentName
        }

        fun getStudentGender(): String? {
            return studentGender
        }

        fun setStudentGender(studentGender: String?) {
            this.studentGender = studentGender
        }

        fun getIdentificationId(): String? {
            return identificationId
        }

        fun setIdentificationId(identificationId: String?) {
            this.identificationId = identificationId
        }


   @SerializedName("")
        @Expose
        private var isSelected: Boolean = false

        fun getSelected(): Boolean {
            return isSelected
        }

        fun setSelected(selected: Boolean) {
            isSelected = selected
        }
    }
}