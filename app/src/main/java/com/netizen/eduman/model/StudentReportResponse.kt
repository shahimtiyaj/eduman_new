package com.netizen.eduman.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


class StudentReportResponse {

    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<StudentReport?>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<StudentReport?>? {
        return item
    }

    fun setItem(item: List<StudentReport?>?) {
        this.item = item
    }

    @Parcelize
    class StudentReport : Parcelable {

        @SerializedName("studentId")
        @Expose
        private var studentId: String? = null
        @SerializedName("customStudentId")
        @Expose
        private var customStudentId: String? = null
        @SerializedName("studentName")
        @Expose
        private var studentName: String? = null
        @SerializedName("studentGender")
        @Expose
        private var studentGender: String? = null
        @SerializedName("studentDOB")
        @Expose
        private var studentDOB: Any? = null
        @SerializedName("studentReligion")
        @Expose
        private var studentReligion: String? = null
        @SerializedName("motherName")
        @Expose
        private var motherName: String? = null
        @SerializedName("guardianMobile")
        @Expose
        private var guardianMobile: String? = null
        @SerializedName("registrationDate")
        @Expose
        private var registrationDate: String? = null
        @SerializedName("instituteId")
        @Expose
        private var instituteId: String? = null
        @SerializedName("academicSession")
        @Expose
        private var academicSession: Any? = null
        @SerializedName("bloodGroup")
        @Expose
        private var bloodGroup: Any? = null
        @SerializedName("fatherName")
        @Expose
        private var fatherName: String? = null
        @SerializedName("identificationId")
        @Expose
        private var identificationId: Int? = null
        @SerializedName("classConfigId")
        @Expose
        private var classConfigId: String? = null
        @SerializedName("classConfigName")
        @Expose
        private var classConfigName: String? = null
        @SerializedName("classId")
        @Expose
        private var classId: String? = null
        @SerializedName("className")
        @Expose
        private var className: String? = null
        @SerializedName("shiftId")
        @Expose
        private var shiftId: String? = null
        @SerializedName("shiftName")
        @Expose
        private var shiftName: String? = null
        @SerializedName("sectionId")
        @Expose
        private var sectionId: String? = null
        @SerializedName("sectionName")
        @Expose
        private var sectionName: String? = null
        @SerializedName("groupId")
        @Expose
        private var groupId: String? = null
        @SerializedName("groupName")
        @Expose
        private var groupName: String? = null
        @SerializedName("studentCategoryId")
        @Expose
        private var studentCategoryId: String? = null
        @SerializedName("studentCategoryName")
        @Expose
        private var studentCategoryName: String? = null
        @SerializedName("studentRoll")
        @Expose
        private var studentRoll: Int? = null
        @SerializedName("studentStatus")
        @Expose
        private var studentStatus: Boolean? = null
        @SerializedName("migrationStatus")
        @Expose
        private var migrationStatus: Boolean? = null
        @SerializedName("academicYear")
        @Expose
        private var academicYear: String? = null
        @SerializedName("registrationNo")
        @Expose
        private var registrationNo: Any? = null
        @SerializedName("check")
        @Expose
        private var check: Boolean? = null
        @SerializedName("deviceId")
        @Expose
        private var deviceId: Any? = null
        @SerializedName("stringDateOfBirth")
        @Expose
        private var stringDateOfBirth: Any? = null
        @SerializedName("age")
        @Expose
        private var age: Any? = null
        @SerializedName("email")
        @Expose
        private var email: Any? = null
        @SerializedName("imageName")
        @Expose
        private var imageName: Any? = null
        @SerializedName("mobileNo")
        @Expose
        private var mobileNo: String? = null
        @SerializedName("fatherNid")
        @Expose
        private var fatherNid: Any? = null
        @SerializedName("motherNid")
        @Expose
        private var motherNid: Any? = null
        @SerializedName("studentEmail")
        @Expose
        private var studentEmail: Any? = null
        @SerializedName("height")
        @Expose
        private var height: Any? = null
        @SerializedName("weight")
        @Expose
        private var weight: Any? = null
        @SerializedName("specialDisease")
        @Expose
        private var specialDisease: Any? = null
        @SerializedName("admissionCategory")
        @Expose
        private var admissionCategory: Any? = null
        @SerializedName("birthCertificateNo")
        @Expose
        private var birthCertificateNo: Any? = null
        @SerializedName("testimonialNo")
        @Expose
        private var testimonialNo: Any? = null
        @SerializedName("image")
        @Expose
        private var image: String? = null

        @SerializedName("pureByteImage")
        @Expose
        private var pureByteImage: String? = null

        @SerializedName("studentMobile")
        @Expose
        private var studentMobile: Any? = null
        @SerializedName("fullImagePath")
        @Expose
        private var fullImagePath: Any? = null

        fun getStudentId(): String? {
            return studentId
        }

        fun setStudentId(studentId: String?) {
            this.studentId = studentId
        }

        fun getCustomStudentId(): String? {
            return customStudentId
        }

        fun setCustomStudentId(customStudentId: String?) {
            this.customStudentId = customStudentId
        }

        fun getStudentName(): String? {
            return studentName
        }

        fun setStudentName(studentName: String?) {
            this.studentName = studentName
        }

        fun getStudentGender(): String? {
            return studentGender
        }

        fun setStudentGender(studentGender: String?) {
            this.studentGender = studentGender
        }

        fun getStudentDOB(): Any? {
            return studentDOB
        }

        fun setStudentDOB(studentDOB: Any?) {
            this.studentDOB = studentDOB
        }

        fun getStudentReligion(): String? {
            return studentReligion
        }

        fun setStudentReligion(studentReligion: String?) {
            this.studentReligion = studentReligion
        }

        fun getMotherName(): String? {
            return motherName
        }

        fun setMotherName(motherName: String?) {
            this.motherName = motherName
        }

        fun getGuardianMobile(): String? {
            return guardianMobile
        }

        fun setGuardianMobile(guardianMobile: String?) {
            this.guardianMobile = guardianMobile
        }

        fun getRegistrationDate(): String? {
            return registrationDate
        }

        fun setRegistrationDate(registrationDate: String?) {
            this.registrationDate = registrationDate
        }

        fun getInstituteId(): String? {
            return instituteId
        }

        fun setInstituteId(instituteId: String?) {
            this.instituteId = instituteId
        }

        fun getAcademicSession(): Any? {
            return academicSession
        }

        fun setAcademicSession(academicSession: Any?) {
            this.academicSession = academicSession
        }

        fun getBloodGroup(): Any? {
            return bloodGroup
        }

        fun setBloodGroup(bloodGroup: Any?) {
            this.bloodGroup = bloodGroup
        }

        fun getFatherName(): String? {
            return fatherName
        }

        fun setFatherName(fatherName: String?) {
            this.fatherName = fatherName
        }

        fun getIdentificationId(): Int? {
            return identificationId
        }

        fun setIdentificationId(identificationId: Int?) {
            this.identificationId = identificationId
        }

        fun getClassConfigId(): String? {
            return classConfigId
        }

        fun setClassConfigId(classConfigId: String?) {
            this.classConfigId = classConfigId
        }

        fun getClassConfigName(): String? {
            return classConfigName
        }

        fun setClassConfigName(classConfigName: String?) {
            this.classConfigName = classConfigName
        }

        fun getClassId(): String? {
            return classId
        }

        fun setClassId(classId: String?) {
            this.classId = classId
        }

        fun getClassName(): String? {
            return className
        }

        fun setClassName(className: String?) {
            this.className = className
        }

        fun getShiftId(): String? {
            return shiftId
        }

        fun setShiftId(shiftId: String?) {
            this.shiftId = shiftId
        }

        fun getShiftName(): String? {
            return shiftName
        }

        fun setShiftName(shiftName: String?) {
            this.shiftName = shiftName
        }

        fun getSectionId(): String? {
            return sectionId
        }

        fun setSectionId(sectionId: String?) {
            this.sectionId = sectionId
        }

        fun getSectionName(): String? {
            return sectionName
        }

        fun setSectionName(sectionName: String?) {
            this.sectionName = sectionName
        }

        fun getGroupId(): String? {
            return groupId
        }

        fun setGroupId(groupId: String?) {
            this.groupId = groupId
        }

        fun getGroupName(): String? {
            return groupName
        }

        fun setGroupName(groupName: String?) {
            this.groupName = groupName
        }

        fun getStudentCategoryId(): String? {
            return studentCategoryId
        }

        fun setStudentCategoryId(studentCategoryId: String?) {
            this.studentCategoryId = studentCategoryId
        }

        fun getStudentCategoryName(): String? {
            return studentCategoryName
        }

        fun setStudentCategoryName(studentCategoryName: String?) {
            this.studentCategoryName = studentCategoryName
        }

        fun getStudentRoll(): Int? {
            return studentRoll
        }

        fun setStudentRoll(studentRoll: Int?) {
            this.studentRoll = studentRoll
        }

        fun getStudentStatus(): Boolean? {
            return studentStatus
        }

        fun setStudentStatus(studentStatus: Boolean?) {
            this.studentStatus = studentStatus
        }

        fun getMigrationStatus(): Boolean? {
            return migrationStatus
        }

        fun setMigrationStatus(migrationStatus: Boolean?) {
            this.migrationStatus = migrationStatus
        }

        fun getAcademicYear(): String? {
            return academicYear
        }

        fun setAcademicYear(academicYear: String?) {
            this.academicYear = academicYear
        }

        fun getRegistrationNo(): Any? {
            return registrationNo
        }

        fun setRegistrationNo(registrationNo: Any?) {
            this.registrationNo = registrationNo
        }

        fun getCheck(): Boolean? {
            return check
        }

        fun setCheck(check: Boolean?) {
            this.check = check
        }

        fun getDeviceId(): Any? {
            return deviceId
        }

        fun setDeviceId(deviceId: Any?) {
            this.deviceId = deviceId
        }

        fun getStringDateOfBirth(): Any? {
            return stringDateOfBirth
        }

        fun setStringDateOfBirth(stringDateOfBirth: Any?) {
            this.stringDateOfBirth = stringDateOfBirth
        }

        fun getAge(): Any? {
            return age
        }

        fun setAge(age: Any?) {
            this.age = age
        }

        fun getEmail(): Any? {
            return email
        }

        fun setEmail(email: Any?) {
            this.email = email
        }

        fun getImageName(): Any? {
            return imageName
        }

        fun setImageName(imageName: Any?) {
            this.imageName = imageName
        }

        fun getMobileNo(): String? {
            return mobileNo
        }

        fun setMobileNo(mobileNo: String?) {
            this.mobileNo = mobileNo
        }

        fun getFatherNid(): Any? {
            return fatherNid
        }

        fun setFatherNid(fatherNid: Any?) {
            this.fatherNid = fatherNid
        }

        fun getMotherNid(): Any? {
            return motherNid
        }

        fun setMotherNid(motherNid: Any?) {
            this.motherNid = motherNid
        }

        fun getStudentEmail(): Any? {
            return studentEmail
        }

        fun setStudentEmail(studentEmail: Any?) {
            this.studentEmail = studentEmail
        }

        fun getHeight(): Any? {
            return height
        }

        fun setHeight(height: Any?) {
            this.height = height
        }

        fun getWeight(): Any? {
            return weight
        }

        fun setWeight(weight: Any?) {
            this.weight = weight
        }

        fun getSpecialDisease(): Any? {
            return specialDisease
        }

        fun setSpecialDisease(specialDisease: Any?) {
            this.specialDisease = specialDisease
        }

        fun getAdmissionCategory(): Any? {
            return admissionCategory
        }

        fun setAdmissionCategory(admissionCategory: Any?) {
            this.admissionCategory = admissionCategory
        }

        fun getBirthCertificateNo(): Any? {
            return birthCertificateNo
        }

        fun setBirthCertificateNo(birthCertificateNo: Any?) {
            this.birthCertificateNo = birthCertificateNo
        }

        fun getTestimonialNo(): Any? {
            return testimonialNo
        }

        fun setTestimonialNo(testimonialNo: Any?) {
            this.testimonialNo = testimonialNo
        }

        fun getImage(): String? {
            return image
        }

        fun setImage(image: String?) {
            this.image = image
        }

        fun getPureByteImage(): String? {
            return pureByteImage
        }

        fun setPureByteImage(pureByteImage: String?) {
            this.pureByteImage = pureByteImage
        }

        fun getStudentMobile(): Any? {
            return studentMobile
        }

        fun setStudentMobile(studentMobile: Any?) {
            this.studentMobile = studentMobile
        }

        fun getFullImagePath(): Any? {
            return fullImagePath
        }

        fun setFullImagePath(fullImagePath: Any?) {
            this.fullImagePath = fullImagePath
        }
    }
}