package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SectionResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<Section?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Section?>? {
        return item
    }

    fun setItem(item: List<Section?>?) {
        this.item = item
    }

    class Section {

        @SerializedName("classConfigId")
        @Expose
        private var classConfigId: String? = null
        @SerializedName("classShiftSection")
        @Expose
        private var classShiftSection: String? = null
        @SerializedName("instituteId")
        @Expose
        private var instituteId: String? = null
        @SerializedName("classConfigSerial")
        @Expose
        private var classConfigSerial: Int? = null
        @SerializedName("viewStatus")
        @Expose
        private var viewStatus: Int? = null
        @SerializedName("createDate")
        @Expose
        private var createDate: String? = null

        fun getClassConfigId(): String? {
            return classConfigId
        }

        fun setClassConfigId(classConfigId: String?) {
            this.classConfigId = classConfigId
        }

        fun getClassShiftSection(): String? {
            return classShiftSection
        }

        fun setClassShiftSection(classShiftSection: String?) {
            this.classShiftSection = classShiftSection
        }

        fun getInstituteId(): String? {
            return instituteId
        }

        fun setInstituteId(instituteId: String?) {
            this.instituteId = instituteId
        }

        fun getClassConfigSerial(): Int? {
            return classConfigSerial
        }

        fun setClassConfigSerial(classConfigSerial: Int?) {
            this.classConfigSerial = classConfigSerial
        }

        fun getViewStatus(): Int? {
            return viewStatus
        }

        fun setViewStatus(viewStatus: Int?) {
            this.viewStatus = viewStatus
        }

        fun getCreateDate(): String? {
            return createDate
        }

        fun setCreateDate(createDate: String?) {
            this.createDate = createDate
        }
    }
}