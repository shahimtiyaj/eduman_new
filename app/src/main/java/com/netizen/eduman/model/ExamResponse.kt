package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.netizen.eduman.model.GroupResponse.Group.ClassObject


class ExamResponse {

    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<Exam?>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Exam?>? {
        return item
    }

    fun setItem(item: List<Exam?>?) {
        this.item = item
    }

    class Exam {

        @SerializedName("examConfigId")
        @Expose
        private var examConfigId: Int? = null
        @SerializedName("instituteId")
        @Expose
        private var instituteId: String? = null
        @SerializedName("examSerial")
        @Expose
        private var examSerial: Int? = null
        @SerializedName("percentage")
        @Expose
        private var percentage: Double? = null
        @SerializedName("viewStatus")
        @Expose
        private var viewStatus: Int? = null
        @SerializedName("meritProcessType")
        @Expose
        private var meritProcessType: Int? = null
        @SerializedName("grandfinalProcessType")
        @Expose
        private var grandfinalProcessType: Any? = null
        @SerializedName("classObject")
        @Expose
        private var classObject: ClassObject? = null
        @SerializedName("examObject")
        @Expose
        private var examObject: ExamObject? = null

        fun getExamConfigId(): Int? {
            return examConfigId
        }

        fun setExamConfigId(examConfigId: Int?) {
            this.examConfigId = examConfigId
        }

        fun getInstituteId(): String? {
            return instituteId
        }

        fun setInstituteId(instituteId: String?) {
            this.instituteId = instituteId
        }

        fun getExamSerial(): Int? {
            return examSerial
        }

        fun setExamSerial(examSerial: Int?) {
            this.examSerial = examSerial
        }

        fun getPercentage(): Double? {
            return percentage
        }

        fun setPercentage(percentage: Double?) {
            this.percentage = percentage
        }

        fun getViewStatus(): Int? {
            return viewStatus
        }

        fun setViewStatus(viewStatus: Int?) {
            this.viewStatus = viewStatus
        }

        fun getMeritProcessType(): Int? {
            return meritProcessType
        }

        fun setMeritProcessType(meritProcessType: Int?) {
            this.meritProcessType = meritProcessType
        }

        fun getGrandfinalProcessType(): Any? {
            return grandfinalProcessType
        }

        fun setGrandfinalProcessType(grandfinalProcessType: Any?) {
            this.grandfinalProcessType = grandfinalProcessType
        }

        fun getClassObject(): ClassObject? {
            return classObject
        }

        fun setClassObject(classObject: ClassObject?) {
            this.classObject = classObject
        }

        fun getExamObject(): ExamObject? {
            return examObject
        }

        fun setExamObject(examObject: ExamObject?) {
            this.examObject = examObject
        }

        class ExamObject {

            @SerializedName("id")
            @Expose
            private var id: String? = null
            @SerializedName("name")
            @Expose
            private var name: String? = null
            @SerializedName("typeId")
            @Expose
            private var typeId: String? = null
            @SerializedName("typeName")
            @Expose
            private var typeName: String? = null
            @SerializedName("defaultId")
            @Expose
            private var defaultId: String? = null
            @SerializedName("viewStatus")
            @Expose
            private var viewStatus: Int? = null
            @SerializedName("viewSerial")
            @Expose
            private var viewSerial: Int? = null

            fun getId(): String? {
                return id
            }

            fun setId(id: String?) {
                this.id = id
            }

            fun getName(): String? {
                return name
            }

            fun setName(name: String?) {
                this.name = name
            }

            fun getTypeId(): String? {
                return typeId
            }

            fun setTypeId(typeId: String?) {
                this.typeId = typeId
            }

            fun getTypeName(): String? {
                return typeName
            }

            fun setTypeName(typeName: String?) {
                this.typeName = typeName
            }

            fun getDefaultId(): String? {
                return defaultId
            }

            fun setDefaultId(defaultId: String?) {
                this.defaultId = defaultId
            }

            fun getViewStatus(): Int? {
                return viewStatus
            }

            fun setViewStatus(viewStatus: Int?) {
                this.viewStatus = viewStatus
            }

            fun getViewSerial(): Int? {
                return viewSerial
            }

            fun setViewSerial(viewSerial: Int?) {
                this.viewSerial = viewSerial
            }
        }
    }
}