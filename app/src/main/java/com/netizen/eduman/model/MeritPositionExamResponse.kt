package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MeritPositionExamResponse {

    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<MeritPosition?>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<MeritPosition?>? {
        return item
    }

    fun setItem(item: List<MeritPosition?>?) {
        this.item = item
    }

    class MeritPosition {

        @SerializedName("examConfigId")
        @Expose
        private var examConfigId: Int? = null

        @SerializedName("examObject")
        @Expose
        private var examObject: ExamObject? = null

        fun getExamConfigId(): Int? {
            return examConfigId
        }

        fun setExamConfigId(examConfigId: Int?) {
            this.examConfigId = examConfigId
        }

        fun getExamObject(): ExamObject? {
            return examObject
        }

        fun setExamObject(examObject: ExamObject?) {
            this.examObject = examObject
        }

        class ExamObject {

            @SerializedName("id")
            @Expose
            private var id: String? = null
            @SerializedName("name")
            @Expose
            private var name: String? = null

            fun getId(): String? {
                return id
            }

            fun setId(id: String?) {
                this.id = id
            }

            fun getName(): String? {
                return name
            }

            fun setName(name: String?) {
                this.name = name
            }
        }
    }
}