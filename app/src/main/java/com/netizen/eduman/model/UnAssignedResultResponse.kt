package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UnAssignedResultResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<UnAssignedResult?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<UnAssignedResult?>? {
        return item
    }

    fun setItem(item: List<UnAssignedResult?>?) {
        this.item = item
    }

    class UnAssignedResult {

        @SerializedName("sectionName")
        @Expose
        private var sectionName: String? = null
        @SerializedName("numOfTotalSubjects")
        @Expose
        private var numOfTotalSubjects: Int? = null
        @SerializedName("numOfMarkInputtedSubjects")
        @Expose
        private var numOfMarkInputtedSubjects: Int? = null
        @SerializedName("numOfMarkUnInputtedSubjects")
        @Expose
        private var numOfMarkUnInputtedSubjects: Int? = null
        @SerializedName("markUnInputtedSubjects")
        @Expose
        private var markUnInputtedSubjects: String? = null

        fun getSectionName(): String? {
            return sectionName
        }

        fun setSectionName(sectionName: String?) {
            this.sectionName = sectionName
        }

        fun getNumOfTotalSubjects(): Int? {
            return numOfTotalSubjects
        }

        fun setNumOfTotalSubjects(numOfTotalSubjects: Int?) {
            this.numOfTotalSubjects = numOfTotalSubjects
        }

        fun getNumOfMarkInputtedSubjects(): Int? {
            return numOfMarkInputtedSubjects
        }

        fun setNumOfMarkInputtedSubjects(numOfMarkInputtedSubjects: Int?) {
            this.numOfMarkInputtedSubjects = numOfMarkInputtedSubjects
        }

        fun getNumOfMarkUnInputtedSubjects(): Int? {
            return numOfMarkUnInputtedSubjects
        }

        fun setNumOfMarkUnInputtedSubjects(numOfMarkUnInputtedSubjects: Int?) {
            this.numOfMarkUnInputtedSubjects = numOfMarkUnInputtedSubjects
        }

        fun getMarkUnInputtedSubjects(): String? {
            return markUnInputtedSubjects
        }

        fun setMarkUnInputtedSubjects(markUnInputtedSubjects: String?) {
            this.markUnInputtedSubjects = markUnInputtedSubjects
        }
    }
}