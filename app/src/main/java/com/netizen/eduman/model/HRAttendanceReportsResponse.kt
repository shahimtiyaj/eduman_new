package com.netizen.eduman.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HRAttendanceReportsResponse {

    @SerializedName("message")
    @Expose
    private var message: Any? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<HRAttendanceReports?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<HRAttendanceReports?>? {
        return item
    }

    fun setItem(item: List<HRAttendanceReports?>?) {
        this.item = item
    }

    class HRAttendanceReports{

        @SerializedName("customStaffId")
        @Expose
        private var customStaffId: String? = null
        @SerializedName("staffName")
        @Expose
        private var staffName: String? = null
        @SerializedName("staffCategory")
        @Expose
        private var staffCategory: String? = null
        @SerializedName("staffDesignation")
        @Expose
        private var staffDesignation: String? = null
        @SerializedName("staffMobileNo")
        @Expose
        private var staffMobileNo: String? = null
        @SerializedName("stringInTime")
        @Expose
        private var stringInTime: String? = null


        fun getHr_p_id(): String? {
            return customStaffId
        }

        fun setHr_p_id(hr_p_id: String) {
            this.customStaffId = hr_p_id
        }

        fun getHr_p_name(): String? {
            return staffName
        }

        fun setHr_p_name(hr_p_name: String) {
            this.staffName = hr_p_name
        }

        fun getHr_p_category(): String? {
            return staffCategory
        }

        fun setHr_p_category(hr_p_category: String) {
            this.staffCategory = hr_p_category
        }

        fun getHr_p_designation(): String? {
            return staffDesignation
        }

        fun setHr_p_designation(hr_p_designation: String) {
            this.staffDesignation = hr_p_designation
        }

        fun getHr_p_mobile(): String? {
            return staffMobileNo
        }

        fun setHr_p_mobile(hr_p_mobile: String) {
            this.staffMobileNo = hr_p_mobile
        }

        fun getHr_p_in_time(): String? {
            return stringInTime
        }

        fun setHr_p_in_time(hr_p_in_time: String) {
            this.stringInTime = hr_p_in_time
        }

    }
}