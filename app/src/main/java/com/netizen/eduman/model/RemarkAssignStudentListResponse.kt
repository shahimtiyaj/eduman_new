package com.netizen.eduman.model


import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


class RemarkAssignStudentListResponse {

    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null
    @SerializedName("item")
    @Expose
    private var item: List<StudentRemarkAssign?>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<StudentRemarkAssign?>? {
        return item
    }

    fun setItem(item: List<StudentRemarkAssign?>?) {
        this.item = item
    }

    @Parcelize
    class StudentRemarkAssign : Parcelable {

        @SerializedName("identificationId")
        @Expose
        private var identificationId: String? = null
        @SerializedName("customStudentId")
        @Expose
        private var customStudentId: String? = null
        @SerializedName("studentRoll")
        @Expose
        private var studentRoll: String? = null
        @SerializedName("studentName")
        @Expose
        private var studentName: String? = null
        @SerializedName("totalMarks")
        @Expose
        private var totalMarks: Any? = null
        @SerializedName("letterGrade")
        @Expose
        private var letterGrade: String? = null
        @SerializedName("gpa")
        @Expose
        private var gpa: String? = null

        fun getIdentificationId(): String? {
            return identificationId
        }

        fun setIdentificationId(identificationId: String?) {
            this.identificationId = identificationId
        }

        fun getCustomStudentId(): String? {
            return customStudentId
        }

        fun setCustomStudentId(customStudentId: String?) {
            this.customStudentId = customStudentId
        }

        fun getStudentName(): String? {
            return studentName
        }

        fun setStudentName(studentName: String?) {
            this.studentName = studentName
        }

        fun getStudentRoll(): String? {
            return studentRoll
        }

        fun setStudentRoll(studentRoll: String?) {
            this.studentRoll = studentRoll
        }

        fun getTotalMarks(): Any? {
            return totalMarks
        }

        fun setTotalMarks(totalMarks: Any?) {
            this.totalMarks = totalMarks
        }

        fun getLetterGrade(): String? {
            return letterGrade
        }

        fun setLetterGrade(letterGrade: String?) {
            this.letterGrade = letterGrade
        }

        fun getGpa(): String? {
            return gpa
        }

        fun setGpa(gpa: String?) {
            this.gpa = gpa
        }

        @SerializedName("")
        @Expose
        private var isSelected: Boolean = false

        fun getSelected(): Boolean {
            return isSelected
        }

        fun setSelected(selected: Boolean) {
            isSelected = selected
        }

    }
}