package com.netizen.eduman.apiService

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.netizen.eduman.model.*
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @GET("institute/setting/user/check")
    fun userValidation(@Query("userName") name: String,
                       @Query("password") pass: String): Call<ValidUserResponse>

    @FormUrlEncoded
    @POST("oauth/token")
    fun postData(@FieldMap params: HashMap<String?, String?>,
                 @HeaderMap headers: HashMap<String?, String?>): Call<TokenResponse>

    @GET("institute/setting/user/info/details")
    fun getInstituteInfo(@HeaderMap headers: HashMap<String?, String?>): Call<Institute>

    @GET("sms/report/sms/balance")
    fun getSmsBalance(@Query("access_token") accessToken: String): Call<SmsInfo>

    @GET("dashboard/info")
    fun getBillPayment(@Query("instituteId") instituteId: String): Call<JsonObject>

    @GET("core/setting/list/by-type-id")
    fun getHRDesignation(@Query("access_token") accessToken: String,
                         @Query("typeId") typeId: String): Call<HRDesignation>//type_ID==2601 fixed

    @POST("staff/basic/save")
    fun postHREnlistDataHRAutoID(@Query("access_token") accessToken: String,
                                 @Body obj: Array<*>): Call<ValidUserResponse>

    @POST("staff/basic/save/custom-id")
    fun postHREnlistDataHRcustomID(@Query("registrationType") registrationType: String,
                         @Query("access_token") accessToken: String,
                         @Body obj: Array<*>): Call<ValidUserResponse>

    @GET("staff/basic/list/with/photo")
    fun getHRreports(@Query("access_token") accessToken: String): Call<HrReport>

    @GET("manual/attendance/period/list")
    fun getTakeAdtPeriodList(@Query("access_token") accessToken: String,
                      @Query("attendanceDate") attendanceDate: String,
                             @Query("classConfigId") localSectionID: String
    ): Call<TakeAtdPeriodResponse>

    @GET("student/list/by/class-config-id")
    fun getAllTakeAttendanceStudent(@Query("access_token") accessToken: String,
                             @Query("attendanceDate") attendanceDate: String,
                             @Query("classConfigId") localSectionID: String,
                             @Query("periodId") periodId: String
    ): Call<TakeAtdStudentResponse>

    @POST("manual/attendance/save/for/student")
    fun postTakeAttendanceData(@Query("access_token") access_token: String,
                                 @Body takeAttendanceList: TakeAtdStudentPost): Call<TakeAtdStudentPost>

    @GET("staff/attendance/date-based/by/attendance-status")
    fun getHRAttendanceReports(@Query("attendanceDate") attendanceDate: String,
                                    @Query("attendanceStatus") localSectionID: String,
                                    @Query("access_token") periodId: String
    ): Call<HRAttendanceReportsResponse>

    @POST("result/process/section-wise")
    fun generalExamResultProcess(
        @Query("access_token") access_token: String,
        @Query("classConfigurationId") classConfigurationId: String,
        @Query("examConfigurationId") examConfigurationId: String,
        @Query("type") type: String

    ): Call<ValidUserResponse>

    @GET("core/setting/list/by-type-id")
    fun getGrandFinalClassData(
        @Query("access_token") accessToken: String,
        @Query("typeId") typeId: String
    ): Call<GrandFinalClassResponse> //2102

    @GET("exam/grand-final/configuration/list/by/class-id")
    fun getGrandFinalExamList(
        @Query("classId") classId: String,
        @Query("access_token") accessToken: String
    ): Call<GrandFinalExamResponse>

    @POST("result/process/class-wise/grand-final")
    fun grandFinalExamResultProcess(
        @Query("access_token") accessToken: String,
        @Query("examConfigurationId") classConfigurationId: String

    ): Call<ValidUserResponse>

    @GET("exam/configuration/list/by/class-id")
    fun getMeritPositionExamList(
        @Query("access_token") accessToken: String,
        @Query("classId") classId: String
    ): Call<MeritPositionExamResponse>

    @POST("result/process/merit-position/generate")
    fun meritPositionProcess(
        @Query("access_token") accessToken: String,
        @Query("classConfigurationId") classConfigurationId: String,
        @Query("examConfigurationId") examConfigurationId: String,
        @Query("type") type: String
    ): Call<ValidUserResponse>

    @POST("result/process/working-day/generate")
    fun attendanceCountProcess(
        @Query("access_token") accessToken: String,
        @Query("classConfigId") classConfigId: String,
        @Query("periodId") periodId: String,
        @Query("examConfigurationId") examConfigurationId: String,
        @Query("type") type: String
    ): Call<ValidUserResponse>

    @GET("exam/report/section-wise/result/details")
    fun getStudentResultReportList(
        @Query("access_token") accessToken: String,
        @Query("classConfigId") classConfigId: String,
        @Query("examConfigId") examConfigId: String
    ): Call<StudentResultReportResponse>

    @GET("exam/remarks/template/find/all/list")
    fun getRemarksAssignTitleList(
        @Query("access_token") accessToken: String
    ): Call<RemarksTitleResponse>

    @GET("exam/remarks/template/find/by-remarkId")
    fun getRemarksAssignDescription(
        @Query("access_token") accessToken: String,
        @Query("remarkTempId") remarkTempId: String
    ): Call<JsonObject>

    @POST("exam/un-remarked/student/list")
    fun getRemarkAssignStudentList(
        @Query("access_token") accessToken: String,
        @Query("classConfigId") classConfigId: String,
        @Query("examConfigId") examConfigId: String
    ): Call<RemarkAssignStudentListResponse>

    @POST("exam/remarks/create")
    fun postRemarkAssignData(
        @Query("access_token") accessToken: String,
        @Body remarkAssignList: RemarkAssignPostData
    ): Call<ValidUserResponse>

    //---------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("core/setting/list/by-type-id")
    fun getAcademicYearList(
        @Query("access_token") accessToken: String,
        @Query("typeId") typeId: String
    ): Call<AcademicYearResponse>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("core/setting/class-configuration/list")
    fun getSectionList(@Query("access_token") accessToken: String): Call<SectionResponse>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("core/setting/group-configuration/list/by/class-config-id")
    fun getGroupList(
        @Query("access_token") accessToken: String,
        @Query("classConfigId") configId: String
    ): Call<GroupResponse>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("core/setting/list/by-type-id")
    fun getCategoryList(
        @Query("access_token") accessToken: String,
        @Query("typeId") typeId: String
    ): Call<CategoryResponse>

    @Headers("Content-Type: application/json")
    @POST("student/list/save")
    fun saveStudentEnrollmentData(@Query("access_token") accessToken: String,
                                  @Query("registrationType") idTYpe: String,
                                  @Body jsonObject: JsonObject
    ): Call<JsonObject>

    @GET("student/list/by/class-config-id")
    fun getStudentReportList(@Query("access_token") accessToken: String,
                             @Query("classConfigId") sectionId: String
    ): Call<StudentReportResponse>

    @GET("core/setting/list/by-type-id")
    fun getPeriodListForSearchingStudentReport(@Query("access_token") accessToken: String,
                                               @Query("typeId") typeId: String
    ): Call<PeriodResponse>

    @GET("attendance/report/sec-wise/atten/summary/by-date")
    fun getStudentAttendanceReportSummaryList(@Query("access_token") accessToken: String,
                                              @Query("attendanceDate") attendanceDate: String,
                                              @Query("periodId") periodId: String
    ): Call<AttendanceReportSummaryResponse>

    @GET("attendance/report/status-and-sec-wise/std-details/by/period-id/and/date")
    fun getStudentAttendanceReportList(@Query("access_token") accessToken: String,
                                       @Query("attendanceDate") attendanceDate: String,
                                       @Query("periodId") periodId: String,
                                       @Query("classConfigId") configId: String,
                                       @Query("attendanceStatus") attendanceStatus: String
    ): Call<StudentAttendanceReportDetailsResponse>

    @GET("core/setting/class-config/list/role/wise")
    fun getRoleWiseSectionList(@Query("access_token") accessToken: String) : Call<SectionResponse>

    @GET("exam/configuration/list/by/class-config-id")
    fun getExamList(@Query("access_token") accessToken: String,
                    @Query("classConfigId") classConfigId: String?
    ) : Call<ExamResponse>

    @GET("core/setting/subjectconfiguration/list/by/class-config-id/and/group-id")
    fun getSubjectList(@Query("access_token") accessToken: String,
                       @Query("classConfigId") classConfigId: String,
                       @Query("groupId") groupId: String
    ) : Call<SubjectResponse>

    @GET("exam/mark/student/list")
    fun getMarkInputList(@Query("access_token") accessToken: String,
                         @Query("classConfigurationId") configId: String,
                         @Query("groupId") groupId: String,
                         @Query("subjectId") subjectId: String,
                         @Query("examConfigurationId") examId: String
    ) : Call<MarkInputResponse>

    @GET("exam/mark/config/list/by/subject-id")
    fun getMarkScaleList(@Query("access_token") accessToken: String,
                         @Query("classConfigurationId") configId: String,
                         @Query("groupId") groupId: String,
                         @Query("subjectId") subjectId: String,
                         @Query("examConfigId") examId: String
    ) : Call<MarkScaleResponse>

    @GET("exam/mark/list/by/section-id/subject-id")
    fun getMarkUpdateList(@Query("access_token") accessToken: String,
                          @Query("classConfigurationId") configId: String,
                          @Query("groupId") groupId: String,
                          @Query("subjectId") subjectId: String,
                          @Query("examConfigurationId") examId: String
    ) : Call<MarkInputResponse>

    @Headers("Content-Type: application/json")
    @POST("exam/mark/input")
    fun saveExamMarkInput(@Query("access_token") accessToken: String,
                          @Body examMarkInput: ExamMarkInput
    ): Call<JsonObject>

    @Headers("Content-Type: application/json")
    @POST("exam/mark/update")
    fun updateExamMarkInput(@Query("access_token") accessToken: String,
                            @Body examMarkInput: ExamMarkInput
    ): Call<JsonObject>

    @GET("exam/report/exam-wise/mark/inputted/summary")
    fun getUnAssignedResultSummary(@Query("access_token") accessToken: String,
                                   @Query("examId") examId: String
    ): Call<UnAssignedResultResponse>

    @GET("exam/section-wise/remarks/find/by/exam-config-id")
    fun getRemarkUpdateList(@Query("access_token") accessToken: String,
                            @Query("classConfigId") classConfigId: String,
                            @Query("examConfigId") examConfigId: String
    ): Call<RemarksUpdateResponse>

    @PUT("exam/remarks/all/update")
    fun updateRemarksList(@Query("access_token") accessToken: String,
                          @Body jsonArray: JsonArray
    ): Call<JsonObject>
}