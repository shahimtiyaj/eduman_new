package com.netizen.eduman.apiService

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {

     //val BASE_URL = "https://api.netizendev.com:2083/emapi/" //  Live Url     ---   user: 01718911185   pass: eduman6
     //val BASE_URL = "https://api.edumanbd.com/emapi/" //Product Live Url   ---   user: 01750316386   pass: 01750316386
     val BASE_URL = "https://api.edumanbd.com/" //Change (03/03/2020)Product Live Url- user: 01750316386/01965736575   pass: 01750316386
     //val BASE_URL = "http://192.168.0.5:8080/" //Local Url
    //val BASE_URL = "http://192.168.0.129:8083/" //Local Url

    private var retrofit: Retrofit? = null

    val getClient: Retrofit?
        get() {
            /**
             * For observing network request
             */
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val client = OkHttpClient.Builder()
                .followRedirects(false)
                .followSslRedirects(false)
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build()

            if (retrofit == null) {
                retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build()
            }

            return retrofit
        }
}